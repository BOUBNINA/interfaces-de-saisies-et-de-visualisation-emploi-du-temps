package fr.univangers.isve.isvesolveur.entities;

import java.io.Serializable;

public class Programme implements Serializable {

	private Long codeProgramme;
	private int debutProgramme;
	private String dureeProgramme;
	private String semaineProgramme;
	private String jourProgramme;
	private int penalty_programme;


	public Programme() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getDebutProgramme() {
		return debutProgramme;
	}

	public Long getCodeProgramme() {
		return codeProgramme;
	}
	public void setCodeProgramme(Long codeProgramme) {
		this.codeProgramme = codeProgramme;
	}
	public String getDureeProgramme() {
		return dureeProgramme;
	}
	public void setDureeProgramme(String dureeProgramme) {
		this.dureeProgramme = dureeProgramme;
	}
	public String getSemaineProgramme() {
		return semaineProgramme;
	}
	public void setSemaineProgramme(String semaineProgramme) {
		this.semaineProgramme = semaineProgramme;
	}
	public String getJourProgramme() {
		return jourProgramme;
	}
	public void setJourProgramme(String jourProgramme) {
		this.jourProgramme = jourProgramme;
	}
	public int getPenalty_programme() {
		return penalty_programme;
	}
	public void setPenalty_programme(int penalty_programme) {
		this.penalty_programme = penalty_programme;
	}


}
