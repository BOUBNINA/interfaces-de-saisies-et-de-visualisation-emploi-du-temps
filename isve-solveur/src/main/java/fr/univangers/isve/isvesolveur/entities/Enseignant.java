package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.Collection;


public class Enseignant implements Serializable {

	private String matriculeEnseignant;

	private String nomEnseignant;
	private String prenomEnseignant;


	
	
	
	public Enseignant() {
		super();
	}
	
	
	public Enseignant(String matriculeEnseignant) {
		
		super();
		this.matriculeEnseignant = matriculeEnseignant;



	}

	public String getNomEnseignant() {
		return nomEnseignant;
	}

	public String getPrenomEnseignant() {
		return prenomEnseignant;
	}

	public void setNomEnseignant(String nomEnseignant) {
		this.nomEnseignant = nomEnseignant;
	}

	public void setPrenomEnseignant(String prenomEnseignant) {
		this.prenomEnseignant = prenomEnseignant;
	}

	public String getMatriculeEnseignant() {
		return matriculeEnseignant;
	}

	public void setMatriculeEnseignant(String matriculeEnseignant) {
		this.matriculeEnseignant = matriculeEnseignant;
	}
	


}
