package fr.univangers.isve.isvesolveur.Service;

import fr.univangers.isve.isvesolveur.Rest.ScanSolveurRestService;
import fr.univangers.isve.isvesolveur.params.Params;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.WatchService;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.util.Properties;

import javax.naming.TimeLimitExceededException;

public class Scanneur implements Runnable{

	Logger logger = LoggerFactory.getLogger(Scanneur.class);

	private Thread thread;
	private int msLimitTime;
	private int msWaitingTime;
	private Path folder;
	PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.txt");
	private Path file;

	private final PropertyChangeSupport support = new PropertyChangeSupport(this);

	private boolean isRunning;
	
	public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
        support.addPropertyChangeListener( property, listener);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(String property, PropertyChangeListener listener) {
        support.removePropertyChangeListener(property, listener);
    }
	
	public Scanneur() throws IOException {


		logger.info("Constructeur Scanneur");
		System.out.println(System.getProperty("user.dir"));


		this.folder = Paths.get(Params.getInstance().getValue("folder"));
		this.msLimitTime = Integer.parseInt(Params.getInstance().getValue("limitTime"));
		this.msWaitingTime = Integer.parseInt(Params.getInstance().getValue("waitingTime"));
		try {
			if(!(boolean)(Files.getAttribute(this.folder, "basic:isDirectory", LinkOption.NOFOLLOW_LINKS))) {
				throw new IllegalArgumentException("Path: " + this.folder + " is not a folder");
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void run() {
		System.out.println("------------------------------RUN-----------------------------------");
		FileSystem fs = this.folder.getFileSystem();
		try (WatchService service = fs.newWatchService()){
			folder.register(service, StandardWatchEventKinds.ENTRY_CREATE,StandardWatchEventKinds.ENTRY_MODIFY);
			WatchKey key = null;
			while(this.isRunning) {
				key = service.take();
				
				Kind<?> kind = null;
				for (WatchEvent<?> watchEvent : key.pollEvents()) {
					kind = watchEvent.kind();
					
					
					if(StandardWatchEventKinds.OVERFLOW == kind) {
						continue;
					} else if(StandardWatchEventKinds.ENTRY_MODIFY == kind ) {

						 Path dir = (Path)key.watchable();
						 Path newFile =  dir.resolve(((WatchEvent<Path>) watchEvent).context());
						 
						

						 if(matcher.matches(newFile.getFileName())) {
							 support.firePropertyChange("file",this.file, newFile);
							 this.isRunning = false;
							 break;
						 }
	                     
	                    
					}
				}
					
				Thread.sleep(msWaitingTime);
				this.msLimitTime -= this.msWaitingTime;
				if(msLimitTime <=0) {
					this.isRunning = false;
					throw new TimeLimitExceededException("Scanneur : Scan's time limit exceeded.");

				}
				 if (!key.reset()) {
	                    break; // loop
	             }

				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void startScan() {
		logger.info("start scan ...");
		this.isRunning = true;
		this.thread = new Thread(this);
		this.thread.start();
	}
	

}
