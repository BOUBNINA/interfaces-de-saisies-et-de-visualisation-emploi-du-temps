package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.Collection;


public class Matiere implements Serializable {
	

	private Long codeMatiere;
	private String libelleMatiere;




	private Collection<Classe> classes;
	
	public Matiere() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Matiere(String codeMatiere, String libelleMatiere, String volumeHoraireMatiere, int ectsMatiere,
			int chargeCM_Matiere, int chargeTP_Matiere, UniteEnseignement uniteEnseignement) {
		super();

		this.libelleMatiere = libelleMatiere;

	}


	public Long getCodeMatiere() {
		return codeMatiere;
	}

	public void setCodeMatiere(Long codeMatiere) {
		this.codeMatiere = codeMatiere;
	}

	public String getLibelleMatiere() {
		return libelleMatiere;
	}
	public void setLibelleMatiere(String libelleMatiere) {
		this.libelleMatiere = libelleMatiere;
	}




	public Collection<Classe> getClasses() {
		return classes;
	}

	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}


	
	

}
