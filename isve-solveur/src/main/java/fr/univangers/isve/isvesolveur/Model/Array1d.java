package fr.univangers.isve.isvesolveur.Model;

import java.util.ArrayList;

public class Array1d<T> extends ArrayList<T>{

	private int s1;
	public Array1d(int s1){
		this.s1 = s1;
	}
	
	public int getS1() {
		return s1;
	}

	private static final long serialVersionUID = 1L;

}
