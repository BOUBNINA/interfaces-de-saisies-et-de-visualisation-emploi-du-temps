package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.Collection;


public class Groupe implements Serializable {

	private Long numeroGroupe;
	private String libelleGroupe;
	private Long numeroGroupeParent;




	
	public Groupe() {
		super();
	}
	
	public Groupe(String libelleGroupe) {
		super();
		this.libelleGroupe = libelleGroupe;

	}
	
	public Long getNumeroGroupe() {
		return numeroGroupe;
	}
	public void setNumeroGroupe(Long numeroGroupe) {
		this.numeroGroupe = numeroGroupe;
	}
	
	
	public String getLibelleGroupe() {
		return libelleGroupe;
	}
	public void setLibelleGroupe(String libelleGroupe) {
		this.libelleGroupe = libelleGroupe;
	}
	



	public Long getNumeroGroupeParent() {
		return this.numeroGroupeParent;
	}
}
