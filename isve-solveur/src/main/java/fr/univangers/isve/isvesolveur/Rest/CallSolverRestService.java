package fr.univangers.isve.isvesolveur.Rest;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.isvesolveur.call.CallSolverControler;

@RestController
public class CallSolverRestService {
	
	CallSolverControler solverCaller = new CallSolverControler();
	
	//le lien pour l'appel au solver a partir du client 
	//http://localhost:8080/callSolver?fileName=garam_facile
	
	@RequestMapping(value="/callSolver", method = RequestMethod.GET)
	public String call(@RequestParam String fileName) throws IOException{
		return solverCaller.executerCommande(fileName);
	}
	
}
