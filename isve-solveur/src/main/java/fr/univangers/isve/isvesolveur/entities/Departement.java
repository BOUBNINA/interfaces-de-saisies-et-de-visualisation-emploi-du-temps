package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;


import java.io.Serializable;
import java.util.Collection;


public class Departement implements Serializable{

	private Long codeDepartement;
	private String nomDepartement;

	//private Faculte faculte;
	//private Collection<Etape> etapes;
	//private Collection<Enseignant> enseignants;
	private Collection<Salle> salles;
	
	public Departement() {
		super();
	}

	public Departement(String nomDepartement) {
		super();
		this.nomDepartement = nomDepartement;

	}


	public String getNomDepartement() {
		return nomDepartement;
	}
	
	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}

	public Long getCodeDepartement() {
		return codeDepartement;
	}

	public void setCodeDepartement(Long codeDepartement) {
		this.codeDepartement = codeDepartement;
	}
	


	public Collection<Salle> getSalles() {
		return salles;
	}
	
	@JsonSetter
	public void setSalles(Collection<Salle> salles) {
		this.salles = salles;
	}
	
	
	



	
	
}
