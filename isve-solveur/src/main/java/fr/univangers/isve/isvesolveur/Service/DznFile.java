package fr.univangers.isve.isvesolveur.Service;




import fr.univangers.isve.isvesolveur.Model.Array1d;
import fr.univangers.isve.isvesolveur.Model.Array2d;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Classe représentant un fichier DZN
 */
public class DznFile extends File{

	private final Map<String, Object> parameters;
	private String filepath;
	
	
	
	private static final long serialVersionUID = 186527478177502897L;

	/**
	 * Créer une instance d'un fichier DZN situé à l'emplacement pathname.
	 *
	 * @param pathname
	 * @throws InvalidFileTypeException
	 */
	public DznFile(String pathname) throws InvalidFileTypeException {
		super(pathname);
		this.filepath = pathname;
		String ext = pathname.split("\\.")[pathname.split("\\.").length-1];
		//System.out.println(ext);
		if(!(ext.equals("dzn") || ext.equals("txt"))) {
			throw new InvalidFileTypeException(ext, "dzn, txt");
		}
		parameters = new HashMap<>();
			
	}

	/**
	 * Charge le contenu du fichier DZN lié s'il existe.
	 * En fonction de la donnée récupérée sur chaque ligne du fichier DZN, un objet ayant un type adapté sera créé et ajouté à la map.
	 *
	 * @throws FileNotFoundException
	 */
	public void load() throws FileNotFoundException {
		FileReader fr = null;

		fr = new FileReader(this.filepath);
			//System.out.println("test read");

		BufferedReader br = new BufferedReader(fr);
		String line;

			try {
				while(((line = br.readLine())!= null)) {
					line = line.replaceAll("\\s+", "");
					if(!line.contains("="))
						continue;
					//System.out.println(line);
					String name = line.split("=")[0];
					String val = line.split("=")[1];
					
					if(val.startsWith("array1d")) {
						Matcher m = Pattern.compile("(?<=\\[)(.*?)(?=\\])").matcher(val);
						int s1;
						Matcher mSize = Pattern.compile("(?<=1\\.\\.)[0-9]+").matcher(val);
						if(mSize.find()) {
							
							s1 = Integer.parseInt(mSize.group());
							
							
						}else {
							throw new Exception();
						}
						
						if(m.find() && m.group().matches(".*[a-zA-Z]+.*")) {
							
							Array1d<String> valArray = new Array1d<String>(s1);
							do {
								Matcher m2 = Pattern.compile("[a-zA-Z]+").matcher(m.group(1));
								while(m2.find()) {

									valArray.add(m2.group());
								}
							}while(m.find());
							this.parameters.put(name,valArray);
						}else if(m.group().matches(".*[0-9]+.*")){
							
							Array1d<Integer> valArray = new Array1d<Integer>(s1);
							do{
								Matcher m2 = Pattern.compile("[0-9]+").matcher(m.group(1));
								while(m2.find()) {

									valArray.add(Integer.parseInt(m2.group()));
								}
							}while(m.find());
							//System.out.println(valArray);
							this.parameters.put(name,valArray);
						}else {
							
							
							Array1d<Integer> valArray = new Array1d<Integer>(s1);
							
							this.parameters.put(name,valArray);
						}


					}else if(val.startsWith("array2d")) {
						Matcher m = Pattern.compile("(?<=\\[)(.*?)(?=\\])").matcher(val);

						Matcher mSize = Pattern.compile("(?<=1\\.\\.)[0-9]+").matcher(val);
						int s1;
						int s2;
						if(mSize.find()) {
							
							s1 = Integer.parseInt(mSize.group());
							
						}else {
							throw new Exception();
						}
						if(mSize.find()) {
							
							
							s2= Integer.parseInt(mSize.group());
						}else {
							throw new Exception();
						}
						
		
						if(m.find() && m.group().matches(".*[a-zA-Z]+.*")) {
							//System.out.println("name = " + name + ", m : "+m.group());
							Array2d<String> valArray = new Array2d<String>(s1,s2);
							do{
								Matcher m2 = Pattern.compile("[a-zA-Z]+").matcher(m.group(1));
								while(m2.find()) {

									valArray.add(m2.group());
								}
							}while(m.find());
							this.parameters.put(name,valArray);
						}else if( m.group().matches(".*[0-9]+.*")) {
							//System.out.println(m.group());
							Array2d<Integer> valArray = new Array2d<Integer>(s1,s2);
							do{
								Matcher m2 = Pattern.compile("[0-9]+").matcher(m.group());
								while(m2.find()) {

									valArray.add(Integer.parseInt(m2.group()));
								}
							}while(m.find());
							
							this.parameters.put(name,valArray);
						}else {
							Array2d<Integer> valArray = new Array2d<Integer>(s1,s2);
							
							
							
							this.parameters.put(name,valArray);
						}
						
					}else if(val.startsWith("[{")){

						Matcher m = Pattern.compile("(?<=\\{)(.*?)(?=\\})").matcher(val);
						
						
						if(m.find() && m.group().matches(".*[a-zA-Z]+.*")) {
							ArrayList<ArrayList<String>> valArray = new ArrayList<>();
							do {
								
								valArray.add(new ArrayList<>());
								Matcher m2 = Pattern.compile("[a-zA-Z]+").matcher(m.group());
								while(m2.find()) {

									valArray.get(valArray.size()-1).add(m2.group());
									
								}
							}while(m.find());
							this.parameters.put(name,valArray);
						}else {
							ArrayList<ArrayList<Integer>> valArray = new ArrayList<>();
							do{
								valArray.add(new ArrayList<>());
								
								Matcher m2 = Pattern.compile("[0-9]+").matcher(m.group());
								while(m2.find()) {
									
									
									valArray.get(valArray.size()-1).add(Integer.parseInt(m2.group()));
								}
							}while(m.find());
							
							this.parameters.put(name,valArray);
						}



					}else if (val.startsWith("[")) {
						Matcher m = Pattern.compile("(?<=\\[)(.*?)(?=\\])").matcher(val);

						if(m.find() && m.group(1).matches(".*[a-zA-Z]+.*")) {
							ArrayList<String> valArray = new ArrayList<String>();
							do {
								Matcher m2 = Pattern.compile("[a-zA-Z]+").matcher(m.group(1));
								while(m2.find()) {

									valArray.add(m2.group());
								}
							}while(m.find());
							this.parameters.put(name,valArray);
						}else if(m.group(1).matches(".*[0-9]+.*")){
							ArrayList<Integer> valArray = new ArrayList<Integer>();
							do {
								Matcher m2 = Pattern.compile("[0-9]+").matcher(m.group(1));
								while(m2.find()) {

									valArray.add(Integer.parseInt(m2.group()));
								}
							}while(m.find());
							this.parameters.put(name,valArray);
						}else {
							ArrayList<Integer> valArray = new ArrayList<>();
							
							
							
							this.parameters.put(name,valArray);
						}
					}else {


						if(val.matches(".*[a-zA-Z]+.*")) {

							Matcher m = Pattern.compile("[a-zA-Z]+").matcher(val);
							m.find();
							this.parameters.put(name, m.group());
						}else {
							
							Matcher m = Pattern.compile("[0-9]+").matcher(val);
							m.find();
							this.parameters.put(name,Integer.parseInt(m.group()));
						}
					}


				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		try {
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}


	/**
	 * Sauvegarde les paramètres ajouté dans le map de DZNFile dans un fichier dzn situé à pathname.
	 * La façon dont le paramètre est sauvegardé dépent du type du paramètre.
	 * Si un fichier existe il est écrasé.
	 *
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public void save() throws IllegalArgumentException, IOException
	{
		FileWriter fw = new FileWriter(this.filepath);
		BufferedWriter br = new BufferedWriter(fw);
		String NewLine=System.getProperty("line.separator");
		
		
		parameters.forEach((key, value) -> {
			//System.out.print(key);
			try {
			if(value instanceof Integer || value instanceof String) {
				
					br.write(key + "="+value+";"+NewLine);
				
			}else if (value instanceof Array2d<?>){

				Array2d<?> val = (Array2d<?>)value;

				br.write(key+"=array2d(1.."+val.getS1()+", 1.."+val.getS2()+",[");

				for(int i =0; i<val.getS1() *  val.getS2();i++) {

					if(!val.isEmpty() && val.get(i) != null){
						//System.out.println(key);
						br.write(val.get(i).toString());
					}else{
						br.write("{}");
					}

					if(i != val.size()-1)
						br.write(",");

				}


				br.write("]);"+NewLine);

			}else if(value instanceof Array1d<?>){
				Array1d<?> val = (Array1d<?>)value;
				br.write(key+"=array1d(1.."+val.getS1()+",[");

				for(int i =0; i<val.getS1();i++) {
					if(!val.isEmpty() && val.get(i) != null){
						br.write(val.get(i).toString());
					}else{
						br.write("{}");
					}
					if(i != val.size()-1)
						br.write(",");
				}


				br.write("]);"+NewLine);

			}

			else if(value instanceof ArrayList<?>) {
				if(((ArrayList<?>)value).isEmpty()) {
					br.write(key+"=[];"+NewLine);
				}
				else if(((ArrayList<?>)value).get(0) instanceof ArrayList<?>) {
					ArrayList<ArrayList<?>> val = (ArrayList<ArrayList<?>>)value;
					
						br.write(key+"=[");
						for(ArrayList<?> line : val) {
							br.write("{");
							for(int i =0; i<line.size();i++) {
								br.write(line.get(i).toString());
								if(i != line.size()-1)
									br.write(",");
							}
							br.write("}");
							if(line != val.get(val.size()-1)) {
								br.write(",");
							}
						}
						br.write("];"+NewLine);
					
					
				}else {
					ArrayList<?> val = (ArrayList<?>)value;
					br.write(key+"=[");
					
						for(int i =0; i<val.size();i++) {
							br.write(val.get(i).toString());
							if(i != val.size()-1)
								br.write(",");
						}
						
					
					br.write("];"+NewLine);
				}
			}

			} catch (IOException e) {

				e.printStackTrace();
			}
		});
		
		
		br.close();
		fw.close();
		
		
	}

	/**
	 * Ajoute le paramètre value de type <T> et ayant pour clé textKey au Map de DZNFile
	 *
	 * @param textKey
	 * @param value
	 * @param <T>
	 */
	 public <T> void put(String textKey, T value) {
		 if (parameters.containsKey(textKey))
		    	throw new IllegalArgumentException("The texKey is already in the list.");
				    parameters.put(textKey, value);
		  }


	/**
	 * Retourne le type d'un paramètre ayant pour clé textKey
	 *
	 * @param textKey
	 * @param <T>
	 * @return
	 */
	 @SuppressWarnings("unchecked")
	public <T> Class<T> getType(String textKey) {
		 	if (!parameters.containsKey(textKey))
		 		throw new IllegalArgumentException("The texKey is not in the list.");
		    Object value = parameters.get(textKey);
		    System.out.println(value.getClass());
		    
		    
		    return (Class<T>) value.getClass();
		  }


	/**
	 * Retourne le paramètre ayant pour clé textKey
	 *
	 * @param textKey
	 * @param <T>
	 * @return
	 */
		  @SuppressWarnings("unchecked")
		public <T> T get(String textKey) {
			  if (!parameters.containsKey(textKey))
			    	throw new IllegalArgumentException("The texKey is not in the list.");
		    Object value = parameters.get(textKey);
		    
		    
		    
		    
		    return (T)value;
		  }

	/**
	 * Supprime le paramètre ayant pour clé textKey
	 *
	 * @param textKey
	 */
		  public void remove(String textKey) {
			    parameters.remove(textKey);
			    
			    
			    
			   
		  }

		  
	

	

	

}
