package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Collection;


public class UniteEnseignement implements Serializable{
	

	private String codeUE;
	private String nom_UE;






	private Collection<Matiere> matieres;
	
	public UniteEnseignement() {
		super();
	}
	
	public UniteEnseignement(String code_UE, String nom_UE) {
		super();
		this.codeUE = codeUE;
		this.nom_UE = nom_UE;

	}

	
	public String getCodeUE() {
		return codeUE;
	}

	public void setCodeUE(String codeUE) {
		this.codeUE = codeUE;
	}

	public String getNom_UE() {
		return nom_UE;
	}

	public void setNom_UE(String nom_UE) {
		this.nom_UE = nom_UE;
	}
	


	public Collection<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(Collection<Matiere> matieres) {
		this.matieres = matieres;
	}

	

	
	
}