package fr.univangers.isve.isvesolveur.Rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univangers.isve.isvesolveur.Service.*;
import fr.univangers.isve.isvesolveur.Service.Scanneur;
import fr.univangers.isve.isvesolveur.call.CallSolverControler;
import fr.univangers.isve.isvesolveur.entities.Etape;
import fr.univangers.isve.isvesolveur.params.Params;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@RestController
public class ScanSolveurRestService {

    Logger logger = LoggerFactory.getLogger(ScanSolveurRestService.class);

    @RequestMapping(value="/scan", method = RequestMethod.GET)
    public void scan(String codeEtape) throws IOException {

        logger.info("/scan web service : ");

        HttpURLConnection conn =null;
        Etape etape = null;

        conn = (HttpURLConnection) new URL(Params.getInstance().getValue("url")+"etapeCode?codeEtape="+ URLEncoder.encode(codeEtape, StandardCharsets.UTF_8.toString())).openConnection();

        logger.info("url = " + Params.getInstance().getValue("url")+"etapeCode?codeEtape="+ URLEncoder.encode(codeEtape, StandardCharsets.UTF_8.toString()));

        conn.setRequestMethod("GET");
        StringBuffer response = new StringBuffer();

        conn.setDoInput(true);
        conn.setDoOutput(true);
        int responseCode = conn.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            ObjectMapper mapper = new ObjectMapper();
            try{
                etape = mapper.readValue(response.toString(),Etape.class);
                logger.info("etape = " + etape);

            }catch (Exception ex){
                System.out.println("Parsing not worked");
                System.out.println(ex.getMessage());
            }




            try {
                Scanneur scanneur = new Scanneur();
                Parseur parseur = new Parseur(scanneur, etape);
                Sender sender = new Sender(parseur, etape);
                scanneur.startScan();
            } catch (IOException e) {
                e.printStackTrace();
            }



        } else {
            System.out.println("GET request not worked");
        }





    }


}
