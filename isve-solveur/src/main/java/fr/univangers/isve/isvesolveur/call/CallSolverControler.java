package fr.univangers.isve.isvesolveur.call;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import fr.univangers.isve.isvesolveur.Rest.DznRestService;
import fr.univangers.isve.isvesolveur.params.Params;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CallSolverControler {

    Logger logger = LoggerFactory.getLogger(CallSolverControler.class);

    private OsDetection os;
    private static String chemin_file_mzn;
    private static String chemin_file_dzn;
    private static String chemin_file_output;


    public CallSolverControler() {
        super();
        this.os = new OsDetection();
    }

    public static String saveResults(Process process) throws IOException {
        Logger logger = LoggerFactory.getLogger(CallSolverControler.class);
        logger.info("saveResults()");

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";

        Path path = Paths.get(chemin_file_output);
        if (Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File fichier = new File(chemin_file_output);

        try {
            //la creation du fichier
            //fichier.createNewFile();
            //la creation d'un writer
            FileWriter writer = new FileWriter(fichier);

            String string = "";
            try {

                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                    string += line + "\n";
                }
                writer.write(string);
				System.out.println("string = " + string);
            } finally {
                writer.close();
            }
            if (fichier.length() != 0) {
                return "true";
            }

        } catch (Exception e) {
            System.out.println("Impossible de créer lse fichier " + e.getMessage());
            e.printStackTrace();
        }

        return "false";
    }


    public String executerCommande(String fileName) throws IOException {

        logger.info("os = linux");
        chemin_file_mzn = Params.getInstance().getValue("chemin_mzn");
        chemin_file_dzn = Params.getInstance().getValue("folder_dzn") + fileName + ".dzn";
        chemin_file_output = Params.getInstance().getValue("chemin_output");

        logger.info("chemin_file_mzn = " + chemin_file_mzn);
        logger.info("chemin_file_dzn = " + chemin_file_dzn);
        logger.info("chemin_file_output = " + chemin_file_output);

        System.out.println("minizinc " + chemin_file_mzn + " " + chemin_file_dzn);
        Process process = Runtime.getRuntime().exec("minizinc " + chemin_file_mzn + " " + chemin_file_dzn);

        logger.info("fin de l execution de la commande minizinc ");

        return saveResults(process);

    }

}
