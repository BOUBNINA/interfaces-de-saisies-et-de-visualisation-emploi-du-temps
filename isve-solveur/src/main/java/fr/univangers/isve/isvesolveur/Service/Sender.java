package fr.univangers.isve.isvesolveur.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univangers.isve.isvesolveur.Model.Seance;
import fr.univangers.isve.isvesolveur.entities.Etape;
import fr.univangers.isve.isvesolveur.params.Params;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class Sender implements PropertyChangeListener {
    private ArrayList<Seance> seances = null;
    private Etape etape;

    public Sender(Parseur parseur, Etape etape){
        parseur.addPropertyChangeListener("seances",this);
        this.etape = etape;
    }

    public void send() throws Exception {
        if(seances == null) {
            return;
        }


        HttpURLConnection conn =null;


        conn = (HttpURLConnection) new URL(Params.getInstance().getValue("url")+"seancesEtape?etape="+ URLEncoder.encode(this.etape.getCodeEtape(), StandardCharsets.UTF_8.toString())).openConnection();
        conn.setRequestMethod("DELETE");
        StringBuffer response = new StringBuffer();

        conn.setDoInput(true);
        conn.setDoOutput(true);
        int responseCode = conn.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        if (responseCode != HttpURLConnection.HTTP_OK) { // success
            throw new Exception("Delete Seances error");
        }



        conn =null;
        try {
            conn = (HttpURLConnection) new URL(Params.getInstance().getValue("url")+"seanceDTO").openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            String JSON;
            ObjectMapper mapper = new ObjectMapper();

            OutputStream objOut = conn.getOutputStream();
            try{
                mapper.writeValue(objOut, seances);

            }catch (Exception ex){
                System.out.println("Parsing not worked");
                System.out.println(ex.getMessage());
            }
            //objOut.writeObject(seances);
            objOut.flush();
            objOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            ObjectInputStream objIn = new ObjectInputStream(conn.getInputStream());
            Object reply = objIn.readObject();
            objIn.close();
        } catch (Exception ex) {
            // it is ok if we get an exception here
            // that means that there is no object being returned
            System.out.println("No Object Returned");
            if (!(ex instanceof EOFException))
                ex.printStackTrace();
            System.err.println("*");
        }

    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        this.seances = (ArrayList<Seance>)propertyChangeEvent.getNewValue();

        try {
            this.send();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
