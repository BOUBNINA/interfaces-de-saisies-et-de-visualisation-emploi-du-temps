package fr.univangers.isve.isvesolveur.Service;




import fr.univangers.isve.isvesolveur.Model.Array2d;

import fr.univangers.isve.isvesolveur.Rest.DznRestService;
import fr.univangers.isve.isvesolveur.entities.*;
import fr.univangers.isve.isvesolveur.params.Params;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;


public class DznGenerator {

	Logger logger = LoggerFactory.getLogger(DznGenerator.class);

	DznFile file;


	
	private int nr_weeks=10;
	private int nr_days_per_week=5;
	private int nr_slots_per_day=5;
	private int nr_courses=9;
	private int nr_configs=9;
	private int nr_subparts=37;
	private int nr_classes=52;
	private int nr_times=1300;
	private int nr_rooms=5;
	private int max_capacity=100;
	private int nr_closures=0;
	private int nr_students=18;
	private int nr_distributions=85;
	private ArrayList<ArrayList<Integer>> configs;
	private ArrayList<ArrayList<Integer>> subparts;
	private ArrayList<ArrayList<Integer>> classes;
	private ArrayList<Integer> parent;
	private ArrayList<Integer> limit;
	private ArrayList<ArrayList<Integer>> times;
	private ArrayList<ArrayList<Integer>> time_weeks;
	private ArrayList<ArrayList<Integer>> time_days;
	private ArrayList<Integer> time_start;
	private ArrayList<Integer> time_length;
	private ArrayList<Integer> time_penalty;
	private ArrayList<ArrayList<Integer>> rooms;
	private ArrayList<Integer> capacity;
	private Array2d<Integer> room_penalty;
	private Array2d<Integer> travel;

	private ArrayList<ArrayList<Integer>> closures;
	private ArrayList<Integer> closure_weeks;
	private ArrayList<Integer> closure_days;
	private ArrayList<Integer> closure_slot;
	private ArrayList<Integer> closure_length;
	private ArrayList<ArrayList<Integer>> registrations;
	private ArrayList<String> distribution;
	private ArrayList<Integer> distribution_penalty;
	private Array2d<Integer> distribution_classes;
	private ArrayList<Integer> weight;
	
	public DznGenerator(Etape etape,int nr_weeks, int nr_slots_per_day) throws fr.univangers.isve.isvesolveur.Service.InvalidFileTypeException {
			file = new DznFile(Params.getInstance().getValue("folder")+"L1_MPCIE.dzn");


			logger.info("chemin fichier dzn = " + Params.getInstance().getValue("folder")+"L1_MPCIE.dzn");


			HashMap<String, ArrayList<Integer>> classesByEnseignant = new HashMap<>();

		this.nr_courses = 0;
		this.nr_configs = 0;
		this.nr_subparts = 0;
		this.nr_classes = 0;
		this.nr_times = 0;
		this.nr_students = 0;
		this.nr_closures = 0;
		this.nr_weeks = nr_weeks;
		this.parent = new ArrayList<>();
		this.nr_slots_per_day = nr_slots_per_day;
		this.configs = new ArrayList<ArrayList<Integer>>();
		this.subparts = new ArrayList<ArrayList<Integer>>();
		this.classes = new ArrayList<>();
		this.rooms = new ArrayList<>();
		this.distribution_classes = new Array2d<>();
		this.closures = new ArrayList<>();
		this.closure_days = new ArrayList<>();
		this.closure_weeks = new ArrayList<>();
		this.closure_length = new ArrayList<>();
		this.closure_slot = new ArrayList<>();
		this.room_penalty = new Array2d<>();
		this.times = new ArrayList<>();
		this.time_days = new ArrayList<>();
		this.time_weeks = new ArrayList<>();
		this.time_length = new ArrayList<>();
		this.time_penalty = new ArrayList<>();
		this.time_start = new ArrayList<>();
		this.weight = new ArrayList<>();
		this.distribution = new ArrayList<>();
		this.distribution_penalty = new ArrayList<>();
		this.nr_rooms =etape.getDepartement().getSalles().size();
		this.travel = new Array2d<Integer>(this.nr_rooms, this.nr_rooms);
		this.registrations = new ArrayList<>();
		this.capacity = new ArrayList<>();
		this.limit = new ArrayList<>();


		ArrayList<ArrayList<Integer>> toutLesGroupeFeuilleByClasses = new ArrayList<>();
		ArrayList<Groupe> toutLesGroupesFeuilles = this.feuilles(etape.getGroupes());
		ArrayList<Salle> toutesLesSalles = new ArrayList<>(etape.getDepartement().getSalles());

		// on récupère les groupes de plus petite granularités
		for(Groupe feuille : toutLesGroupesFeuilles){
			this.registrations.add(new ArrayList<>());
			this.nr_students+=1;

		}

		//on met la capacité de toutes les salle, on récupère la capacité max, et par défaut les salles ne sont jamais fermées
		int cSalleMax = 0;
		for (Salle s: toutesLesSalles) {

			this.capacity.add(s.getCapaciteSalle());
			this.closures.add(new ArrayList<>());
			if(s.getCapaciteSalle()>cSalleMax)
				cSalleMax = s.getCapaciteSalle();


		}
		this.max_capacity = cSalleMax;

		ArrayList<Map<String,Object>> tmp_times = new ArrayList<Map<String,Object>>();

		//on parcourt toutes les ue
		ArrayList<UniteEnseignement> ues = new ArrayList<UniteEnseignement>(etape.getUniteEnseignements());
		for (int i = 0; i< ues.size(); i++) {
			ArrayList<Matiere> mat = new ArrayList<Matiere>(ues.get(i).getMatieres());
			//on parcourt toutes les matières
			for (int j = 0; j <  mat.size(); j++) {
				//chaque matière correspondant à un course, on augmente le nr_courses de 1, idem pour nr_config, chaque matière ayant une seule config
				this.nr_courses +=1;
				this.nr_configs+=1;
				
				this.configs.add(new ArrayList<Integer>());
				this.configs.get(this.configs.size()-1).add(this.nr_configs);
				//on ajoute un nouveau tableau de subpart correspondant à cette config/matière
				this.subparts.add(new ArrayList<Integer>());
				// on récupère les différents courssame_att de la matière et on les tri suivant leurs type : CM, TD ou TP
				ArrayList<Classe> classe = new ArrayList<Classe>(mat.get(j).getClasses());
				Collections.sort(classe, new Comparator<Classe>() {
					@Override
					public int compare(Classe t1, Classe t2) {
						return t1.getTypeSeance().getCodeTypeSeance().compareTo(t2.getTypeSeance().getCodeTypeSeance());
					}
				});




				//on parcourt les classes triéées de la matière
				for (int k = 0; k < classe.size();k++) {
					System.out.println(this.nr_classes+1 + ": " +classe.get(k).getCodeClasse());
					//la première classe d'une matière est fait forcément partie d'une nouvelle subpart, ici on considère qu'un type de cours est une subpart
					if(k==0){
						this.nr_subparts +=1;
						this.classes.add(new ArrayList<>());
						this.subparts.get(this.subparts.size()-1).add(this.nr_subparts);
					//les classe étant trié, tout changement de type indique que l'on change de subpart
					}else if(classe.get(k).getTypeSeance().getCodeTypeSeance() != classe.get(k-1).getTypeSeance().getCodeTypeSeance()){
						this.nr_subparts +=1;
						this.classes.add(new ArrayList<>());
						this.subparts.get(this.subparts.size()-1).add(this.nr_subparts);
					}
					//on incrémente le nombre de classes
					this.nr_classes+=1;

					// on prépare les pénalité des salle pour ce cours en initialisant toutes les pénalité lié à cette classe à 0
					for(Salle salle : toutesLesSalles){
						this.room_penalty.add(0);
					}


					//on ajoute un liste de salle pour chaque cours
					this.rooms.add(new ArrayList<>());

					//on recupère la liste des salles possible pour un cours. on met a jour le tableau de pénalité si nécessaire et on ajoute chaque salle possible dans la liste des salle par cours
					for(ClasseSalle cs : classe.get(k).getSalles()){
						for(int s = 0 ; s<toutesLesSalles.size();s++){


							if(toutesLesSalles.get(s).getCodeSalle().compareTo(cs.getSalle().getCodeSalle())==0) {
								this.rooms.get(this.rooms.size() - 1).add(s + 1);
								this.room_penalty.set((this.nr_classes-1)*toutesLesSalles.size()+s,cs.getPenalty());


							}
						}


					}
					this.rooms.get(this.rooms.size()-1).sort(new Comparator<Integer>() {
						@Override
						public int compare(Integer integer, Integer t1) {
							return integer.compareTo(t1);
						}
					});

					//on garde les classes de chaque enseignant
					Enseignant enseignant = classe.get(k).getEnseignant();
					if(!classesByEnseignant.containsKey(enseignant.getMatriculeEnseignant())){
						classesByEnseignant.put(enseignant.getMatriculeEnseignant(), new ArrayList<>());
					}


					classesByEnseignant.get(enseignant.getMatriculeEnseignant()).add(this.nr_classes);


					//on ajoute l'id du cours actuel dans le tableau des cours, dans la subpart coresspondante
					this.classes.get(this.classes.size()-1).add(this.nr_classes);
					//les parents ne sont pas géré actuellement, on les définit tous à 0
					this.parent.add(0);//a revoir

					//on récupère les groupe feuilles inscrit à un cours. le nombre de ces groupe est la limite de groupe de ce cours.
					ArrayList<Groupe> classeGroupesFeuilles = new ArrayList<>();

					for(Groupe g : classe.get(k).getGroupes()){


						classeGroupesFeuilles.addAll(this.feuilles(g, etape.getGroupes()));
					}

					//ArrayList<Groupe> classeGroupesFeuilles = this.feuilles(classe.get(k).getGroupe(), etape.getGroupes());


					this.limit.add(classeGroupesFeuilles.size());
					toutLesGroupeFeuilleByClasses.add(new ArrayList<>());
					if(this.nr_classes==5){
						System.out.println(classeGroupesFeuilles.get(0).getNumeroGroupe());
					}
					//on inscrit les groupes de ce cours à la matière

					for(int n = 0; n < classeGroupesFeuilles.size(); n++){
						for(int m = 0; m < toutLesGroupesFeuilles.size(); m++){
							if(classeGroupesFeuilles.get(n).getNumeroGroupe().compareTo(toutLesGroupesFeuilles.get(m).getNumeroGroupe())==0){
								boolean isRegistered = false;
								toutLesGroupeFeuilleByClasses.get(toutLesGroupeFeuilleByClasses.size()-1).add(m+1);
								for(int o =0 ;o<this.registrations.get(m).size();o++){
									//on empeche la double inscription
									if(this.registrations.get(m).get(o).compareTo(this.nr_courses) == 0){
										isRegistered = true;
										break;
									}


								}
								if(!isRegistered){
									this.registrations.get(m).add(this.nr_courses);

								}
							}
						}



					}








					//on place tout les créneau possible dans un tableau temporaire liant chaque créneau à l'id du cours en question
					ArrayList<Programme> time = new ArrayList<>(classe.get(k).getProgrammes());

					this.times.add(new ArrayList<>());
					for(int l=0; l<time.size();l++){
						Map<String,Object> tmp_map = new HashMap<>();
						tmp_map.put("Time", time.get(l));
						tmp_map.put("classIndex", this.nr_classes);

						tmp_times.add(tmp_map);
					}

				}

			}
		}
		//on trie les créneau par pénalité
		Collections.sort(tmp_times, new Comparator<Map<String,Object>>() {
			@Override
			public int compare(Map<String,Object> time, Map<String,Object> t1) {
				return ((Programme)time.get("Time")).getPenalty_programme() - ((Programme)t1.get("Time")).getPenalty_programme();
			}
		});
		//et on affecte chaque attribut du créneau dans les tableaux correspondants

		for(int i = 0; i< tmp_times.size();i++){
			this.nr_times+=1;
			this.times.get((Integer)tmp_times.get(i).get("classIndex")-1).add(this.nr_times);

			this.time_penalty.add(((Programme)tmp_times.get(i).get("Time")).getPenalty_programme());
			this.time_start.add(((Programme)tmp_times.get(i).get("Time")).getDebutProgramme());//((Programme)tmp_times.get(i).get("Time")).getDebutProgramme());
			this.time_length.add(Integer.parseInt(((Programme)tmp_times.get(i).get("Time")).getDureeProgramme()));
			this.time_weeks.add(new ArrayList<>());
			this.time_days.add(new ArrayList<>());

			String tmp_week = ((Programme)tmp_times.get(i).get("Time")).getSemaineProgramme();
			String tmp_days = ((Programme)tmp_times.get(i).get("Time")).getJourProgramme();

			for(int j =0; j<tmp_days.length();j++){
				if(tmp_days.charAt(j)-'0' == 1){
					this.time_days.get(this.nr_times-1).add(j+1);
				}
			}
			for(int j =0; j<tmp_week.length();j++){
				if(tmp_week.charAt(j)-'0' == 1){
					this.time_weeks.get(this.nr_times-1).add(j+1);
				}
			}


		}



		//On ajoute une contrainte pour chaque groupe, ses cours ne peuvent avoir lieu en même temps.
		ArrayList<ArrayList<Integer>> sameAttendes = new ArrayList<>();
		for(int i = 0; i<toutLesGroupesFeuilles.size();i++){
			sameAttendes.add(new ArrayList<>());
			for(int j = 0; j<toutLesGroupeFeuilleByClasses.size();j++){
				if(toutLesGroupeFeuilleByClasses.get(j).contains(i+1)){
					sameAttendes.get(sameAttendes.size()-1).add(j+1);
				}else{
					sameAttendes.get(sameAttendes.size()-1).add(0);
				}
			}
			System.out.println(toutLesGroupeFeuilleByClasses);
			System.out.println(sameAttendes.get(sameAttendes.size()-1));

		}

		//On ajoute une contrainte pour chaque enseignant, ses cours ne peuvent avoir lieu en même temps.
		for(Map.Entry<String, ArrayList<Integer>> mapentry: classesByEnseignant.entrySet()){
			sameAttendes.add(new ArrayList<>());
			for(int j = 1; j <= this.nr_classes; j++){
				if(mapentry.getValue().contains(j)){
					sameAttendes.get(sameAttendes.size()-1).add(j);
				}else{
					sameAttendes.get(sameAttendes.size()-1).add(0);
				}
			}



		}

		//on verifie que les contraintes ne s'applique pas à une seule classe
		for( ArrayList<Integer> sameAttendee : sameAttendes.stream().distinct().collect(Collectors.toList())){
			if(sameAttendee.stream().filter(x -> x > 0).toArray().length >1){
				this.distribution.add("same_attendees");
				this.distribution_penalty.add(0);
				System.out.println(sameAttendee);
				this.distribution_classes.addAll(sameAttendee);

			}
		}

		// on affecte les dernier paramètres de nombres
		this.nr_distributions = this.distribution.size();

		this.distribution_classes.setS1(this.nr_distributions);
		this.distribution_classes.setS2(this.nr_classes);

		this.room_penalty.setS1(this.nr_classes);
		this.room_penalty.setS2(this.nr_rooms);



		//on initialize les paramètres de poids par défaut
		this.weight.add(1);
		this.weight.add(0);
		this.weight.add(0);
		this.weight.add(0);

	}

	private ArrayList<Groupe> feuilles(Collection<Groupe> liste){
		ArrayList<Groupe> feuilles = new ArrayList<>();

		for(Groupe g : liste){
			if(liste.stream().filter(ge -> ge.getNumeroGroupeParent() == g.getNumeroGroupe()).toArray().length==0){
				feuilles.add(g);
			}
		}
		return feuilles;

	}

	private ArrayList<Groupe> feuilles(Groupe g, Collection<Groupe> liste){
		ArrayList<Groupe> feuilles = new ArrayList<>();
		ArrayList<Groupe> children = new ArrayList<>();

		for(Groupe ge : liste){
			if(ge.getNumeroGroupeParent().compareTo(g.getNumeroGroupe())==0){
				children.add(ge);
			}
		}


		if(children.size() == 0){

			feuilles.add(g);
			return feuilles;
		}else{
			for(Groupe child : children){
				feuilles.addAll(this.feuilles(child, liste));
			}
			return feuilles;
		}

	}
	
	public String makeDzn() throws IllegalArgumentException, IllegalAccessException, IOException {
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields) {

			file.put(field.getName(), field.get(this));

		}
		
		
		
		file.save();

		String content = "";
		for (String line : Files.readAllLines(file.toPath()))
		{
			content+=line+'\n';
		}

		return content;

	}


}
