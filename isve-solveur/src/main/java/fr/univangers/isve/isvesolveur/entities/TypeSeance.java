package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.Collection;


public class TypeSeance implements Serializable {

	private Long codeTypeSeance;
	private String libelleTypeSeance;


	
	
	public TypeSeance() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TypeSeance(String libelleTypeSeance) {
		super();
		this.libelleTypeSeance = libelleTypeSeance;

	}
	

	public Long getCodeTypeSeance() {
		return codeTypeSeance;
	}
	

	public void setCodeTypeSeance(Long codeTypeSeance) {
		this.codeTypeSeance = codeTypeSeance;
	}
	
	public String getLibelleTypeSeance() {
		return libelleTypeSeance;
	}
	public void setLibelleTypeSeance(String libelleTypeSeance) {
		this.libelleTypeSeance = libelleTypeSeance;
	}
	

	
	
}
