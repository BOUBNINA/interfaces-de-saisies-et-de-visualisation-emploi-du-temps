package fr.univangers.isve.isvesolveur.call;

public class OsDetection {
	
	public OsDetection() {
		super();
	}
	
	public static boolean isLinux(){
		
	    String os = System.getProperty("os.name");  
	    return os.toLowerCase().indexOf("linux") >= 0;
	}
	
	public static boolean isWindows(){
		
	    String os = System.getProperty("os.name");
	    return os.toLowerCase().indexOf("windows") >= 0;
	}

}
