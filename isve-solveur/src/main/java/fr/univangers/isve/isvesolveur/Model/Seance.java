package fr.univangers.isve.isvesolveur.Model;


import fr.univangers.isve.isvesolveur.entities.Classe;
import fr.univangers.isve.isvesolveur.entities.Salle;
import fr.univangers.isve.isvesolveur.entities.TypeSeance;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;


public class Seance implements Serializable {


    public Long codeClasse;

    public Long codeTypeSeance;

    public String codeSalle;

    public String dateSeance;

    public String dureeSeance;

}
