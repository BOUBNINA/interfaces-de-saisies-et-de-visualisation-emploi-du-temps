package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Collection;

public class Classe implements Serializable{
	private Long codeClasse;

	private Collection<Groupe> groupes;

	private TypeSeance typeSeance;

	private Collection<ClasseSalle> salles;

	private Enseignant enseignant;

	private Collection<Programme> programmes;
	
	
	
	
	public Classe() {
		super();
	}

	public Classe(Matiere matiere, Collection<Groupe> groupe) {
		super();

		this.groupes = groupe;
	}
	
	

	public Classe(Long codeClasse) {
		super();
		this.codeClasse = codeClasse;
	}

	public Enseignant getEnseignant(){
		return this.enseignant;
	}



	public Long getCodeClasse() {
		return codeClasse;
	}
	
	public void setCodeClasse(Long codeClasse) {
		this.codeClasse = codeClasse;
	}
	

	
	
	public Collection<Groupe> getGroupes() {
		return groupes;
	}






	public TypeSeance getTypeSeance() {
		return this.typeSeance;
	}

	public Collection<ClasseSalle> getSalles() {
		return this.salles;
	}

	public Collection<Programme> getProgrammes() {
		return this.programmes;
	}
}
