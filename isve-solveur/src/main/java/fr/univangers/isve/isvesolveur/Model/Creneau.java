package fr.univangers.isve.isvesolveur.Model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Creneau implements Serializable {
    public LocalDateTime time;

    public LocalTime length;
}
