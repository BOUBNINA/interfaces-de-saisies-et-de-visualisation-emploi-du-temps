package fr.univangers.isve.isvesolveur.entities;

import java.io.Serializable;
import java.util.Collection;


public class Contrainte implements Serializable {

	private Long codeContrainte;
	private String typeContrainte;
	private Collection<Classe> classes;
	public Contrainte() {
		super();
	}

	public Contrainte(Long codeContrainte, String typeContrainte) {
		super();
		this.codeContrainte = codeContrainte;
		this.typeContrainte = typeContrainte;
	}

	public Long getCodeContrainte() {
		return codeContrainte;
	}
	
	public void setCodeContrainte(Long codeContrainte) {
		this.codeContrainte = codeContrainte;
	}
	public String getTypeContrainte() {
		return typeContrainte;
	}
	public void setTypeContrainte(String typeContrainte) {
		this.typeContrainte = typeContrainte;
	}

	public Collection<Classe> getClasses() {
		return classes;
	}

	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}

	@Override
	public String toString() {
		return "Contrainte [codeContrainte=" + codeContrainte + ", typeContrainte=" + typeContrainte + ", classes="
				+ classes + "]";
	}
		
}
