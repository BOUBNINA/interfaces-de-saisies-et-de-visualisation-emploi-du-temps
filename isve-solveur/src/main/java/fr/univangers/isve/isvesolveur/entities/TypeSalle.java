package fr.univangers.isve.isvesolveur.entities;

import java.io.Serializable;


public class TypeSalle implements Serializable {

	private Long codeTypeSalle;
	private String libelleTypeSalle;
	
	
	public TypeSalle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TypeSalle(String libelleTypeSalle) {
		super();
		this.libelleTypeSalle = libelleTypeSalle;
	}
	public Long getCodeTypeSalle() {
		return codeTypeSalle;
	}
	public void setCodeTypeSalle(Long codeTypeSalle) {
		this.codeTypeSalle = codeTypeSalle;
	}
	public String getLibelleTypeSalle() {
		return libelleTypeSalle;
	}
	public void setLibelleTypeSalle(String libelleTypeSalle) {
		this.libelleTypeSalle = libelleTypeSalle;
	}
	

}
