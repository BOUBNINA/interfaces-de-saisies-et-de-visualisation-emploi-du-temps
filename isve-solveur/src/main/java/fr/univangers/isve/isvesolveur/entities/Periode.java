package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Collection;


public class Periode implements Serializable{
	

	private Long codePeriode;
	private String libellePeriode;
	private String anneePeriode;
	private Collection<UniteEnseignement> uniteEnseignements;
	
	
	
	public Periode() {
		super();
	}

	public Periode(Long codePeriode, String libellePeriode, String anneePeriode) {
		super();
		this.codePeriode = codePeriode;
		this.libellePeriode = libellePeriode;
		this.anneePeriode = anneePeriode;
	}

	public Long getCodePeriode() {
		return codePeriode;
	}

	public void setCodePeriode(Long codePeriode) {
		this.codePeriode = codePeriode;
	}

	public String getLibellePeriode() {
		return libellePeriode;
	}

	public void setLibellePeriode(String libellePeriode) {
		this.libellePeriode = libellePeriode;
	}

	public String getAnneePeriode() {
		return anneePeriode;
	}

	public void setAnneePeriode(String anneePeriode) {
		this.anneePeriode = anneePeriode;
	}
	
	@JsonIgnore
	public Collection<UniteEnseignement> getUniteEnseignements() {
		return uniteEnseignements;
	}

	public void setUniteEnseignements(Collection<UniteEnseignement> uniteEnseignements) {
		this.uniteEnseignements = uniteEnseignements;
	}

	@Override
	public String toString() {
		return "Periode [codePeriode=" + codePeriode + ", libellePeriode=" + libellePeriode + ", anneePeriode="
				+ anneePeriode + ", uniteEnseignements=" + uniteEnseignements + "]";
	}
	
	
	
	
}
