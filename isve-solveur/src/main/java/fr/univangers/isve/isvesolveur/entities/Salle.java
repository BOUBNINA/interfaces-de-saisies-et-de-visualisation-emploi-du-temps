package fr.univangers.isve.isvesolveur.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;


import java.io.Serializable;
import java.util.Collection;


public class Salle implements Serializable {

	private String codeSalle;
	private String libelleSalle;

	private int capaciteSalle;



	
	public Salle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Salle(String codeSalle, TypeSalle typeSalle, int capaciteSalle, Departement departement) {
		super();
		this.codeSalle = codeSalle;

		this.capaciteSalle = capaciteSalle;

	}

	public String getLibelleSalle() {
		return libelleSalle;
	}

	public void setLibelleSalle(String libelleSalle) {
		this.libelleSalle = libelleSalle;
	}

	public String getCodeSalle() {
		return codeSalle;
	}
	
	public void setCodeSalle(String codeSalle) {
		this.codeSalle = codeSalle;
	}
	

	public int getCapaciteSalle() {
		return capaciteSalle;
	}

	public void setCapaciteSalle(int capaciteSalle) {
		this.capaciteSalle = capaciteSalle;
	}




	
}
