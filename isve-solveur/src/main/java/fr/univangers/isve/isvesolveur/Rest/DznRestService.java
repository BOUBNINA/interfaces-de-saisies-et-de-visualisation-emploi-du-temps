package fr.univangers.isve.isvesolveur.Rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import fr.univangers.isve.isvesolveur.Service.DznGenerator;
import fr.univangers.isve.isvesolveur.Service.InvalidFileTypeException;
import fr.univangers.isve.isvesolveur.entities.Etape;
import fr.univangers.isve.isvesolveur.entities.Salle;

import fr.univangers.isve.isvesolveur.params.Params;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class DznRestService {

    Logger logger = LoggerFactory.getLogger(DznRestService.class);

    /**
     *
     * @param codeEtape
     * @param responseRequest
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     */
    @RequestMapping(value = "/dzn", method = RequestMethod.GET)
    @ResponseBody
    public String createDzn(@RequestParam String codeEtape, HttpServletResponse responseRequest) throws IOException, IllegalAccessException {

        logger.info("createDzn()");
        logger.info("etape = " + codeEtape);

        HttpURLConnection conn =null;
        Etape etape = null;
        String dznContent = "";
        boolean verif = false;



        String spec = Params.getInstance().getValue("url")+"etapeCode?codeEtape="+URLEncoder.encode(codeEtape, StandardCharsets.UTF_8.toString());

        logger.info("spec = "+spec);

        conn = (HttpURLConnection) new URL(Params.getInstance().getValue("url")+"etapeCode?codeEtape="+URLEncoder.encode(codeEtape, StandardCharsets.UTF_8.toString())).openConnection();
        logger.info("after connection");
        conn.setRequestMethod("GET");
        StringBuffer response = new StringBuffer();

        conn.setDoInput(true);
        conn.setDoOutput(true);
        int responseCode = conn.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String inputLine;


            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            logger.info(response.toString());

            ObjectMapper mapper = new ObjectMapper();
            try{

                etape = mapper.readValue(response.toString(),Etape.class);

            }catch (Exception ex){
                logger.info("une exeception à été levé");
                ex.printStackTrace();
            }



            //ArrayList<Salle> salles = new ArrayList<Salle>(salleMetier.listSalleByDepartement(etape.getDepartement()));
            DznGenerator dzn;

            try {
                dzn = new DznGenerator(etape, 10,  5);

                 dznContent = dzn.makeDzn();
                verif = true;
                responseRequest.setStatus(HttpServletResponse.SC_ACCEPTED);
                logger.info("Status de la requête "+responseRequest.getStatus());


            } catch (InvalidFileTypeException e) {
                e.printStackTrace();


            }



        } else {


        }

        return dznContent;


    }

}
