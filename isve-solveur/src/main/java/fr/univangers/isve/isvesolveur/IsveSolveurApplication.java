package fr.univangers.isve.isvesolveur;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class IsveSolveurApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(IsveSolveurApplication.class, args);
	}

}
