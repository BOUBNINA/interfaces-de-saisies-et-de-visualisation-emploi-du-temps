package fr.univangers.isve.isvesolveur.Service;

import java.io.IOException;

public class InvalidFileTypeException extends IOException
{

	private static final long serialVersionUID = 1L;

	public InvalidFileTypeException(String path, String acceptedTypeMask) 
    {
		super(
            "File type "+path+" does not fall within the expected range: "+acceptedTypeMask);
    }
}