package fr.univangers.isve.isvesolveur.entities;


import java.io.Serializable;


public class ClasseSalle implements Serializable {
	

	private ClasseSalleId code_classe_salle;
	


	

	private Salle salle;
	

	private int penalty;

	public ClasseSalle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClasseSalle(Classe classe, Salle salle, int penalty) {
		super();

		this.salle = salle;
		this.code_classe_salle = new ClasseSalleId(classe.getCodeClasse(), salle.getCodeSalle());
		this.penalty = penalty;
	}

	public ClasseSalleId getCode_classe_salle() {
		return code_classe_salle;
	}

	public void setCode_classe_salle(ClasseSalleId code_classe_salle) {
		this.code_classe_salle = code_classe_salle;
	}




	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}
	
	
	
}
