package fr.univangers.isve.isvesolveur.Model;

import java.util.ArrayList;

public class Array2d<T> extends ArrayList<T> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int s1;
	private int s2;
	public Array2d(int s1, int s2){
		this.s1 = s1;
		this.s2 = s2;
		
	}
	public Array2d(){


	}
	public void setS1(int s1){this.s1 = s1;}
	public void setS2(int s2){this.s2 = s2;}
	public int getS1() {
		return s1;
	}
	public int getS2() {
		return s2;
	}
	
	public ArrayList<ArrayList<T>> toDoubleArray(){
		ArrayList<ArrayList<T>> res = new ArrayList<>();
		for(int i = 0; i<s1;i++) {
			res.add(new ArrayList<>());
			for(int j =0; j< s2;j++) {
				res.get(res.size()-1).add(this.get(i*s2+j));
			}
		}
		return res;
	}
	
}
