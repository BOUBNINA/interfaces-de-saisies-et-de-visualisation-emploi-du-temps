package fr.univangers.isve.isvesolveur.Service;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.univangers.isve.isvesolveur.Model.Array1d;
import fr.univangers.isve.isvesolveur.Model.Array2d;
import fr.univangers.isve.isvesolveur.Model.Creneau;
import fr.univangers.isve.isvesolveur.Model.Seance;
import fr.univangers.isve.isvesolveur.entities.*;
import fr.univangers.isve.isvesolveur.params.Params;
import org.apache.tomcat.jni.File;


public class Parseur implements PropertyChangeListener{

	// TODO initialisation de la date
	private LocalDateTime initDate;
	private LocalTime creneau;

	static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

	private ArrayList<Salle> BDDSalles;
	private ArrayList<Classe> BDDClasses;

	private DznFile fileIn;
	private DznFile fileOut;

	private ArrayList<ArrayList<Integer>> classes;
	private ArrayList<ArrayList<Integer>> time_weeks;
	private ArrayList<ArrayList<Integer>> time_days ;
	private ArrayList<Integer> time_starts;
	private ArrayList<Integer> time_length;
	private Array1d<Integer> x_room;
	private Array1d<Integer> x_time;


	private final PropertyChangeSupport support = new PropertyChangeSupport(this);



	public void addPropertyChangeListener(String property, PropertyChangeListener listener) {
		support.addPropertyChangeListener( property, listener);
	}
	
	public Parseur(Scanneur sc, Etape etape) throws IOException {
		sc.addPropertyChangeListener("file",this);
		this.BDDClasses = new ArrayList<>();
		this.BDDSalles = new ArrayList<>(etape.getDepartement().getSalles());
		for(UniteEnseignement ue : etape.getUniteEnseignements()){
			for(Matiere matiere: ue.getMatieres()){
				ArrayList<Classe> classe = new ArrayList<Classe>(matiere.getClasses());
				Collections.sort(classe, new Comparator<Classe>() {
					@Override
					public int compare(Classe t1, Classe t2) {
						return t1.getTypeSeance().getCodeTypeSeance().compareTo(t2.getTypeSeance().getCodeTypeSeance());
					}
				});
				for(Classe c : classe){
					System.out.println(c.getCodeClasse());
					this.BDDClasses.add(c);
				}
			}
		}



		this.fileIn = new DznFile(Params.getInstance().getValue("folder")+"L1_MPCIE.dzn");
		// TODO pour le moment la date initiale et la durée d'un créneau sont défini en dure dans le code.
		this.initDate = LocalDateTime.of(2020, Month.SEPTEMBER,1,8,0);
		this.creneau = LocalTime.of(1, 20);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		try {
			this.fileOut = new DznFile(((Path)evt.getNewValue()).toString());
		} catch (InvalidFileTypeException e) {
			e.printStackTrace();
		}
//		System.out.println(this.fileOut.getAbsolutePath());
		try {
			this.parse();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	//On récupère les données dont on a besoin du fichier dzn
	private void getFileInInfos() throws IOException {
		fileIn.load();

		time_weeks = fileIn.get("time_weeks");
		time_days = fileIn.get("time_days");
		time_starts = fileIn.get("time_start");

		classes = fileIn.get("classes");
		time_length = fileIn.get("time_length");


		
	}
	//on récupère les données dont on a besoin de la sortie du solveur
	private void getFileOutInfos() throws IOException, FileNotFoundException {

		fileOut.load();

		x_room = fileOut.get("x_room");
		x_time = fileOut.get("x_time");
		


	}
	
	private void createSeances() {
		ArrayList<Seance> seances = new ArrayList<Seance>();
		int currentClass = 0;
		//pour chaque classe, on créé des seances en fonction des données fourni par le solveur : Une séance par créneau horaire.
		for(int i =0; i<classes.size();i++) {
			for(int z = 0; z<classes.get(i).size(); z++) {
				ArrayList<Integer> tmp_time_weeks = time_weeks.get(x_time.get(currentClass)-1);
				ArrayList<Integer> tmp_time_days = time_days.get(x_time.get(currentClass )-1);

				for(int j = 0; j<tmp_time_weeks.size();j++) {

					for(int k = 0; k<tmp_time_days.size();k++) {
						Seance s = new Seance();
						s.codeSalle = this.BDDSalles.get(this.x_room.get(currentClass)-1).getCodeSalle();
						s.codeClasse = this.BDDClasses.get(currentClass).getCodeClasse();

						s.codeTypeSeance = this.BDDClasses.get(currentClass).getTypeSeance().getCodeTypeSeance();

						//on créé un objet datetime en focntion des données minizinc et de la date de début du calendrier
						s.dateSeance=initDate
								.plusDays((tmp_time_weeks.get(j)-1) * 7 + tmp_time_days.get(k)-1)
								.plusMinutes((this.creneau.getMinute() + this.creneau.getHour()*60)* (this.time_starts.get(x_time.get(currentClass )-1) -1)).toString();


						System.out.println("----------------------------------------------INFO HORRAIRES--------------------------------");
						System.out.println(this.creneau.getMinute());
						System.out.println(this.creneau.getHour());

						s.dureeSeance= LocalTime.of(0,0,0)
								.plusMinutes((this.creneau.getMinute() + this.creneau.getHour()*60) * (this.time_length.get(x_time.get(currentClass )-1))).toString();
						System.out.println((s.dureeSeance));
						seances.add(s); 

					}
				}
				currentClass +=1;
			}
		}

		System.out.println("la liste des séances");
		System.out.println(seances);



		support.firePropertyChange("seances", null,seances);
		
	}

	
	public void parse() throws IOException  {
		System.out.println("test");
		getFileInInfos();
		getFileOutInfos();
		
		
		createSeances();

		//System.out.println(this.seances);
		

	}
}
