package fr.univangers.isve.isvesolveur.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.io.Serializable;
import java.util.Collection;


public class Etape implements Serializable {
	

	private String codeEtape;
	private String libelleEtape;

	private Departement departement;
	
	private Collection<Groupe> groupes;
	private Collection<UniteEnseignement> uniteEnseignements;
	
	public Etape() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Etape(String codeEtape, String libelleEtape, Departement departement) {
		super();
		this.codeEtape = codeEtape;
		this.libelleEtape = libelleEtape;
		this.departement = departement;
	}

	public String getCodeEtape() {
		return codeEtape;
	}

	public void setCodeEtape(String codeEtape) {
		this.codeEtape = codeEtape;
	}

	public String getLibelleEtape() {
		return libelleEtape;
	}

	public void setLibelleEtape(String libelleEtape) {
		this.libelleEtape = libelleEtape;
	}
	

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	

	public Collection<Groupe> getGroupes() {
		return groupes;
	}

	public void setGroupes(Collection<Groupe> groupes) {
		this.groupes = groupes;
	}
	

	public Collection<UniteEnseignement> getUniteEnseignements() {
		return uniteEnseignements;
	}

	public void setUniteEnseignements(Collection<UniteEnseignement> uniteEnseignements) {
		this.uniteEnseignements = uniteEnseignements;
	}

	@Override
	public String toString() {
		return "Etape [codeEtape=" + codeEtape + ", libelleEtape=" + libelleEtape + ", departement=" + departement
				+ ", groupes=" + groupes + ", uniteEnseignements=" + uniteEnseignements + "]";
	}	

}
