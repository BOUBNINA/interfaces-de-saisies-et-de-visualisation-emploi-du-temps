# Documentation d'installation Projet ISVE

### Prérequis

* Machine debian 10
* JAVA 11
* Toute les instructions doivent se faire en root
  <br> Pour activer l'utilisateur root utilisez la commande suivante
```BASH
su root
```

### Installation des outils

#### Git
    apt update
    apt install git

#### Maven
    apt install maven

#### Minizinc
    apt install snapd
    snap install minizinc --classic

#### Apache2
    apt install apache2

#### Clone le projet
```BASH
git clone https://gitlab.com/BOUBNINA/interfaces-de-saisies-et-de-visualisation-emploi-du-temps.git
```
Notez bien qu'il faut se positionner dans le répertoire du projet
```BASH
cd interfaces-de-saisies-et-de-visualisation-emploi-du-temps
```
**À partir de ce moment toutes les instructions deveront s'exécuter depuis ce répertoire, le répertoire du projet**


#### Mysql (MariaDB sur debian)
    apt install mariadb-server
Il faudra se connecter à MySQL et créer un nouveau utilisateur
```BASH
mysql -u root -h localhost -p
```
Le mot de passe par défaut est root

```SQL
mysql> CREATE USER 'isve-admin'@'localhost' IDENTIFIED BY 'isveojv-21';
```

Pour simplifier, on va donner tout les droit à l'utilisateur isve-admin:
```SQL
mysql> GRANT ALL PRIVILEGES ON *.* TO 'isve-admin'@'localhost';
```

Ensuite on va créer la base de données 
```SQL
mysql> CREATE DATABASE emploi_temps;
```
Initialisation de la base de donnée
```SQL
mysql> USE emploi_temps;
```
Initialisation de la base de donnée
```SQL
mysql> source init_database.sql;
```
Pour sortir de la console mysql on tape exit

### Compiler et lancer le projet

**Redémarez la machine et positionnez vous sur le répertoire courant**

#### Vérifier la version minizinc (nos tests ont été réalisés avec la version 2.5.3)

```BASH
minizinc --version
```

#### Build le projet

On lance un build avec maven
```BASH
mvn clean install
```
On lance nos 2 application JAVA en tâches de fonds
```BASH
java -jar isve-backend/target/isve-backend-0.0.1-SNAPSHOT.jar &
java -jar isve-solveur/target/isve-solveur-0.0.1-SNAPSHOT.jar &
```
Optionnel : On peut vérifier l'état des applications par les web service :
* http://localhost:10000/actuator/health (back-end)
* http://localhost:10001/actuator/health (solveur)

On déploie le front-end sur Apache2
```BASH
rm -r /var/www/html/*
cp -r isve-frontend/src/main/web/dist/isve-frontend/* /var/www/html/
```
On redémarre et appache
```BASH
systemctl restart apache2.service
```
Maintenant votre application est accessible par le lien: 

http://localhost

### Optionnel installer adminer pour visualiser l'emploi du temps
```BASH
apt install libapache2-mod-php php-mysql
cp adminer.php /var/www/html/
```
Voici lien pour accéder à adminer : http://localhost/adminer.php

### Un script Python pour générer la table programme:
Comme nous avons été contraint par le temps, nous avons mis directement les contraintes dans une table programme. Le script SQL de la création se génère via le programme python "generate_program.py".<br>
Il prend comme paramètre le nombre total de classes, le nombre de slot par jour, et le nombre de jours par semaine.





