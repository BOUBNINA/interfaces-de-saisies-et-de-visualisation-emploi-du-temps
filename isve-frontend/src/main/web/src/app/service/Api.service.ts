import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { catchError, retry, tap, map } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpErrorResponse,
  HttpEventType,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { Seance } from '../model/seance.model';
import { Faculte } from '../model/faculte.model';
import { Filiere } from '../model/filiere.model';
import { Salle } from '../model/salle.model';
import { Classe } from '../model/classe.model';
import { Etape } from '../model/etape.model';
import { ClasseSalle } from '../model/classeSalle.model';
import { Matiere } from '../model/matiere.model';

const API_URL = environment.apiUrl;
const API_URL_SOLVEUR = environment.apiURLSolveur;

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  /*
    to get:
    this apiService.getAllCours().pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<Cours[]>) => {
      this.cours = res.body;
    })
  */
  public postFile(fileToUpload: JSON) {
    const formData: FormData = new FormData();
    formData.append('fileKey', JSON.stringify(fileToUpload));
    return this.http
      .post(API_URL + 'upload', formData, {
        reportProgress: true,
        observe: 'events',
      })
      .pipe(retry(3), catchError(this.handleError));
  }

// Appelle le solveur en spécifiant le nom du fichier dzn 
// Retorune true ou false en fonction de la réussite 
  public launchSolveur(fileName: string) {
    const formData: FormData = new FormData();
    formData.append('fileName', fileName);
    return this.http
      .get(API_URL_SOLVEUR + 'callSolver', 
      {
        params:{
          fileName : fileName
        }
      })
      .pipe(retry(3), catchError(this.handleError));
  }


  // lance le scan en spécifiant le code de l'étape lié 
  public launchScan(codeEtape: string) {
    return this.http
      .get(API_URL_SOLVEUR + 'scan', 
      {
        params:{
          codeEtape : codeEtape
        }
      })
      .pipe(retry(3), catchError(this.handleError));
  }


  // Permet d'obtenir la liste des Seance pour un code Etape donné 
  // Retourne une liste de Seance 
  public getCours(etapeCode : string): Observable<any> {
    let params = new HttpParams();
   /* params = params.append('observe', 'response');
    params = params.append('formation',filiere);
    params = params.append('libelleGroupe',faculte);*/

    return this.http
      .get<Seance>(API_URL +"seancesEtape", {
        observe : 'response',
        params:{
          etape : etapeCode
        }
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

  //Permet d'obtenir la liste de toutes les facultés de la BDD
  public getFaculte(): Observable<any> {
    return this.http
      .get<Faculte>(API_URL + 'facultes', {
        observe: 'response'
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

  // Permet d'obtenir la liste de toutes les matières de la BDD 
  public getMatieres(): Observable<any> {
    return this.http
      .get<Matiere>(API_URL + 'matieres', {
        observe: 'response'
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

  // Permet d'obtenir la liste de toutes les Classes de la BDD 
  public getClasses(): Observable<any> {
    return this.http
      .get<Classe>(API_URL + 'classes', {
        observe: 'response'
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

  // Permet d'obtenir la liste des classes pour un code d'étape donné 
  public getClassesByCode(codeEtape : string): Observable<any> {
    return this.http
      .get<Classe>(API_URL + 'classesByEtape', {
        observe: 'response',
        params : {
          etape : codeEtape
        }
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

  // Permet d'obtenir la liste des classesSalles (correspondance) pour un code d'étape donné 
  public classeSallesByEtape(codeEtape : string): Observable<any> {
    return this.http
      .get<ClasseSalle>(API_URL + 'classeSallesByEtape', {
        observe: 'response',
        params : {
          etape : codeEtape
        }
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

  // Permet d'obtenir la liste des faculté pour un code d'étape donné 
  public getFaculteByEtape(codeEtape : string): Observable<any> {
    return this.http
      .get<Faculte>(API_URL + 'faculteByEtape', {
        observe: 'response',
        params : {
          etape : codeEtape
        }
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }


    // Permet d'obtenir la liste de toutes les salles de la BDD
  public getSalles(): Observable<any> {
    return this.http
      .get<Salle>(API_URL + 'typeSalles', {
        observe: 'response'
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }
// Permet d'obtenir la liste de toutes les salles à partir du code d'une faculté 
  public getEtape(faculte: Faculte): Observable<any> {
    return this.http
      .get<Filiere>(API_URL + 'etapesFaculte', {
        observe: 'response',
        params: {
          faculte: faculte.codeFaculte,
        },
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

// Permet d'obtenir l'étape à partir de son codeEtape
  public getEtapeByCode(code: string): Observable<any> {
    return this.http
      .get<Etape>(API_URL + 'etapeCode', {
        observe: 'response',
        params: {
          codeEtape: code
        },
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }

// Méthode qui appelle la génération du fichier DZN à partir d'une code étape
// Retourne un blob permettant de créer le fichier téléchargeable
  public callGenerateDZN(code : string): Observable<any> {
    return this.http
      .get<String>(API_URL_SOLVEUR + 'dzn', {
        observe : 'response',
        responseType: 'blob' as 'json',
        params: {
          codeEtape: code
        },
        
      })
      .pipe(
        retry(3),
        catchError(this.handleError),
        tap((res) => {
          console.log(res.headers.get('Link'));
        })
      );
  }




  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, `
      );
      console.error(error)
    }

    return throwError('Something bad happened; please try again later.');
  }
}
