import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../service/Api.service';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.css']
})
export class ConfigureComponent implements OnInit {

  faculte: string;
  filiere: string;

  constructor(private api: ApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.faculte = this.route.snapshot.paramMap.get('faculte').split("#")[0];
    this.filiere = this.route.snapshot.paramMap.get('filiere');
  }

  loadConfig(){

    
  }

}
