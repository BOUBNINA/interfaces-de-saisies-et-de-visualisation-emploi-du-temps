import { Component, OnInit,ViewChild } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TimeTableComponent} from '../time-table/time-table.component'
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
showFiller = false;
isDrawer = false
nomFaculte : String ="";
selectedDate = Date.now;

@ViewChild(TimeTableComponent) tableChild;

  linkFac : String
  nomFac : String

  constructor(private route : ActivatedRoute) { 
      this.nomFaculte = this.route.snapshot.paramMap.get("faculte");
  }

  ngOnInit(): void {
    this.linkFac = this.route.snapshot.paramMap.get('faculte')
    this.nomFac = this.route.snapshot.paramMap.get('faculte').split('#')[0]+'-'+this.route.snapshot.paramMap.get('faculte').split('#')[1]
   
  }

  toggleDrawer()
  {
    this.isDrawer = !this.isDrawer;
    console.log(this.isDrawer)
    this.tableChild.calendar.render();
  }

  dateIsChanged(event){
    console.log(event)
    this.selectedDate = event
    this.tableChild.calendar.gotoDate(event)
  }

  	
}
