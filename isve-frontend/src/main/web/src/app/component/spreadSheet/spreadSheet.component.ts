import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Spreadsheet } from 'dhx-spreadsheet';

@Component({
  selector: 'app-spreadSheet',
  templateUrl: './spreadSheet.component.html',
  styleUrls: ['./spreadSheet.component.css'],
})
export class SpreadSheetComponent implements OnInit, OnDestroy {
  @ViewChild('widget', { static: true }) container: ElementRef;
  spreadsheet: Spreadsheet;

  @Input() toolbar: string[];
  @Input() menu: boolean;
  @Input() editLine: boolean;
  @Input() rowsCount: number;
  @Input() colsCount: number;

  ngOnInit() {
    this.spreadsheet = new Spreadsheet(this.container.nativeElement, {
      toolbar: this.toolbar,
      menu: this.menu ?? false,
      editLine: this.editLine ?? true,
      rowsCount: this.rowsCount,
      colsCount: this.colsCount,
      autoFormat: true,
      exportModulePath: 'src/app/json2excel/js/worker.js',
    });
  }

  columnLetterToNumber(val) {
    var base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      i,
      j,
      result = 0;

    for (i = 0, j = val.length - 1; i < val.length; i += 1, j -= 1) {
      result += Math.pow(base.length, j) * (base.indexOf(val[i]) + 1);
    }

    return result;
  }

  saveAsCsv() {
    var res: string[] = [''];
    var lastCell = 1;
    var json = this.spreadsheet.serialize();
    json.forEach((obj) => {
      var rowNumber = parseInt(obj.cell.replace(/\D/g, ''));
      var colomnNumber = this.columnLetterToNumber(
        obj.cell.replace(/[^A-Z]+/g, '')
      );
      if (rowNumber > res.length) {
        res.push('');
        lastCell = 1;
      }

      for (lastCell; lastCell < colomnNumber; lastCell++) {
        res[res.length - 1] += ',';
      }
      res[res.length - 1] += obj.value;
    });
    return res;
  }

  save() {
    return this.spreadsheet.serialize();
  }

  load(file, extension) {
    this.spreadsheet.load(file, extension);
  }

  ngOnDestroy() {
    this.spreadsheet.destructor();
  }
}
