import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() color: string;
  @Input() link: string;
  @Input() queryParam;

  constructor(private router: Router) {}

  ngOnInit() {}
  ngAfterViewInit() {
    document.getElementById(
      this.title + '-' + this.subtitle
    ).style.backgroundColor = this.color ?? '#000000';
    console.log(
      document.getElementById(this.title + '-' + this.subtitle).style
        .backgroundColor
    );
  }
}
