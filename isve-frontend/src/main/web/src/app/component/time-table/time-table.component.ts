import { Component, OnInit, Input } from '@angular/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import { ApiService } from '../../service/Api.service';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import frLocale from '@fullcalendar/core/locales/fr';
import {
  Calendar,
  CalendarOptions,
  EventInput,
  ToolbarInput,
} from '@fullcalendar/core';
import listPlugin from '@fullcalendar/list';
import dayGridPlugin from '@fullcalendar/daygrid';

import 'bootstrap/dist/css/bootstrap.css';
import { Seance, toEventsInput } from '../../model/seance.model';
import { takeUntil } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.css'],
})
export class TimeTableComponent implements OnInit {
  calendar: Calendar; //calender.gotoDate(Date);
  options: CalendarOptions;

  //test
  cours: Seance[];
  faculte: string;
  filiere: string;
  @Input() grid_type: string;
  @Input() toolbar: ToolbarInput;

  constructor(private api: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.faculte = this.route.snapshot.paramMap.get('faculte').split("#")[0];
    this.filiere = this.route.snapshot.paramMap.get('filiere');
    var etapeCode = this.route.snapshot.paramMap
      .get('filiere')
      .split('#')[1];
    this.api
      .getCours(etapeCode)
      .subscribe((res: HttpResponse<Seance[]>) => {
        console.log(res);
        res.body.forEach((valeur,index) =>{
          /*if (valeur.classe.groupes ==null)
            res.body[index].classe.groupes = {numeroGroupe=0,libelleGroupe : "",numeroGroupeParent : 0};*/
        })
        this.cours = res.body;
        console.log("liste des cours");
        console.log(res.body);
        this.renderCalendar();
      });
  }

  renderCalendar(): void {
    this.options = {
      customButtons: {
        print: {
          bootstrapFontAwesome: 'fa-print',
          click: () => {
            window.print();
          },
        },
        ics: {
          bootstrapFontAwesome: 'fas fa-sync-alt',
          click: () => {
            var div = document.getElementById('icsdiv');
            if (div.classList.contains('show')) {
              $('icsdiv').modal('hide');

            } else {
              $('#icsdiv').modal('show');
            }
          },
        },
      },

      headerToolbar: this.toolbar ?? {
        start: 'prev,next today',
        center: 'title',
        end: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek print ics',
      },
      themeSystem: 'bootstrap',
      slotMinTime: '07:00:00',
      slotMaxTime: '20:00:00',
      initialView: this.grid_type ?? 'timeGridWeek',
      allDaySlot: false,
      weekends: true,
      eventMouseEnter: (info) => {
        $(info.el).tooltip('show');
      },

      eventMouseLeave: (info) => {
        $(info.el).tooltip('hide');
      },
      editable: true,
      eventDidMount: (info) => {
        $(info.el).tooltip({
          placement: 'right',
          title: `<b>Catégorie : </b>${
            info.event.extendedProps['categorie'] ?? ''
          }<br>
                    <b>Salle : </b>${
                      info.event.extendedProps['salle'] ?? ''
                    }<br>
                    <b>Matière : </b>${
                      info.event.extendedProps['matiere'] ?? ''
                    }<br>
                    <b>Groupe : </b>${
                      info.event.extendedProps['groupe'] ?? ''
                    }<br>
                    <b>Personnel : </b>${
                      info.event.extendedProps['personnel'] ?? ''
                    }<br>
                    <b>Remarque : </b>${
                      info.event.extendedProps['remarque'] ?? ''
                    }`,
          html: true,
        });
      },
      nowIndicator: true,
      events: toEventsInput(this.cours),
      titleFormat: {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        week: 'short',
      },
      height: 'auto',
      locales: [frLocale],
      locale: 'fr',

      plugins: [timeGridPlugin, bootstrapPlugin, listPlugin, dayGridPlugin],

      weekNumbers: false,

      aspectRatio: 1,
      contentHeight: 'auto',
      slotDuration: '00:30:00',
      slotLabelInterval: '01:00:00',
    };
    this.calendar = new Calendar(
      document.querySelector('full-calendar'),
      this.options
    );
    const mydate = new Date();
    mydate.setFullYear(2020);
    mydate.setMonth(9);
    mydate.setDate(1);
    this.calendar.gotoDate(mydate);
    this.calendar.render();
  }
}
