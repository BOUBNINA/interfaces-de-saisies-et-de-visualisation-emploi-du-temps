import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Faculte } from 'src/app/model/faculte.model';
import { ApiService } from 'src/app/service/Api.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css'],
})
export class AccueilComponent implements OnInit {
  faculte: Faculte[];

  palette = environment.palette;

  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getFaculte().subscribe((res: HttpResponse<Faculte[]>) => {
      this.faculte = res.body;
      this.faculte.forEach((element) => {
        console.log(element)
       // this.stringifiedFaculte.push(JSON.stringify(element));
      });
      console.log(res.body);
    });
    console.log(this.palette);
  }
}
