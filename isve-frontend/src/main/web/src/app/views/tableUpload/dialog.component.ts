
import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { TableUploadComponent } from './tableUpload.component';
import { ApiService } from '../../service/Api.service';
import { Etape } from '../../model/etape.model';
import { HttpResponse } from '@angular/common/http';
import { Faculte } from '../../model/faculte.model';




interface DialogData {
  isReussi: string;
  codeEtape : string;
 
}

@Component({
    selector: 'dialog-redirect',
    templateUrl: 'dialog-overview-example-dialog.html',
  })
  export class DialogRedirect {
  
    etape : Etape;
    faculte : Faculte;

    constructor(
      private api: ApiService,
      public dialogRef: MatDialogRef<DialogRedirect>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
  

      ngOnInit() {

        this.api
      .getEtapeByCode(this.data.codeEtape)
      .subscribe((res: HttpResponse<Etape>) => {
        this.etape = res.body;
        console.log(this.etape)

      });

      this.api.getFaculteByEtape(this.data.codeEtape).subscribe((res: HttpResponse<Faculte>) => {
        this.faculte = res.body;
        console.log(this.faculte)

      });

      }

    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }