import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { saveAs } from 'file-saver';
import { ActivatedRoute } from '@angular/router';
import { SpreadSheetComponent } from 'src/app/component/spreadSheet/spreadSheet.component';
import { ApiService } from 'src/app/service/Api.service';
import {environment} from "../../../environments/environment";
import { Classe } from '../../model/classe.model';
import { Correspondance } from '../../model/Correspondance.model';
import { Salle } from '../../model/salle.model';
import { Etape } from '../../model/etape.model';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DialogRedirect} from './dialog.component'
import { ClasseSalle } from '../../model/classeSalle.model';
import { Matiere } from '../../model/matiere.model';

declare var $: any;



class ClasseMatiere
{
  codeClasse : string;
  matiere : string;

  constructor(code : string,matiere : string)
  {
    this.codeClasse=code;
    this.matiere=matiere
  }
}


@Component({
  selector: 'app-tableUpload',
  templateUrl: './tableUpload.component.html',
  styleUrls: ['./tableUpload.component.css'],
})
export class TableUploadComponent implements OnInit {
  palette = environment.palette;
  @ViewChild(SpreadSheetComponent) spreadsheet: SpreadSheetComponent;
  fileToUpload: File = null;
  progress: number = 0;
  label: string = 'sélectionner un fichier...';
  newName: string = '';
  modalShow = false;
  salles: Salle[]; //Liste des salles disponible
  classes : Classe[];
  spinner : boolean = false; //Spinner de la generation du DZN
  nbSallePref : Array<number> = []; //Listes des salle preferes choisis
  nbCoresspondance : Array<number> = []; //Nombre de coresspondances
  lesSallesChoisis : Array<String> = new Array();
  lesCorresChoisis : Array<Correspondance> = new Array();
  filliere : string = ""; //nom de la filliere
  isDznGenerate : boolean = false; // Variable pour savoir si le dzn a été généré
  isDZNCliqued : boolean = false; //Varibale pour savoir si le bouton dzn a été cliqué pour généré le spinner
  spinnerSolveur : boolean = false; //Spinner du solveur
  blob : Blob; // Blob pour la generation du fichier DZN

  matieres : Matiere[];
  classeMatieres : Array<ClasseMatiere> = [];

  etape : Etape;
  correspondances : ClasseSalle[];


  constructor(private api: ApiService, private snackBar: MatSnackBar,private route: ActivatedRoute,public dialog: MatDialog) {}

 async ngOnInit() {
    this.nbSallePref.push(0);
    this.nbCoresspondance.push(0);

    // this.filliere= this.route.snapshot.paramMap
    //   .get('filliere')
      this.filliere="1"


      this.api.getEtapeByCode(this.filliere).subscribe((res: HttpResponse<Etape>) => {
        this.etape = res.body
        console.log(this.etape);
      });


//récupèration des classes
      this.api
      .getClassesByCode(this.filliere)
      .subscribe((res: HttpResponse<Classe[]>) => {
        this.classes = res.body;
        console.log(this.classes)

      });

      //récupèration des classesSalles
      this.api
      .classeSallesByEtape(this.filliere)
      .subscribe((res: HttpResponse<ClasseSalle[]>) => {
        this.correspondances = res.body;
        console.log(this.correspondances)

      });

      //récupération des matières
      this.api.getMatieres().subscribe((res: HttpResponse<Matiere[]>) => {
        this.matieres = res.body;

        this.matieres.forEach(matiere =>{
          matiere.classes.forEach(classe =>{
            var stringTypeSeance = classe.typeSeance.libelleTypeSeance
            switch (classe.typeSeance.libelleTypeSeance)
            {
              case "Cours Magistral" :
              stringTypeSeance = "CM"
              break;

              case "Travaux Pratiques" :
              stringTypeSeance = "TP"
              break;

              case "Travaux Dirigés" :
              stringTypeSeance = "TD"
              break;
            }


              this.classeMatieres.push(new ClasseMatiere(classe.codeClasse, matiere.libelleMatiere+' - '+stringTypeSeance))
          })
          ;
        })

      });



  }


  //Event quand on choisi une valeur de la selection des salle
  changeSalle(nb,value)
  {


    if (this.lesSallesChoisis.length==1&&value=="")
    {
      this.lesSallesChoisis.pop()
    }
    else if (this.lesSallesChoisis.length==nb)
    {
      this.lesSallesChoisis.push(value);

    }
    else
    {
      this.lesSallesChoisis[nb]=value
    }

  }

  changeCorrespondance(nb,valueSalle,valueCours)
  {
      if (valueSalle=="" && valueCours=="" &&  this.nbCoresspondance.length==1)
      {
          this.lesCorresChoisis.pop();
      }

      else if (this.lesCorresChoisis.length==nb)
      {

          var cores = new Correspondance();
          cores.cours=valueCours;
          cores.salle=valueSalle

          this.lesCorresChoisis.push(cores);


      }


      else
      {
        this.lesCorresChoisis[nb].cours=valueCours;
        this.lesCorresChoisis[nb].salle=valueSalle;
      }

  }




  incrementeNumber()
  {
    this.nbSallePref.push(this.nbSallePref.length);
  }


  // Se délcenche à l'appui du bouton download, et permet de télécharger le fichier DZN
  donwloadFile()
  {

    saveAs(this.blob,this.etape.libelleEtape+'.dzn');
  }



  containsSalle(valeur)
  {
    var verif = false;

    for (var i=0;i<this.lesCorresChoisis.length;i++)
    {
      if (this.lesCorresChoisis[i].salle==valeur)
      {
        verif = true;
      }
    }
    return verif
  }


  // Methode appeler quand on clique sur le bouton "croix" des salles préféré, elle va décrémenté le nombre de salles et
  // supprimer la salle de la liste des salles préféré
  decrementeNumber()
  {
    this.nbSallePref.pop();
    this.nbSallePref.length != this.lesSallesChoisis.length && this.lesSallesChoisis.pop();
  }

  // Event appui du bouton "ajout correspondace", créer une nouvelle ligne de correspondance
  incrementCorres()
  {
    this.nbCoresspondance.push(this.nbCoresspondance.length);

  }

  decrementeCorres()
  {
    this.nbCoresspondance.pop();
    this.nbCoresspondance.length!=this.lesCorresChoisis.length && this.lesCorresChoisis.pop();

    if (this.lesCorresChoisis.length==1 && this.lesCorresChoisis[0].salle=="" && this.lesCorresChoisis[0].cours == "")
    {
      this.nbCoresspondance.pop();
      this.lesCorresChoisis.pop();
    }
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.label = this.fileToUpload.name;
    var url = URL.createObjectURL(this.fileToUpload);
    this.spreadsheet.load(url, this.label.split('.')[1]);
  }

  reset() {
    this.fileToUpload = null;
    this.label = 'Sélectionner un fichier...';
    this.progress = 0;
  }

  getFile() {
    var text = this.spreadsheet.saveAsCsv();
    var blob = new Blob([text.join('\n')], { type: 'text/csv;charset=utf-8;' });
    var a = document.createElement('a');
    a.download = this.label;
    a.href = URL.createObjectURL(blob);
    a.dataset.downloadurl = [
      'text/csv;charset=utf-8;',
      a.download,
      a.href,
    ].join(':');
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    console.log(text);
  }

  showHideModal() {
    var div = document.getElementById('icsdiv');
    if (div.classList.contains('show')) {
      $('#icsdiv').modal('hide');
      console.log('hide');
    } else {
      $('#icsdiv').modal('show');
      console.log('show');
    }
  }

  createTableau() {
    this.showHideModal();
    this.reset();

    this.fileToUpload = new File([], this.newName + '.csv');
    this.label = this.fileToUpload.name;
    var url = URL.createObjectURL(this.fileToUpload);
    this.spreadsheet.load(url, this.label.split('.')[1]);
    this.newName = '';

  }



  //Appelle de la génération du DZN lors de l'appui bouton
  generateDZN()
  {
      this.spinner = true;

       this.api.callGenerateDZN(this.filliere).subscribe((res) => {
        console.log(res.body);
        this.spinner = false;
        this.isDZNCliqued = true;
        this.isDznGenerate = (res.body!="");
        this.blob = new Blob([res.body], {type: "text/plain;charset=utf-8"}); //création d'un blob permettant de télécharger le fichier
        this.blob

       });



  }


  //méthode de lancemennt du solveur
  launchSolveur()
  {

    this.spinnerSolveur = true,
    console.log(this.filliere)

    this.api.launchScan(this.filliere).subscribe((res) =>{
      this.api.launchSolveur("L1_MPCIE").subscribe((res) => {
        this.spinnerSolveur = false;
          const dialogRef = this.dialog.open(DialogRedirect, {

            data: {isReussi: res,codeEtape : this.filliere}
          });

          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
           // this.animal = result;
          });
        });
    })




  }

//vérification que tous les champs sont bien remplis
  canGenerateDZN()
  {
    var valide = true;

    for (var i=0;i<this.lesCorresChoisis.length;i++)
    {
        if (this.lesCorresChoisis[i].salle == "" || this.lesCorresChoisis[i].cours =="")
        {
          valide = false;
        }
    }

    for (var i=0;i<this.lesSallesChoisis.length;i++)
    {
        if (this.lesSallesChoisis[i] == "")
        {
          valide = false;
        }
    }

    return valide

  }

  sendFile() {
    this.api.postFile(this.spreadsheet.save()).subscribe(
      (data: HttpEvent<object>) => {
        if (data.type === HttpEventType.Response) {
          this.fileToUpload = null;
          this.progress = 0;
          this.label = 'sélectionner un fichier...';
          this.snackBar.open('succès', 'ok', { duration: 2000 });
        }
        if (data.type === HttpEventType.UploadProgress) {
          this.progress = Math.round((data.loaded * 100) / data.total);
        }
      },
      (error) => {
        this.snackBar.open('échec', 'ok', { duration: 2000 });
      }
    );
  }
}
