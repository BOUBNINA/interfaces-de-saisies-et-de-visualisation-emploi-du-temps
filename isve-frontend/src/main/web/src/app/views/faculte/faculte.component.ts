import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { Filiere } from 'src/app/model/filiere.model';
import { ApiService } from 'src/app/service/Api.service';
import { ActivatedRoute } from '@angular/router';
import { Faculte } from 'src/app/model/faculte.model';
import { Etape } from '../../model/etape.model';

@Component({
  selector: 'app-faculte',
  templateUrl: './faculte.component.html',
  styleUrls: ['./faculte.component.css'],
})
export class FaculteComponent implements OnInit {
  faculte: Faculte;
  etape: Etape[]; //= ['aaaaa', 'abbbc', 'azzvcs', 'sddsfdsvqt', 'fSFDFSFy'];

  etapeFiltered: Etape[];

  constructor(private api: ApiService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.faculte = new Faculte();
    this.faculte.nomFaculte = this.route.snapshot.paramMap
      .get('faculte')
      .split('#')[0];

    this.faculte.lieuFaculte = this.route.snapshot.paramMap
      .get('faculte')
      .split('#')[1];
    this.faculte.codeFaculte = this.route.snapshot.paramMap
      .get('faculte')
      .split('#')[2];
    console.log(this.faculte);
    this.api
      .getEtape(this.faculte)
      .subscribe((res: HttpResponse<Etape[]>) => {
        this.etape = res.body;
        this.etapeFiltered = this.etape;
        console.log(res.body);
      });
    //TODO api
  }

  etapeFilter(value) {
    this.etapeFiltered = this.etape.filter((x) =>
      x.libelleEtape.match(`^${value}.*`)
    );
  }
}
