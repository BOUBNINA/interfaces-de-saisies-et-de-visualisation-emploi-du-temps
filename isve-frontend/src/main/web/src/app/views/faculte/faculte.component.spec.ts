/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FaculteComponent } from './faculte.component';

describe('FaculteComponent', () => {
  let component: FaculteComponent;
  let fixture: ComponentFixture<FaculteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FaculteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FaculteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
