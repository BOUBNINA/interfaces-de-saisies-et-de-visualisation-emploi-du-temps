import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TimeTableComponent } from './component/time-table/time-table.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './service/Api.service';
import { FaculteComponent } from './views/faculte/faculte.component';
import { FormsModule } from '@angular/forms';
import { AccueilComponent } from './views/accueil/accueil.component';
import { CardComponent } from './component/card/card.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { EmploiDuTempsComponent } from './views/emploi-du-temps/emploi-du-temps.component';
import { HeaderComponent } from './header/header.component';
import { TableUploadComponent } from './views/tableUpload/tableUpload.component';
import { SpreadSheetComponent } from './component/spreadSheet/spreadSheet.component';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ConfigureComponent } from './configure/configure.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { DialogRedirect } from './views/tableUpload/dialog.component';

@NgModule({
  declarations: [	
    FaculteComponent,
    AccueilComponent,
    TimeTableComponent,
    CardComponent,
    AppComponent,
    SidebarComponent,
    EmploiDuTempsComponent,
    HeaderComponent,
    TableUploadComponent,
    SpreadSheetComponent,
    ConfigureComponent,
    DialogRedirect
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FullCalendarModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatCardModule,
    FormsModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatInputModule
    
  ],
  providers: [ApiService],
  bootstrap: [AppComponent],
})
export class AppModule {}
