import { Matiere } from "./matiere.model";
import { Groupe } from "./groupe.model";
import { TypeSeance } from "./typeSeance.model";

export class Classe {
    codeClasse : string;
    groupes : Groupe[];
    typeSeance : TypeSeance
}
//faculte : string
