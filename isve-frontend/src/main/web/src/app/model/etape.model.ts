import {Departement} from "./departement.model";

export class Etape {
    codeEtape : string
    libelleEtape : string;
    departement : Departement;
}
//faculte : string
