import {Faculte} from "./faculte.model";
import { Salle } from "./salle.model";

export class Departement {
    codeDepartement : string;
    nomDepartement : Faculte;
    salles : Salle[];
}
//faculte : string
