import {Faculte} from "./faculte.model";

export class Filiere {
  libelleFormation: string;
  codeFormation : string;
  faculte : Faculte;
}
//faculte : string
