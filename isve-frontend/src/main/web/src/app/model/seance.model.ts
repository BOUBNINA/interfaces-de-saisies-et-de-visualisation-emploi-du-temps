import { EventInput } from '@fullcalendar/core';
import { environment } from '../../environments/environment';
import { Classe } from './classe.model';
import { Groupe } from './groupe.model';

class Enseignant {
  matriculeEnseignant: string;
  nomEnseignant : string;
  prenomEnseignant : string;
}

export class Seance {
  classe : {
    codeClasse : string,

    groupes : Groupe[];


  };

  libelleMatiere : string;


  typeCours: {
    libelleTypeSeance: string;
  };
  salle: {
    codeSalle: string;
    libelleSalle : string
  };
  /*commentaire: {
    libelleCommentaire : string;
  };*/

  dateSeance: string;
  dureeSeance: string;
  enseignants: Enseignant[];

  /*categorie: string;
  salle: string;
  matiere: string;
  groupe: string;
  personnel: string;
  remarque: string;

  start: string;
  end: string; // or duration*/
}

function stringToMs(time: string): number {
  var hms: string[] = time.split(':');
  var ms = parseInt(hms[0]) * 3600000 +
    parseInt(hms[1]) * 60000
  return ms
}

export function toEventInput(seance: Seance): EventInput {

  var debut = new Date(seance.dateSeance);

    var stringGroupe = ""

     seance.classe.groupes.forEach(valeur =>{
      stringGroupe+=valeur.libelleGroupe+","
     })


    switch (seance.typeCours.libelleTypeSeance)
    {
      case "Cours Magistral" :
      seance.typeCours.libelleTypeSeance = "CM"
      break;

      case "Travaux Pratiques" :
      seance.typeCours.libelleTypeSeance = "TP"
      break;

      case "Travaux Dirigés" :
      seance.typeCours.libelleTypeSeance = "TD"
      break;
    }

  var end = new Date(debut.getTime() + stringToMs(seance.dureeSeance));

  var professeurs = '';
  seance.enseignants.forEach((prof) => {
    professeurs +=
      prof.nomEnseignant.toUpperCase() +' '+ prof.prenomEnseignant.toUpperCase()  + ', ';
  });
  return {
    title:
    seance.typeCours.libelleTypeSeance +
      ' - ' +
      seance.salle.libelleSalle +
      ' - ' +
      seance.libelleMatiere +
      ' - ' +
      stringGroupe +
      ' - ' +
      professeurs,
    start: debut,

    end: end,
    extendedProps: {
      categorie: seance.typeCours.libelleTypeSeance,
      salle: seance.salle.libelleSalle,
      matiere: seance.libelleMatiere,
      groupe: stringGroupe,
      personnel: professeurs,
      //remarque: seance.commentaire.libelleCommentaire,
    },

    backgroundColor: environment.colors[seance.typeCours.libelleTypeSeance], // TODO find in settings by type
    textColor: '#000000',
    borderColor: environment.colors[seance.typeCours.libelleTypeSeance],
  };
}

export function toEventsInput(ev: Seance[]): EventInput[] {
  var events: EventInput[] = [];
  ev.forEach((cours) => {
    events.push(toEventInput(cours));
  });
  return events;
}

export function toGoogleCalendar(ev: Seance[]): void {}
