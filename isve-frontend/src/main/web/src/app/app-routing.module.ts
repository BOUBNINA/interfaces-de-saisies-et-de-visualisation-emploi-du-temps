import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaculteComponent } from './views/faculte/faculte.component';
import { AccueilComponent } from './views/accueil/accueil.component';
import { EmploiDuTempsComponent } from './views/emploi-du-temps/emploi-du-temps.component';
import { TableUploadComponent } from './views/tableUpload/tableUpload.component';
import { ConfigureComponent } from './configure/configure.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'faculte/:faculte', component: FaculteComponent },
  { path: 'admin', component: TableUploadComponent },
  { path: 'faculte/:faculte/:filiere/edt', component: EmploiDuTempsComponent },
  { path: 'faculte/:faculte/:filiere/configure', component: ConfigureComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
