package fr.univangers.isve.dto;

import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSeance;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Seance implements Serializable {
	public Long codeClasse;

	public Long codeTypeSeance;

	public String codeSalle;

	public String dateSeance;

	public String dureeSeance;
	public Seance() {
		super();
	}
	public Seance(Long codeClasse, String codeSalle, Long codeTypeSeance, String dateSeance, String dureeSeance) {
		super();
		this.codeClasse = codeClasse;
		this.codeSalle = codeSalle;
		this.codeTypeSeance = codeTypeSeance;
		this.dateSeance = dateSeance;
		this.dureeSeance = dureeSeance;
	}

	public Long getCodeClasse() {
		return codeClasse;
	}

	public void setCodeClasse(Long codeClasse) {
		this.codeClasse = codeClasse;
	}

	public Long getCodeTypeSeance() {
		return codeTypeSeance;
	}

	public void setCodeTypeSeance(Long codeTypeSeance) {
		this.codeTypeSeance = codeTypeSeance;
	}

	public void setCodeSalle(String codeSalle) {
		this.codeSalle = codeSalle;
	}

	public String getCodeSalle() {
		return codeSalle;
	}

	public String getDateSeance() {
		return dateSeance;
	}

	public void setDateSeance(String dateSeance) {
		this.dateSeance = dateSeance;
	}

	public String getDureeSeance() {
		return dureeSeance;
	}

	public void setDureeSeance(String dureeSeance) {
		this.dureeSeance = dureeSeance;
	}

	@Override
	public String toString() {
		return "Seance [codeClasse=" + codeClasse + ", codeSalle=" + codeSalle + ", codeTypeSeance=" + codeTypeSeance + ", dateSeance=" + dateSeance + ", dureeSeance=" + dureeSeance + "]";
	}
	
	
}


