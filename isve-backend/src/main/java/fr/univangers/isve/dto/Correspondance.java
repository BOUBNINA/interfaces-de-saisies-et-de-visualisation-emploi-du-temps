package fr.univangers.isve.dto;

import java.io.Serializable;

public class Correspondance implements Serializable {
	private Long codeClasse;
	private String codeSalle;
	public Correspondance() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Correspondance(Long codeClasse, String codeSalle) {
		super();
		this.codeClasse = codeClasse;
		this.codeSalle = codeSalle;
	}
	public Long getCodeClasse() {
		return codeClasse;
	}
	public void setCodeClasse(Long codeClasse) {
		this.codeClasse = codeClasse;
	}
	public String getCodeSalle() {
		return codeSalle;
	}
	public void setCodeSalle(String codeSalle) {
		this.codeSalle = codeSalle;
	}
	@Override
	public String toString() {
		return "Correspondance [codeClasse=" + codeClasse + ", codeSalle=" + codeSalle + "]";
	}
	
}
