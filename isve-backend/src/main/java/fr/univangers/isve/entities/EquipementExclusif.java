package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="equipement_exclusif")
@DiscriminatorValue("EE")
public class EquipementExclusif extends Equipement{
	
	@ManyToOne
	@JoinColumn(name="code_ue")
	UniteEnseignement uniteEnseignement;
	
	public EquipementExclusif(String nomEquipement, UniteEnseignement ue) {
		super(nomEquipement);
		uniteEnseignement = ue;
	}

	public UniteEnseignement getUniteEnseignement() {
		return uniteEnseignement;
	}

	public void setUniteEnseignement(UniteEnseignement uniteEnseignement) {
		this.uniteEnseignement = uniteEnseignement;
	}

}
