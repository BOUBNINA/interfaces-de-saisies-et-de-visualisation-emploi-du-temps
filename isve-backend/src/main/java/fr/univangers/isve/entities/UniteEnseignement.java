package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="unite_enseignement")
public class UniteEnseignement implements Serializable{
	
	@Id
	@Column(name="code_ue")
	private String codeUE;
	@Column(name="nom_ue")
	private String nom_UE;
	@Column(name="ects_ue")
	private int ECTS_UE;
	@Column(name="volume_horaire_ue")
	private String volumeHoraire_UE;
	@Column(name="effectif_prevu_ue")
	private int effectifPrevu_UE;
	@Column(name="charge_prevu_cm_ue")
	private int chargePrevuCM_UE;
	@Column(name="charge_prevu_tp_ue")
	private int chargePrevuTP_UE;
	@ManyToOne
	@JoinColumn(name="code_periode")
	private Periode periode;
	@ManyToMany
	@JoinTable(name = "UE_etape",
			   joinColumns = 
		   		@JoinColumn(name="code_ue", referencedColumnName = "code_ue"),
		   inverseJoinColumns = 
		   		@JoinColumn(name="code_etape", referencedColumnName = "code_etape")
	)
	private Collection<Etape> etapes;
	@OneToMany(mappedBy = "uniteEnseignement",fetch = FetchType.LAZY)
	private Collection<Matiere> matieres;
	
	public UniteEnseignement() {
		super();
	}
	
	public UniteEnseignement(String code_UE, String nom_UE, int ECTS_UE, String volumeHoraire_UE, int effectifPrevu_UE, int chargePrevuCM_UE,
			int chargePrevuTP_UE, Periode periode) {
		super();
		this.codeUE = codeUE;
		this.nom_UE = nom_UE;
		this.ECTS_UE = ECTS_UE;
		this.volumeHoraire_UE = volumeHoraire_UE;
		this.effectifPrevu_UE = effectifPrevu_UE;
		this.chargePrevuCM_UE = chargePrevuCM_UE;
		this.chargePrevuTP_UE = chargePrevuTP_UE;
		this.periode = periode;
	}

	
	public String getCodeUE() {
		return codeUE;
	}

	public void setCodeUE(String codeUE) {
		this.codeUE = codeUE;
	}

	public String getNom_UE() {
		return nom_UE;
	}

	public void setNom_UE(String nom_UE) {
		this.nom_UE = nom_UE;
	}
	@JsonIgnore
	public int getECTS_UE() {
		return ECTS_UE;
	}

	public void setECTS_UE(int eCTS_UE) {
		ECTS_UE = eCTS_UE;
	}
	@JsonIgnore
	public String getVolumeHoraire_UE() {
		return volumeHoraire_UE;
	}

	public void setVolumeHoraire_UE(String volumeHoraire_UE) {
		this.volumeHoraire_UE = volumeHoraire_UE;
	}
	@JsonIgnore
	public int getEffectifPrevu_UE() {
		return effectifPrevu_UE;
	}

	public void setEffectifPrevu_UE(int effectifPrevu_UE) {
		this.effectifPrevu_UE = effectifPrevu_UE;
	}
	@JsonIgnore
	public int getChargePrevuCM_UE() {
		return chargePrevuCM_UE;
	}

	public void setChargePrevuCM_UE(int chargePrevuCM_UE) {
		this.chargePrevuCM_UE = chargePrevuCM_UE;
	}
	@JsonIgnore
	public int getChargePrevuTP_UE() {
		return chargePrevuTP_UE;
	}

	public void setChargePrevuTP_UE(int chargePrevuTP_UE) {
		this.chargePrevuTP_UE = chargePrevuTP_UE;
	}
	@JsonIgnore
	public Periode getPeriode() {
		return periode;
	}

	public void setPeriode(Periode periode) {
		this.periode = periode;
	}
	

	public Collection<Matiere> getMatieres() {
		return matieres;
	}

	public void setMatieres(Collection<Matiere> matieres) {
		this.matieres = matieres;
	}

	@JsonIgnore
	public Collection<Etape> getEtapes() {
		return etapes;
	}

	public void setEtapes(Collection<Etape> etapes) {
		this.etapes = etapes;
	}
	
	
	
}