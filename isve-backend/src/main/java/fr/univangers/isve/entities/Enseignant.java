package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name = "enseignant")
public class Enseignant implements Serializable {
	@Id
	@Column(name="matricule_enseignant", length=15)
	private String matriculeEnseignant;
	@Column(name="nom_enseignant", length=45)
	private String nomEnseignant;
	@Column(name="prenom_enseignant", length=60)
	private String prenomEnseignant;
	@Column(name="email_enseignant", length=100)
	private String emailEnseignant;
	@Column(name="obligation_theorique_enseignent")
	private int ObligationTheoriqueEnseignent;
	@ManyToOne
	@JoinColumn(name="code_statut")
	private StatutEnseignant statutEnseignant;
	@ManyToOne
	@JoinColumn(name="code_departement")
	private Departement departement;
	@OneToMany(mappedBy="enseignant")
	private Collection<Agenda> agendas;
	@ManyToMany(mappedBy = "enseignants")
	private Collection<Seance> seances;
	@OneToMany(mappedBy = "enseignant")
	private Collection<Classe> classes;
	
	
	
	public Enseignant() {
		super();
	}
	
	
	public Enseignant(String matriculeEnseignant, String nomEnseignant, String prenomEnseignant,
			String emailEnseignant, int volumeHoraireEnseignent,StatutEnseignant statut,Departement departement) {
		
		super();
		this.matriculeEnseignant = matriculeEnseignant;
		this.nomEnseignant = nomEnseignant;
		this.prenomEnseignant = prenomEnseignant;
		this.emailEnseignant = emailEnseignant;
		this.ObligationTheoriqueEnseignent = volumeHoraireEnseignent;
		this.statutEnseignant = statut;
		this.departement = departement;
	}
	@JsonIgnore
	public String getEnseignant() {
		return this.nomEnseignant.toUpperCase() + " " + this.prenomEnseignant;
	}
	
	public void setEnseignantPrenom(String prenom) {
		this.prenomEnseignant = prenom;
	}
	
	public void setEnseignantNom(String nom) {
		this.nomEnseignant = nom;
	}
	

	public String getMatriculeEnseignant() {
		return matriculeEnseignant;
	}

	public void setMatriculeEnseignant(String matriculeEnseignant) {
		this.matriculeEnseignant = matriculeEnseignant;
	}

	public String getNomEnseignant() {
		return nomEnseignant;
	}

	public String getPrenomEnseignant() {
		return prenomEnseignant;
	}

	@JsonIgnore
	public String getEmailEnseignant() {
		return emailEnseignant;
	}
	
	public void setEmailEnseignant(String emailEnseignant) {
		this.emailEnseignant = emailEnseignant;
	}

	@JsonIgnore
	public StatutEnseignant getStatutEnseignant() {
		return statutEnseignant;
	}
	
	public void setStatutEnseignant(StatutEnseignant statutEnseignant) {
		this.statutEnseignant = statutEnseignant;
	}
	
	@JsonIgnore
	public Departement getDepartement() {
		return departement;
	}
	@JsonSetter
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	
	@JsonIgnore
	public Collection<Seance> getSeance() {
		return seances;
	}
	@JsonSetter
	public void setSeance(Collection<Seance> cours) {
		this.seances = cours;
	}
	
	@JsonIgnore
	public Collection<Agenda> getAgendas() {
		return agendas;
	}
	@JsonSetter
	public void setAgendas(Collection<Agenda> agendas) {
		this.agendas = agendas;
	}

	@JsonIgnore
	public Collection<Classe> getClasses() {
		return classes;
	}

	@JsonSetter
	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}
	
		
}
