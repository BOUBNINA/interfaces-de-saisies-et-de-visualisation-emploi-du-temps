package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="groupe")
public class Groupe implements Serializable {
	@Id 
	@GeneratedValue
	@Column(name="numero_groupe")
	private Long numeroGroupe;
	@Column(name="libelle_groupe", length=30)
	private String libelleGroupe;
	@Column(name = "code_groupe_parent")
	private Long numeroGroupeParent;
	@ManyToOne
	@JoinColumn(name="code_etape")
	private Etape etape;
	@ManyToMany(mappedBy = "groupes")
	private Collection<Classe> classes;
	@OneToMany(mappedBy="groupe", fetch=FetchType.LAZY)
	private Collection<Etudiant> etudiants;
	
	public Groupe() {
		super();
	}
	
	public Groupe(String libelleGroupe, Etape etape) {
		super();
		this.libelleGroupe = libelleGroupe;
		this.etape = etape;
	}
	

	public Long getNumeroGroupe() {
		return numeroGroupe;
	}

	public void setNumeroGroupe(Long numeroGroupe) {
		this.numeroGroupe = numeroGroupe;
	}
	
	
	public String getLibelleGroupe() {
		return libelleGroupe;
	}
	public void setLibelleGroupe(String libelleGroupe) {
		this.libelleGroupe = libelleGroupe;
	}

	public Long getNumeroGroupeParent() {
		return numeroGroupeParent;
	}

	@JsonIgnore
	public Etape getEtape() {
		return this.etape;
	}

	public void setEtape(Etape etape) {
		this.etape = etape;
	}
	
	@JsonIgnore
	public Collection<Classe> getClasses() {
		return classes;
	}
	@JsonSetter
	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}
	
	@JsonIgnore
	public Collection<Etudiant> getEtudiants() {
		return etudiants;
	}
	@JsonSetter
	public void setEtudiants(Collection<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	@Override
	public String toString() {
		return "Groupe [numeroGroupe=" + numeroGroupe + ", libelleGroupe=" + libelleGroupe + ", etape=" + etape
				+ ", classes=" + classes + ", etudiants=" + etudiants + "]";
	}

}
