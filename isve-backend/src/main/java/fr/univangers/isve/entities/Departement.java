package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="departement")
public class Departement implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="code_departement")
	private Long codeDepartement;
	@Column(name="nom_departement")
	private String nomDepartement;
	@ManyToOne
	@JoinColumn(name="code_faculte")
	private Faculte faculte;
	@OneToMany(mappedBy = "departement",fetch = FetchType.LAZY)
	private Collection<Etape> etapes;
	@OneToMany(mappedBy = "departement", fetch = FetchType.LAZY)
	private Collection<Enseignant> enseignants; 
	@OneToMany(mappedBy = "departement", fetch = FetchType.LAZY)
	private Collection<Salle> salles;
	
	public Departement() {
		super();
	}

	public Departement(String nomDepartement, Faculte faculte) {
		super();
		this.nomDepartement = nomDepartement;
		this.faculte = faculte;
	}

	public Long getCodeDepartement() {
		return codeDepartement;
	}
	
	public void setCodeDepartement(Long codeDepartement) {
		this.codeDepartement = codeDepartement;
	}
	
	public String getNomDepartement() {
		return nomDepartement;
	}
	
	public void setNomDepartement(String nomDepartement) {
		this.nomDepartement = nomDepartement;
	}
	
	@JsonIgnore
	public Faculte getFaculte() {
		return faculte;
	}
	
	@JsonSetter
	public void setFaculte(Faculte faculte) {
		this.faculte = faculte;
	}
	
	@JsonIgnore
	public Collection<Etape> getEtapes() {
		return etapes;
	}
	
	@JsonSetter
	public void setEtapes(Collection<Etape> etapes) {
		this.etapes = etapes;
	}
	
	@JsonIgnore
	public Collection<Enseignant> getEnseignants() {
		return enseignants;
	}
	
	@JsonSetter
	public void setEnseignants(Collection<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}
	

	public Collection<Salle> getSalles() {
		return salles;
	}
	
	@JsonSetter
	public void setSalles(Collection<Salle> salles) {
		this.salles = salles;
	}
	
	
	

	@Override
	public String toString() {
		return "Departement [codeDepartement=" + codeDepartement + ", nomDepartement=" + nomDepartement + ", faculte="
				+ faculte + ", etapes=" + etapes + ", enseignants=" + enseignants + ", salles=" + salles + "]";
	}

	
	
}
