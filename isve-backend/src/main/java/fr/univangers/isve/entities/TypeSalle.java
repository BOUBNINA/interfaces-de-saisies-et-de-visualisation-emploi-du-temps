package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="type_salle")
public class TypeSalle implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="code_type_salle")
	private Long codeTypeSalle;
	@Column(name="libelle_type_salle", length=30)
	private String libelleTypeSalle;
	
	
	public TypeSalle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TypeSalle(String libelleTypeSalle) {
		super();
		this.libelleTypeSalle = libelleTypeSalle;
	}
	public Long getCodeTypeSalle() {
		return codeTypeSalle;
	}
	public void setCodeTypeSalle(Long codeTypeSalle) {
		this.codeTypeSalle = codeTypeSalle;
	}
	public String getLibelleTypeSalle() {
		return libelleTypeSalle;
	}
	public void setLibelleTypeSalle(String libelleTypeSalle) {
		this.libelleTypeSalle = libelleTypeSalle;
	}
	

}
