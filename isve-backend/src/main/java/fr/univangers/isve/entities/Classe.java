package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalIdCache;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="classe")
public class Classe implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="code_classe")
	private Long codeClasse;
	@ManyToOne
	@JoinColumn(name="code_matiere")
	private Matiere matiere;
	@ManyToMany
	@JoinTable(name = "classe_groupe",
			joinColumns =
			@JoinColumn(name="code_classe", referencedColumnName = "code_classe"),
			inverseJoinColumns =
			@JoinColumn(name="numero_groupe", referencedColumnName = "numero_groupe")
	)
	private Collection<Groupe> groupes;
	@ManyToOne
	@JoinColumn(name="matricule_enseignant")
	private Enseignant enseignant;
	@ManyToOne
	@JoinColumn(name="code_type_seance")
	private TypeSeance typeSeance;
	@OneToMany(mappedBy = "classe", fetch = FetchType.LAZY)
	private Collection<Seance> seances;
	@OneToMany(mappedBy = "classe", cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<ClasseSalle> salles;
	@ManyToMany
	@JoinTable(name = "classe_contrainte",
			   joinColumns = 
			   		@JoinColumn(name="code_clase", referencedColumnName = "code_classe"),
			   inverseJoinColumns = 
			   		@JoinColumn(name="code_contrainte", referencedColumnName = "code_contrainte")
	)
	private Collection<Contrainte> contraintes;
	@OneToMany(mappedBy = "classe", fetch = FetchType.LAZY)
	private Collection<Programme> programmes;
	
	
	
	
	public Classe() {
		super();
	}

	public Classe(Matiere matiere, Collection<Groupe> groupe) {
		super();
		this.matiere = matiere;
		this.groupes = groupe;
	}
	
	

	public Classe(Long codeClasse) {
		super();
		this.codeClasse = codeClasse;
	}
	
	public Collection<ClasseSalle> getSalles() {
		return salles;
	}
	@JsonIgnore
	public Collection<Contrainte> getContraintes() {
		return contraintes;
	}

	public Collection<Programme> getProgrammes() {
		return programmes;
	}

	public TypeSeance getTypeSeance() {
		return typeSeance;
	}

	public Long getCodeClasse() {
		return codeClasse;
	}
	
	public void setCodeClasse(Long codeClasse) {
		this.codeClasse = codeClasse;
	}

	@JsonIgnore
	public Matiere getMatiere() {
		return matiere;
	}
	
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public Collection<Groupe> getGroupes() {
		return groupes;
	}

	public void setGroupes(Collection<Groupe> groupes) {
		this.groupes = groupes;
	}
	
	@JsonIgnore
	public Collection<Seance> getSeances() {
		return seances;
	}

	public void setSeances(Collection<Seance> seances) {
		this.seances = seances;
	}



	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}
	
	@JsonSetter
	public void setSalles(Collection<ClasseSalle> salles) {
		this.salles = salles;
	}

	@Override
	public String toString() {
		return "Classe [codeClasse=" + codeClasse + ", matiere=" + matiere + "]";
	}
	
	
	
	
	
}
