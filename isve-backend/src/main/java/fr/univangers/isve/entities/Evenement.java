package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "evenement")
public class Evenement implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="code_evenement")
	private Long codeEvenement;
	@Column(name="date_debut")
	private Date dateDebutEvenement;
	@Column(name="date_fin")
	private Date dateFinEvenement;
	@Column(name="type_evenement")
	private String typeEvenement;
	@ManyToOne
	@JoinColumn(name="code_calendrier")
	private Calendrier calendrier;
	
	public Evenement() {
		super();
	}

	public Evenement(Long codeEvenement, Date dateDebutEvenement, Date dateFinEvenement, String typeEvenement,
			Calendrier calendrier) {
		super();
		this.codeEvenement = codeEvenement;
		this.dateDebutEvenement = dateDebutEvenement;
		this.dateFinEvenement = dateFinEvenement;
		this.typeEvenement = typeEvenement;
		this.calendrier = calendrier;
	}

	public Long getCodeEvenement() {
		return codeEvenement;
	}

	public void setCodeEvenement(Long codeEvenement) {
		this.codeEvenement = codeEvenement;
	}

	public Date getDateDebutEvenement() {
		return dateDebutEvenement;
	}

	public void setDateDebutEvenement(Date dateDebutEvenement) {
		this.dateDebutEvenement = dateDebutEvenement;
	}

	public Date getDateFinEvenement() {
		return dateFinEvenement;
	}

	public void setDateFinEvenement(Date dateFinEvenement) {
		this.dateFinEvenement = dateFinEvenement;
	}

	public String getTypeEvenement() {
		return typeEvenement;
	}

	public void setTypeEvenement(String typeEvenement) {
		this.typeEvenement = typeEvenement;
	}

	public Calendrier getCalendrier() {
		return calendrier;
	}

	public void setCalendrier(Calendrier calendrier) {
		this.calendrier = calendrier;
	}
	
	
	
	
	
	
}
