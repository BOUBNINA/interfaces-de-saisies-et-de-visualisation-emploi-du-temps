package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="faculte")
public class Faculte implements Serializable {
	
	@Id
	@Column(name="code_faculte")
	private String codeFaculte;
	@Column(name="nom_faculte", length = 200)
	private String nomFaculte;
	@Column(name="lieu_faculte", length = 200)
	private String lieuFaculte;
	@ManyToOne
	@JoinColumn(name="code_universite")
	private Universite universite;
	@OneToMany(mappedBy = "faculte",fetch = FetchType.LAZY)
	private Collection<Departement> departements;
	
	public Faculte() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Faculte(String codeFaculte, String nomFaculte, String lieuFaculte,Universite universite) {
		super();
		this.codeFaculte = codeFaculte;
		this.nomFaculte = nomFaculte;
		this.lieuFaculte = lieuFaculte;
		this.universite = universite;
	}
	
	public String getNomFaculte() {
		return nomFaculte;
	}
	public void setNomFaculte(String nomFaculte) {
		this.nomFaculte = nomFaculte;
	}
	public String getLieuFaculte() {
		return lieuFaculte;
	}
	public void setLieuFaculte(String lieuFaculte) {
		this.lieuFaculte = lieuFaculte;
	}
	
	@JsonIgnore
	public Collection<Departement> getDepartements() {
		return departements;
	}
	public void setDepartements(Collection<Departement> departements) {
		this.departements = departements;
	}
	
	
	public String getCodeFaculte() {
		return codeFaculte;
	}

	public void setCodeFaculte(String codeFaculte) {
		this.codeFaculte = codeFaculte;
	}

	public Universite getUniversite() {
		return universite;
	}

	public void setUniversite(Universite universite) {
		this.universite = universite;
	}

	@Override
	public String toString() {
		return "Faculte [codeFaculte=" + codeFaculte + ", nomFaculte=" + nomFaculte + ", lieuFaculte=" + lieuFaculte
				+ ", universite=" + universite + ", departements=" + departements + "]";
	}

	
	
}
