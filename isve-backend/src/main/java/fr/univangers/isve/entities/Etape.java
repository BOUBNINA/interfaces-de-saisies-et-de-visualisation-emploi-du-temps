package fr.univangers.isve.entities;


import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name="etape")
public class Etape implements Serializable {
	
	@Id
	@Column(name="code_etape", length = 15)
	private String codeEtape;
	@Column(name="libelle_etape", length = 100)
	private String libelleEtape;
	@ManyToOne
	@JoinColumn(name="code_departement")
	private Departement departement;
	
	@OneToMany(mappedBy = "etape", fetch = FetchType.LAZY)
	private Collection<Groupe> groupes;
	@ManyToMany(mappedBy = "etapes")
	private Collection<UniteEnseignement> uniteEnseignements;
	
	public Etape() {

		super();
		// TODO Auto-generated constructor stub
	}

	public Etape(String codeEtape, String libelleEtape, Departement departement) {
		super();

		this.codeEtape = codeEtape;
		this.libelleEtape = libelleEtape;
		this.departement = departement;
	}

	public String getCodeEtape() {
		return codeEtape;
	}

	public void setCodeEtape(String codeEtape) {
		this.codeEtape = codeEtape;
	}

	public String getLibelleEtape() {
		return libelleEtape;
	}

	public void setLibelleEtape(String libelleEtape) {
		this.libelleEtape = libelleEtape;
	}
	

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}


	public Collection<Groupe> getGroupes() {
		return groupes;
	}
	
	@JsonSetter
	public void setGroupes(Collection<Groupe> groupes) {
		this.groupes = groupes;
	}


	public Collection<UniteEnseignement> getUniteEnseignements() {
		return uniteEnseignements;
	}
	
	@JsonSetter
	public void setUniteEnseignements(Collection<UniteEnseignement> uniteEnseignements) {
		this.uniteEnseignements = uniteEnseignements;
	}

	@Override
	public String toString() {
		return "Etape [codeEtape=" + codeEtape + ", libelleEtape=" + libelleEtape + ", departement=" + departement
				+ ", groupes=" + groupes + ", uniteEnseignements=" + uniteEnseignements + "]";
	}	

}
