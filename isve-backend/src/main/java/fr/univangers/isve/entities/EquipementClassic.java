package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="equipement_classic")
@DiscriminatorValue("EC")
public class EquipementClassic extends Equipement{

	public EquipementClassic(String nomEquipement) {
		super(nomEquipement);
	}
	
}
