package fr.univangers.isve.entities;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="seance")
public class Seance implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="code_seance")
	private Long codeSeance;
	@ManyToOne
	@JoinColumn(name = "code_classe")
	private Classe classe;
	@ManyToOne
	@JoinColumn(name = "code_type_seance")
	private TypeSeance typeSeance;
	@ManyToOne
	@JoinColumn(name = "code_salle")
	private Salle salle;
	@ManyToOne
	@JoinColumn(name = "numero_commentaire")
	private Commentaire commentaire;
	@Column(name="date_seance")
	private String dateSeance;
	@Column(name="duree_seance")
	private String dureeSeance;

	@Column(name = "libelle_matiere")
	private String libelleMatiere;


	@ManyToMany
	@JoinTable(name = "seance_enseignant",
			   joinColumns = 
			   		@JoinColumn(name="code_seance", referencedColumnName = "code_seance"),
			   inverseJoinColumns = 
			   		@JoinColumn(name="matricule_enseignant", referencedColumnName = "matricule_enseignant")
	)	
	private Collection<Enseignant> enseignants;
	
	public Seance() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Seance(Classe classe, TypeSeance typeCours, Salle salle, Commentaire commentaire,
			String dateSeance, String dureeSeance) {
		super();
		this.classe = classe;
		this.typeSeance = typeCours;
		this.salle = salle;
		this.commentaire = commentaire;
		this.dateSeance = dateSeance;
		this.dureeSeance = dureeSeance;
	}
	@JsonIgnore
	public Long getCodeSeance() {
		return codeSeance;
	}
	@JsonSetter
	public void setCodeSeance(Long codeSeance) {
		this.codeSeance = codeSeance;
	}


	public String getLibelleMatiere() {
		return libelleMatiere;
	}

	public void setLibelleMatiere(String libelleMatiere) {
		this.libelleMatiere = libelleMatiere;
	}

	public Classe getClasse() {
		return this.classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	public TypeSeance getTypeCours() {
		return typeSeance;
	}
	public void setTypeCours(TypeSeance typeCours) {
		this.typeSeance = typeCours;
	}
	public Salle getSalle() {
		return salle;
	}
	public void setSalle(Salle salle) {
		this.salle = salle;
	}


	@JsonIgnore
	public Commentaire getCommentaire() {
		return commentaire;
	}


	public void setCommentaire(Commentaire commentaire) {
		this.commentaire = commentaire;
	}
	public String getDateSeance() {
		return dateSeance;
	}
	public void setDateSeance(String dateSeance) {
		this.dateSeance = dateSeance;
	}
	public String getDureeSeance() {
		return dureeSeance;
	}
	public void setDureeSeance(String dureeCours) {
		this.dureeSeance = dureeCours;
	}
	
	
	public Collection<Enseignant> getEnseignants() {
		return enseignants;
	}
	public void setEnseignants(Collection<Enseignant> enseignants) {
		this.enseignants = enseignants;
	}

	@Override
	public String toString() {
		return "Seance [classe=" + classe + ", salle=" + salle + ", dateSeance=" + dateSeance + ", dureeSeance="
				+ dureeSeance + "]";
	}
	
	
}
