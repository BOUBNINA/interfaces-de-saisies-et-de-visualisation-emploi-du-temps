package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="universite")
public class Universite implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="code_universite")
	private Long codeUniversite;
	@Column(name="nom_universite")
	private String nomUniversite;
	@Column(name="lieu_universite")
	private String lieuUniversite;
	@OneToMany(mappedBy = "universite", fetch=FetchType.LAZY)
	private Collection<Faculte> facultes;
	
	@OneToMany(mappedBy = "universite",fetch = FetchType.LAZY)
	private Collection<Calendrier> calendriers; 
	
	public Universite() {
		super();
	}

	public Universite(String nomUniversite, String lieuUniversite) {
		super();
		this.nomUniversite = nomUniversite;
		this.lieuUniversite = lieuUniversite;
	}

	public Long getCodeUniversite() {
		return codeUniversite;
	}
	
	public void setCodeUniversite(Long codeUniversite) {
		this.codeUniversite = codeUniversite;
	}
	
	public String getNomUniversite() {
		return nomUniversite;
	}
	
	public void setNomUniversite(String nomUniversite) {
		this.nomUniversite = nomUniversite;
	}
	
	public String getLieuUniversite() {
		return lieuUniversite;
	}
	
	public void setLieuUniversite(String lieuUniversite) {
		this.lieuUniversite = lieuUniversite;
	}
	
	
}
