package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="statut_enseignant")
public class StatutEnseignant implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="code_statut")
	private Long codeStatut;
	@Column(name="libelle_statut")
	private String libelleStatut;
	@OneToMany(mappedBy = "statutEnseignant",fetch = FetchType.LAZY)
	private Collection<Enseignant> enseignents;
	
	
	public StatutEnseignant() {
		super();
	}
	
	
	public StatutEnseignant(Long codeStatut, String libelleStatut) {
		super();
		this.codeStatut = codeStatut;
		this.libelleStatut = libelleStatut;
	}

	public Long getCodeStatut() {
		return codeStatut;
	}
	
	public void setCodeStatut(Long codeStatut) {
		this.codeStatut = codeStatut;
	}
	
	public String getLibelleStatut() {
		return libelleStatut;
	}
	
	public void setLibelleStatut(String libelleStatut) {
		this.libelleStatut = libelleStatut;
	}
	
	@JsonIgnore
	public Collection<Enseignant> getEnseignements() {
		return enseignents;
	}
	
	public void setEnseignements(Collection<Enseignant> enseignements) {
		this.enseignents = enseignements;
	}
	
	
}
