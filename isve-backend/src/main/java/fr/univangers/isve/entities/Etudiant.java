package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name="etudiant")
@Entity
public class Etudiant implements Serializable {
	@Id
	@Column(name = "code_etudiant")
	private String codeEtudiant;
	@Column(name="prenom_etudiant")
	private String prenomEtudiant;
	@Column(name="nom_etudiant")
	private String nomEtudiant;
	@Column(name="email_etudiant")
	private String emailEtudiant;
	@ManyToOne
	@JoinColumn(name="code_groupe")
	private Groupe groupe;
	public String getCodeEtudiant() {
		return codeEtudiant;
	}
	public void setCodeEtudiant(String codeEtudiant) {
		this.codeEtudiant = codeEtudiant;
	}
	public String getPrenomEtudiant() {
		return prenomEtudiant;
	}
	public void setPrenomEtudiant(String prenomEtudiant) {
		this.prenomEtudiant = prenomEtudiant;
	}
	public String getNomEtudiant() {
		return nomEtudiant;
	}
	public void setNomEtudiant(String nomEtudiant) {
		this.nomEtudiant = nomEtudiant;
	}
	public String getEmailEtudiant() {
		return emailEtudiant;
	}
	public void setEmailEtudiant(String emailEtudiant) {
		this.emailEtudiant = emailEtudiant;
	}
	public Groupe getGroupe() {
		return groupe;
	}
	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}
	
	
	@Override
	public String toString() {
		return "Etudiant [codeEtudiant=" + codeEtudiant + ", prenomEtudiant=" + prenomEtudiant + ", nomEtudiant="
				+ nomEtudiant + ", emailEtudiant=" + emailEtudiant + ", groupe=" + groupe + "]";
	}
	
	
	
	
}
