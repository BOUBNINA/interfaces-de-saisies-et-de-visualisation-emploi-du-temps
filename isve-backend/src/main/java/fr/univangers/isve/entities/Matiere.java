package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="matiere")
public class Matiere implements Serializable {
	
	@Id
	@Column(name="code_matiere", length=15)
	private String codeMatiere;
	@Column(name="libelle_matiere", length=100)
	private String libelleMatiere;
	@Column(name="volume_horaire_matiere")
	private String volumeHoraireMatiere;
	@Column(name="ects_matiere")
	private int ectsMatiere;
	@Column(name="charge_cm_matiere")
	private int chargeCM_Matiere;
	@Column(name="charge_tp_matiere")
	private int chargeTP_Matiere;
	@ManyToOne
	@JoinColumn(name="code_ue")
	private UniteEnseignement uniteEnseignement;
	@OneToMany(mappedBy="matiere")
	private Collection<Classe> classes;
	
	public Matiere() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Matiere(String codeMatiere, String libelleMatiere, String volumeHoraireMatiere, int ectsMatiere,
			int chargeCM_Matiere, int chargeTP_Matiere, UniteEnseignement uniteEnseignement) {
		super();
		this.codeMatiere = codeMatiere;
		this.libelleMatiere = libelleMatiere;
		this.volumeHoraireMatiere = volumeHoraireMatiere;
		this.ectsMatiere = ectsMatiere;
		this.chargeCM_Matiere = chargeCM_Matiere;
		this.chargeTP_Matiere = chargeTP_Matiere;
		this.uniteEnseignement = uniteEnseignement;
	}

	public String getCodeMatiere() {
		return codeMatiere;
	}

	public void setCodeMatiere(String codeMatiere) {
		this.codeMatiere = codeMatiere;
	}
	
	
	public String getLibelleMatiere() {
		return libelleMatiere;
	}
	public void setLibelleMatiere(String libelleMatiere) {
		this.libelleMatiere = libelleMatiere;
	}
	@JsonIgnore
	public String getVolumeHoraireMatiere() {
		return volumeHoraireMatiere;
	}

	public void setVolumeHoraireMatiere(String volumeHoraireMatiere) {
		this.volumeHoraireMatiere = volumeHoraireMatiere;
	}
	
	@JsonIgnore
	public int getEctsMatiere() {
		return ectsMatiere;
	}

	public void setEctsMatiere(int ectsMatiere) {
		this.ectsMatiere = ectsMatiere;
	}
	
	@JsonIgnore
	public int getChargeCM_Matiere() {
		return chargeCM_Matiere;
	}

	public void setChargeCM_Matiere(int chargeCM_Matiere) {
		this.chargeCM_Matiere = chargeCM_Matiere;
	}
	
	@JsonIgnore
	public int getChargeTP_Matiere() {
		return chargeTP_Matiere;
	}

	public void setChargeTP_Matiere(int chargeTP_Matiere) {
		this.chargeTP_Matiere = chargeTP_Matiere;
	}
	
	@JsonIgnore
	public UniteEnseignement getUniteEnseignement() {
		return uniteEnseignement;
	}

	public void setUniteEnseignement(UniteEnseignement uniteEnseignement) {
		this.uniteEnseignement = uniteEnseignement;
	}
	
	public Collection<Classe> getClasses() {
		return classes;
	}

	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}

	@Override
	public String toString() {
		return "Matiere [codeMatiere=" + codeMatiere + ", libelleMatiere=" + libelleMatiere + ", volumeHoraireMatiere="
				+ volumeHoraireMatiere + ", ectsMatiere=" + ectsMatiere + ", chargeCM_Matiere=" + chargeCM_Matiere
				+ ", chargeTP_Matiere=" + chargeTP_Matiere + ", uniteEnseignement=" + uniteEnseignement + ", classes="
				+ classes + "]";
	}
	
	
	

}
