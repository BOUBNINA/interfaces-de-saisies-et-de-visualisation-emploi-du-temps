package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="type_seance")
public class TypeSeance implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="code_type_seance")
	private Long codeTypeSeance;
	@Column(name="libelle_type_cours")
	private String libelleTypeSeance;
	@Column(name="couleur_type_cours")
	private String couleurTypeSeance;
	@OneToMany(mappedBy = "typeSeance")
	private Collection<Classe> classes;
	
	
	public TypeSeance() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TypeSeance(String libelleTypeSeance, String couleurTypeSeance) {
		super();
		this.libelleTypeSeance = libelleTypeSeance;
		this.couleurTypeSeance = couleurTypeSeance;
	}
	

	public Long getCodeTypeSeance() {
		return codeTypeSeance;
	}
	
	@JsonSetter
	public void setCodeTypeSeance(Long codeTypeSeance) {
		this.codeTypeSeance = codeTypeSeance;
	}
	
	public String getLibelleTypeSeance() {
		return libelleTypeSeance;
	}
	public void setLibelleTypeSeance(String libelleTypeSeance) {
		this.libelleTypeSeance = libelleTypeSeance;
	}
	
	@JsonIgnore
	public String getCouleurTypeSeance() {
		return couleurTypeSeance;
	}
	@JsonSetter
	public void setCouleurTypeSeance(String couleurTypeSeance) {
		this.couleurTypeSeance = couleurTypeSeance;
	}
	@JsonIgnore
	public Collection<Classe> getClasses() {
		return classes;
	}

	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}
	
	
	
}
