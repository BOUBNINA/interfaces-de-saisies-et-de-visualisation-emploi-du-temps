package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="contrainte")
public class Contrainte implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="code_contrainte")
	private Long codeContrainte;
	@Column(name="type_contrainte")
	private String typeContrainte;
	@ManyToMany(mappedBy = "contraintes")
	private Collection<Classe> classes;
	public Contrainte() {
		super();
	}

	public Contrainte(Long codeContrainte, String typeContrainte) {
		super();
		this.codeContrainte = codeContrainte;
		this.typeContrainte = typeContrainte;
	}

	public Long getCodeContrainte() {
		return codeContrainte;
	}
	
	public void setCodeContrainte(Long codeContrainte) {
		this.codeContrainte = codeContrainte;
	}
	public String getTypeContrainte() {
		return typeContrainte;
	}
	public void setTypeContrainte(String typeContrainte) {
		this.typeContrainte = typeContrainte;
	}

	public Collection<Classe> getClasses() {
		return classes;
	}

	public void setClasses(Collection<Classe> classes) {
		this.classes = classes;
	}

	@Override
	public String toString() {
		return "Contrainte [codeContrainte=" + codeContrainte + ", typeContrainte=" + typeContrainte + ", classes="
				+ classes + "]";
	}
		
}
