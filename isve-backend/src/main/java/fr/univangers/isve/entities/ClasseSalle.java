package fr.univangers.isve.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "classe_salle")
public class ClasseSalle implements Serializable {

	@EmbeddedId
	private ClasseSalleId code_classe_salle;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "code_classe")
	@MapsId("codeClasse")
	private Classe classe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "code_salle")
	@MapsId("codeSalle")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Salle salle;

	@Column(name = "penalty")
	private int penalty;

	public ClasseSalle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClasseSalle(Classe classe, Salle salle, int penalty) {
		super();
		this.classe = classe;
		this.salle = salle;
		this.code_classe_salle = new ClasseSalleId(classe.getCodeClasse(), salle.getCodeSalle());
		this.penalty = penalty;
	}

	public ClasseSalleId getCode_classe_salle() {
		return code_classe_salle;
	}

	public void setCode_classe_salle(ClasseSalleId code_classe_salle) {
		this.code_classe_salle = code_classe_salle;
	}
	@JsonIgnore
	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}


	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}
	
	
	
}
