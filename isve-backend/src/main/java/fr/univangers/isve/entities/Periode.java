package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="periode")
public class Periode implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="code_periode")
	private Long codePeriode;
	@Column(name="libelle_periode")
	private String libellePeriode;
	@Column(name="annee_periode")
	private String anneePeriode;
	@OneToMany(mappedBy = "periode",fetch = FetchType.LAZY)
	private Collection<UniteEnseignement> uniteEnseignements;
	
	
	
	public Periode() {
		super();
	}

	public Periode(Long codePeriode, String libellePeriode, String anneePeriode) {
		super();
		this.codePeriode = codePeriode;
		this.libellePeriode = libellePeriode;
		this.anneePeriode = anneePeriode;
	}

	public Long getCodePeriode() {
		return codePeriode;
	}

	public void setCodePeriode(Long codePeriode) {
		this.codePeriode = codePeriode;
	}

	public String getLibellePeriode() {
		return libellePeriode;
	}

	public void setLibellePeriode(String libellePeriode) {
		this.libellePeriode = libellePeriode;
	}

	public String getAnneePeriode() {
		return anneePeriode;
	}

	public void setAnneePeriode(String anneePeriode) {
		this.anneePeriode = anneePeriode;
	}
	
	@JsonIgnore
	public Collection<UniteEnseignement> getUniteEnseignements() {
		return uniteEnseignements;
	}

	public void setUniteEnseignements(Collection<UniteEnseignement> uniteEnseignements) {
		this.uniteEnseignements = uniteEnseignements;
	}

	@Override
	public String toString() {
		return "Periode [codePeriode=" + codePeriode + ", libellePeriode=" + libellePeriode + ", anneePeriode="
				+ anneePeriode + ", uniteEnseignements=" + uniteEnseignements + "]";
	}
	
	
	
	
}
