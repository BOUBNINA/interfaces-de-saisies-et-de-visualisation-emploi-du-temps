package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="salle")
public class Salle implements Serializable {
	@Id
	@Column(name="code_salle")
	private String codeSalle;
	@Column(name = "libelle_salle")
	private String libelleSalle;
	@ManyToOne
	@JoinColumn(name = "code_type_salle")
	private TypeSalle typeSalle;
	@Column(name = "capacite_salle")
	private int capaciteSalle;
	@ManyToOne
	@JoinColumn(name="code_departement")
	private Departement departement;
	@OneToMany(mappedBy = "salle", fetch = FetchType.LAZY)
	private Collection<Seance> seances;
	@OneToMany(mappedBy = "salle", fetch = FetchType.LAZY)
	private Collection<Equipement> equipements;
	@OneToMany(mappedBy = "salle", cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<ClasseSalle> classes;

	
	public Salle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Salle(String codeSalle, String libelleSalle, TypeSalle typeSalle, int capaciteSalle, Departement departement) {
		super();
		this.codeSalle = codeSalle;
		this.libelleSalle = libelleSalle;
		this.typeSalle = typeSalle;
		this.capaciteSalle = capaciteSalle;
		this.departement = departement;
	}
	
	public String getCodeSalle() {
		return codeSalle;
	}
	
	public void setCodeSalle(String codeSalle) {
		this.codeSalle = codeSalle;
	}
	
	public String getLibelleSalle() {
		return libelleSalle;
	}

	public void setLibelleSalle(String libelleSalle) {
		this.libelleSalle = libelleSalle;
	}

	@JsonIgnore
	public TypeSalle getTypeSalle() {
		return typeSalle;
	}
	
	@JsonSetter
	public void setTypeSalle(TypeSalle typeSalle) {
		this.typeSalle = typeSalle;
	}
	
	public int getCapaciteSalle() {
		return capaciteSalle;
	}
	@JsonSetter
	public void setCapaciteSalle(int capaciteSalle) {
		this.capaciteSalle = capaciteSalle;
	}

	@JsonIgnore
	public Collection<ClasseSalle> getClasses() {
		return classes;
	}

	public void setClasses(Collection<ClasseSalle> classes) {
		this.classes = classes;
	}

	@JsonIgnore
	public Departement getDepartement() {
		return departement;
	}
	
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	
	@JsonIgnore
	public Collection<Seance> getSeances() {
		return seances;
	}
	
	public void setSeances(Collection<Seance> seances) {
		this.seances = seances;
	}
	
	@JsonIgnore
	public Collection<Equipement> getEquipements() {
		return equipements;
	}
	
	public void setEquipements(Collection<Equipement> equipements) {
		this.equipements = equipements;
	}
	

	@Override
	public String toString() {
		return "Salle [codeSalle=" + codeSalle + ", typeSalle=" + typeSalle + ", departement=" + departement
				+ ", seances=" + seances + ", equipements=" + equipements + "]";
	}
	
}
