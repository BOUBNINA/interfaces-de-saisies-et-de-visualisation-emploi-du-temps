package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="calendrier")
public class Calendrier implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="code_calendrier")
	private Long codeCalendrier;
	@Column(name="date_debut")
	private Date dateDebutCalendrier;
	@Column(name="date_fin")
	private Date dateFinCalendrier;
	@ManyToOne
	@JoinColumn(name="code_universite")
	private Universite universite;
	@OneToMany(mappedBy = "calendrier",fetch=FetchType.LAZY)
	private Collection<Evenement> evenements;
	
	public Calendrier() {
		super();
	}

	public Calendrier(Long codeCalendrier, Date dateDebutCalendrier, Date dateFinCalendrier, Universite universite) {
		super();
		this.codeCalendrier = codeCalendrier;
		this.dateDebutCalendrier = dateDebutCalendrier;
		this.dateFinCalendrier = dateFinCalendrier;
		this.universite = universite;
	}

	public Long getCodeCalendrier() {
		return codeCalendrier;
	}

	public void setCodeCalendrier(Long codeCalendrier) {
		this.codeCalendrier = codeCalendrier;
	}

	public Date getDateDebutCalendrier() {
		return dateDebutCalendrier;
	}

	public void setDateDebutCalendrier(Date dateDebutCalendrier) {
		this.dateDebutCalendrier = dateDebutCalendrier;
	}

	public Date getDateFinCalendrier() {
		return dateFinCalendrier;
	}

	public void setDateFinCalendrier(Date dateFinCalendrier) {
		this.dateFinCalendrier = dateFinCalendrier;
	}

	public Universite getUniversite() {
		return universite;
	}

	public void setUniversite(Universite universite) {
		this.universite = universite;
	}

	public Collection<Evenement> getEvenements() {
		return evenements;
	}

	public void setEvenements(Collection<Evenement> evenements) {
		this.evenements = evenements;
	}

	@Override
	public String toString() {
		return "Calendrier [codeCalendrier=" + codeCalendrier + ", dateDebutCalendrier=" + dateDebutCalendrier
				+ ", dateFinCalendrier=" + dateFinCalendrier + ", universite=" + universite + ", evenements="
				+ evenements + "]";
	}
	
	
	
	
}
