package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="agenda")
public class Agenda implements Serializable{
	
	@Id @GeneratedValue
	@Column(name="code_agenda")
	private Long codeAgenda;
	
	@Column(name="date_debut_agenda")
	private Date dateDebutAgenda;
	
	@Column(name="date_fin_agenda")
	private Date dateFinAgenda;
	
	@Column(name="motif_agenda")
	private String motifAgenda;
	
	@ManyToOne
	@JoinColumn(name="matricule_enseignant")
	private Enseignant enseignant;

	
	public Agenda() {
		super();
	}

	public Agenda(Date dateD,Date dateF, String contenu,Enseignant enseignant) {
		super();
		this.dateDebutAgenda = dateD;
		this.dateFinAgenda = dateF;
		this.motifAgenda = contenu;
		this.enseignant = enseignant;
	}

	public Long getCodeAgenda() {
		return codeAgenda;
	}

	public void setCodeAgenda(Long codeAgenda) {
		this.codeAgenda = codeAgenda;
	}

	public Date getDateDebut() {
		return dateDebutAgenda;
	}

	public void setDateDebut(Date date) {
		this.dateDebutAgenda = date;
	}
	
	public Date getDateFin() {
		return dateFinAgenda;
	}

	public void setDateFin(Date date) {
		this.dateFinAgenda = date;
	}

	public String getContenu() {
		return motifAgenda;
	}

	public void setContenu(String contenu) {
		this.motifAgenda = contenu;
	}
	
}
