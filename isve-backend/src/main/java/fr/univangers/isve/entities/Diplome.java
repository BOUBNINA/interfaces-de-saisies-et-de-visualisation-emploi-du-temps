package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="diplome")
public class Diplome implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="code_diplome")
	private Long codeDiplome;
	@Column(name="libelle_diplome")
	private String libelleDiplome;
	@ManyToOne
	@JoinColumn(name="code_etape")
	private Etape etape;
	
	
	public Diplome() {
		super();
	}

	public Diplome(Long codeDiplome, String libelleDiplome, Etape etape) {
		super();
		this.codeDiplome = codeDiplome;
		this.libelleDiplome = libelleDiplome;
		this.etape = etape;
	}
	
	public Long getCodeDiplome() {
		return codeDiplome;
	}
	public void setCodeDiplome(Long codeDiplome) {
		this.codeDiplome = codeDiplome;
	}
	public String getLibelleDiplome() {
		return libelleDiplome;
	}
	public void setLibelleDiplome(String libelleDiplome) {
		this.libelleDiplome = libelleDiplome;
	}
	public Etape getEtape() {
		return etape;
	}
	public void setEtape(Etape etape) {
		this.etape = etape;
	}

	@Override
	public String toString() {
		return "Diplome [codeDiplome=" + codeDiplome + ", libelleDiplome=" + libelleDiplome + ", etape=" + etape + "]";
	}
	
	
	
}
