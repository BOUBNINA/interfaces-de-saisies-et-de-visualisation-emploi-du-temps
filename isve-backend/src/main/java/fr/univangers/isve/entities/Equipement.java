package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.criteria.Join;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type_equipement",discriminatorType = DiscriminatorType.STRING,length=2)
@Table(name="equipement")
public abstract class Equipement implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="code_equipement")
	private Long codeEquipement;
	
	@Column(name="nom_equipement")
	private String nomEquipement;
	
	@ManyToOne
	@JoinColumn(name="code_salle")
	private Salle salle;
	

	public Equipement() {
		super();
	}
	
	public Equipement(String nomEquipement) {
		super();
		this.nomEquipement = nomEquipement;
	}

	public Long getCodeEquipement() {
		return codeEquipement;
	}

	public void setCodeEquipement(Long codeEquipement) {
		this.codeEquipement = codeEquipement;
	}


	public String getNomEquipement() {
		return nomEquipement;
	}

	public void setNomEquipement(String nomEquipement) {
		this.nomEquipement = nomEquipement;
	}
	
	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

}
