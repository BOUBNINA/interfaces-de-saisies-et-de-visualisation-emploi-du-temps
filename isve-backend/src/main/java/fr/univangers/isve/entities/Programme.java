package fr.univangers.isve.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "programme")
public class Programme implements Serializable {
	@Id
	@GeneratedValue
	@Column(name = "code_programme")
	private Long codeProgramme;
	@Column(name = "debut_programme")
	private  int debutProgramme;
	@Column(name = "duree_programme")
	private String dureeProgramme;
	@Column(name = "semaine_programme")
	private String semaineProgramme;
	@Column(name = "jour_programme")
	private String jourProgramme;
	@Column(name = "penalty_programme")
	private int penalty_programme;
	@ManyToOne
	@JoinColumn(name = "code_classe")
	private Classe classe;
	public Programme() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getCodeProgramme() {
		return codeProgramme;
	}
	public void setCodeProgramme(Long codeProgramme) {
		this.codeProgramme = codeProgramme;
	}

	public void setDebutProgramme(int debutProgramme) {
		this.debutProgramme = debutProgramme;
	}

	public int getDebutProgramme() {
		return debutProgramme;
	}

	public String getDureeProgramme() {
		return dureeProgramme;
	}
	public void setDureeProgramme(String dureeProgramme) {
		this.dureeProgramme = dureeProgramme;
	}
	public String getSemaineProgramme() {
		return semaineProgramme;
	}
	public void setSemaineProgramme(String semaineProgramme) {
		this.semaineProgramme = semaineProgramme;
	}
	public String getJourProgramme() {
		return jourProgramme;
	}
	public void setJourProgramme(String jourProgramme) {
		this.jourProgramme = jourProgramme;
	}
	public int getPenalty_programme() {
		return penalty_programme;
	}
	public void setPenalty_programme(int penalty_programme) {
		this.penalty_programme = penalty_programme;
	}
	@JsonIgnore
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	

}
