package fr.univangers.isve.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ClasseSalleId implements Serializable {
	
	@Column(name = "code_classe")
	private Long codeClasse;
	
	@Column(name = "code_salle")
	private String codeSalle;

	public ClasseSalleId() {
		super();
	}

	public ClasseSalleId(Long codeClasse, String codeSalle) {
		super();
		this.codeClasse = codeClasse;
		this.codeSalle = codeSalle;
	}

	public Long getCodeClasse() {
		return codeClasse;
	}

	public void setCodeClasse(Long codeClasse) {
		this.codeClasse = codeClasse;
	}

	public String getCodeSalle() {
		return codeSalle;
	}

	public void setCodeSalle(String codeSalle) {
		this.codeSalle = codeSalle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeClasse == null) ? 0 : codeClasse.hashCode());
		result = prime * result + ((codeSalle == null) ? 0 : codeSalle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClasseSalleId other = (ClasseSalleId) obj;
		if (codeClasse == null) {
			if (other.codeClasse != null)
				return false;
		} else if (!codeClasse.equals(other.codeClasse))
			return false;
		if (codeSalle == null) {
			if (other.codeSalle != null)
				return false;
		} else if (!codeSalle.equals(other.codeSalle))
			return false;
		return true;
	}
	
	
}
