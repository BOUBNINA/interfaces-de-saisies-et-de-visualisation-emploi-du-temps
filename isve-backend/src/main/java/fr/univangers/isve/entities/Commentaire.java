package fr.univangers.isve.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name="commentaire")
public class Commentaire implements Serializable {
	@Id
	@GeneratedValue
	@Column(name="numero_commentaire")
	private Long numeroCommentaire;
	@Column(name="libelle_commentaire")
	private String libelleCommentaire;
	@OneToMany(mappedBy = "commentaire", fetch = FetchType.LAZY)
	private Collection<Seance> Seances;
	
	public Commentaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commentaire(Long numeroCommentaire, String libelleCommentaire) {
		super();
		this.numeroCommentaire = numeroCommentaire;
		this.libelleCommentaire = libelleCommentaire;
	}
	
	public Long getNumeroCommentaire() {
		return numeroCommentaire;
	}

	public void setNumeroCommentaire(Long numeroCommentaire) {
		this.numeroCommentaire = numeroCommentaire;
	}

	public String getLibelleCommentaire() {
		return libelleCommentaire;
	}

	public void setLibelleCommentaire(String libelleCommentaire) {
		this.libelleCommentaire = libelleCommentaire;
	}
	
	@JsonIgnore
	public Collection<Seance> getSeances() {
		return Seances;
	}
	
	@JsonSetter
	public void setSeances(Collection<Seance> seances) {
		Seances = seances;
	}

	@Override
	public String toString() {
		return "Commentaire [numeroCommentaire=" + numeroCommentaire + ", libelleCommentaire=" + libelleCommentaire
				+ ", Seances=" + Seances + "]";
	}
	
	
	

}
