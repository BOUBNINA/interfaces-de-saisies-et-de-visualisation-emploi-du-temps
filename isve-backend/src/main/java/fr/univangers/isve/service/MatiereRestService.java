package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Matiere;
import fr.univangers.isve.metier.IMatiereMetier;

@RestController
public class MatiereRestService {
	
	@Autowired
	private IMatiereMetier matiereMetier;
	@RequestMapping(value = "/matiere", method = RequestMethod.POST)
	public void saveMatiere(@RequestBody Matiere matiere) {
		matiereMetier.insererMatiere(matiere);
	}
	@RequestMapping(value = "/matieres", method = RequestMethod.GET)
	public List<Matiere> listMatieres() {
		return matiereMetier.listMatieres();
	}
}
