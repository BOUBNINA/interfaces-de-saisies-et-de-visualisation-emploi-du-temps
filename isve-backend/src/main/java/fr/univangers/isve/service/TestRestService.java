package fr.univangers.isve.service;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.metier.IEtapeMetier;
import fr.univangers.isve.params.Params;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.StatutEnseignant;
import fr.univangers.isve.metier.IStatutEnseignantMetier;

@RestController
public class TestRestService {

    @Autowired
    IEtapeMetier etapeMetier;

    @RequestMapping(value="/test", method=RequestMethod.GET)
    public void test() {
        List<Etape> e = etapeMetier.listFormations();
        Etape et = null;
        for(Etape ee : e ){

            if(Integer.parseInt(ee.getCodeEtape()) == 3){
                et = ee;


            }
        }
        System.out.println(et.getDepartement().getCodeDepartement());


        URLConnection conn =null;
        try {
            conn = new URL(Params.getInstance().getValue("urlsolveur")+"dzn").openConnection();
            conn.setDoInput(true);
            //fconn.setRequestProperty("Content-Type", "application/octet-stream;");
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            ObjectOutputStream objOut = new ObjectOutputStream(conn.getOutputStream());
            objOut.writeObject(et);

            objOut.flush();
            objOut.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            ObjectInputStream objIn = new ObjectInputStream(conn.getInputStream());
            Object reply = objIn.readObject();
            objIn.close();
        } catch (Exception ex) {
            // it is ok if we get an exception here
            // that means that there is no object being returned
            System.out.println("No Object Returned");
            if (!(ex instanceof EOFException))
                ex.printStackTrace();
            System.err.println("*");
        }

    }


}
