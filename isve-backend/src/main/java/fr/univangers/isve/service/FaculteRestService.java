package fr.univangers.isve.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.metier.IFaculteMetier;

@RestController
public class FaculteRestService {


	@Autowired
	private IFaculteMetier faculteMetier;
	
	@RequestMapping(value="/faculte", method=RequestMethod.POST)
	public void saveFaculte(Faculte faculte) {
		faculteMetier.insererFaculte(faculte);
	}
	
	@RequestMapping(value ="/facultes", method = RequestMethod.GET)
	public List<Faculte> listFacultes(){
		return faculteMetier.listFacultes();
	}


}
