package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Universite;
import fr.univangers.isve.metier.IUniversiteMetier;

@RestController
public class UniversiteRestService {
	
	@Autowired
	IUniversiteMetier universiteMetier;
	
	@RequestMapping(value="/universite", method=RequestMethod.POST)
	public void saveUniversite(Universite universite) {
		universiteMetier.insererUniversite(universite);
	}
	
	@RequestMapping(value="/universites", method=RequestMethod.GET)
	public List<Universite> listUniversites() {
		return universiteMetier.listUniversite();
	}

}
