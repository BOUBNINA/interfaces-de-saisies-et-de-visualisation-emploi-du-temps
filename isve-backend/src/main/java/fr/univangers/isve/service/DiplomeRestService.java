package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Diplome;
import fr.univangers.isve.metier.IDiplomeMetier;

@RestController
public class DiplomeRestService {
	
	@Autowired
	IDiplomeMetier diplomeMetier;
	
	@RequestMapping(value="/diplome", method= RequestMethod.POST)
	public void saveDiplome(Diplome diplome) {
		diplomeMetier.insererDiplome(diplome);
	}
	
	@RequestMapping(value="/diplomes", method= RequestMethod.GET)
	public List<Diplome> listDiplomes(){
		return diplomeMetier.listDiplome();
	}
}
