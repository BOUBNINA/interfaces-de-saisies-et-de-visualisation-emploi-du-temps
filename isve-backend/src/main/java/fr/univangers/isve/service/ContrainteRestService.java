package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Contrainte;
import fr.univangers.isve.metier.IContrainteMetier;

@RestController
public class ContrainteRestService {

	@Autowired
	IContrainteMetier contrainteMetier;
	
	@RequestMapping(value="/contrainte", method= RequestMethod.POST)
	public void saveContrainte(Contrainte contrainte) {
		contrainteMetier.insererContrainte(contrainte);
	}
	
	@RequestMapping(value="/contraintes", method= RequestMethod.GET)
	public List<Contrainte> listContraintes(){
		return contrainteMetier.listContrainte();
	}
}
