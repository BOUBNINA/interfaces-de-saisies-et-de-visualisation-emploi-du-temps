package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.metier.IDepartementMetier;

@RestController
public class DepartementRestService {
	
	@Autowired
	IDepartementMetier departementMetier;
	
	@RequestMapping(value="/departement", method= RequestMethod.POST)
	public void saveDepartement(Departement departement) {
		departementMetier.insererDepartement(departement);
	}
	
	@RequestMapping(value="/departements", method= RequestMethod.GET)
	public List<Departement> listDepartements(){
		return departementMetier.listDepartement();
	}
}
