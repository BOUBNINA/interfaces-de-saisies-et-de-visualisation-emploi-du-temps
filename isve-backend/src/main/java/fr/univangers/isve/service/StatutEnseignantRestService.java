package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.StatutEnseignant;
import fr.univangers.isve.metier.IStatutEnseignantMetier;

@RestController
public class StatutEnseignantRestService {
	
	@Autowired
	IStatutEnseignantMetier statutEnseignantMetier;
	
	@RequestMapping(value="/statutEnseignement", method=RequestMethod.POST)
	public void saveStatutEnseignement(StatutEnseignant statut) {
		statutEnseignantMetier.insererStatutEnseignant(statut);
	}
	
	@RequestMapping(value="/statutEnseignements", method=RequestMethod.GET)
	public List<StatutEnseignant> listStatutEnseignants(){
		return statutEnseignantMetier.listStatutEnseignant();
	}

}
