package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Seance;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.metier.IEtapeMetier;

@RestController
public class EtapeRestService {
	@Autowired
	IEtapeMetier formationMetier;
	
	@RequestMapping(value="/etape", method=RequestMethod.POST)
	public void saveFormation(Etape formation) {
		formationMetier.insererEtape(formation);
	}
	
	@RequestMapping(value="/etapes", method = RequestMethod.GET)
	public List<Etape> listFormation(){
		return formationMetier.listFormations();
	}
	
	@RequestMapping(value = "/etapesFaculte", method = RequestMethod.GET)
	public List<Etape>  listSeancesFaculte(@RequestParam  Faculte faculte){
		return formationMetier.listEtapeByFaculte(faculte);
	}

	@RequestMapping(value = "/etape", method = RequestMethod.GET)
	public Etape getEtapeByLibelleEtape(@RequestParam String libelleEtape){
		return formationMetier.getByLibelleEtape(libelleEtape);
	}

	
	@RequestMapping(value = "/etapeCode", method = RequestMethod.GET)
	public Etape getEtapeById(@RequestParam String codeEtape) {
		return formationMetier.getEtapeById(codeEtape);
	}
	
	@RequestMapping(value = "/faculteByEtape", method = RequestMethod.GET)
	public Faculte getFaculteByEtape(@RequestParam Etape etape) {
		return formationMetier.getFaculteByEtape(etape);
	}


	
	
}
