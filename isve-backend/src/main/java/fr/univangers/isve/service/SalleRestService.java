package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.metier.ISalleMetier;

@RestController
public class SalleRestService {
	
	@Autowired
	private ISalleMetier salleMetier;
	
	@RequestMapping(value="/salle", method = RequestMethod.POST)
	public void saveSalle(Salle salle) {
		salleMetier.insererSalle(salle);
	}
	
	@RequestMapping(value="/salles", method = RequestMethod.GET)
	public List<Salle> listSalles(){
		
		return salleMetier.listSalles();
	}
	
	@RequestMapping(value="/sallesByDepartement", method = RequestMethod.GET)
	public List<Salle> listSallesByDepartement(@RequestParam Departement departement){
		return salleMetier.listSalleByDepartement(departement);
	}

}

