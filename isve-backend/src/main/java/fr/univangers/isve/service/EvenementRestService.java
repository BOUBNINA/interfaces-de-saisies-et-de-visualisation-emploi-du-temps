package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Evenement;
import fr.univangers.isve.metier.IEvenementMetier;

@RestController
public class EvenementRestService {
	
	@Autowired
	IEvenementMetier evenementMetier;
	
	@RequestMapping(value="/evenement", method=RequestMethod.POST)
	public void saveEvenement(Evenement evenement) {
		evenementMetier.insererEvenement(evenement);
	}
	
	@RequestMapping(value="/evenements", method=RequestMethod.GET)
	public List<Evenement> listEvenements() {
		return evenementMetier.listEvenement();
	}
}
