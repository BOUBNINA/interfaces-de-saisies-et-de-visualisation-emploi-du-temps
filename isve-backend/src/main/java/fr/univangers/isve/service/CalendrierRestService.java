package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.metier.ICalendrierMetier;

@RestController
public class CalendrierRestService {
	
	@Autowired
	ICalendrierMetier calendrierMetier;
	
	@RequestMapping(value="/calendrier", method= RequestMethod.POST)
	public void saveCalendrier(Calendrier calendrier) {
		calendrierMetier.insererCalendrier(calendrier);
	}
	
	@RequestMapping(value="/calendriers", method= RequestMethod.GET)
	public List<Calendrier> listCalendrier(){
		return calendrierMetier.listCalendrier();
	}
}
