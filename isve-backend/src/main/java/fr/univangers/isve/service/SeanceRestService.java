package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Seance;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.metier.ISeanceMetier;

@RestController
public class SeanceRestService {
	
	@Autowired
	private ISeanceMetier SeanceMetier;
	
	@RequestMapping(value = "/seance", method = RequestMethod.POST)
	public void saveSean(@RequestBody Seance cours) {
		SeanceMetier.insererSeance(cours);
	}
	
	@RequestMapping(value = "/seances", method = RequestMethod.GET)
	public List<Seance>  listSeance() {
		return SeanceMetier.listSeance();
	}
	
	@RequestMapping(value = "/seancesEtape", method = RequestMethod.GET)
	public List<Seance>  listSeancesEtape(@RequestParam Etape etape){
		return SeanceMetier.listSeanceByEtape(etape);
	}

	@RequestMapping(value = "/seancesEtape", method = RequestMethod.DELETE)
	public void  deleteSeancesEtape(@RequestParam Etape etape){
		SeanceMetier.deleteSeancesByEtape(etape);
	}


	@RequestMapping(value = "/seancesEtudiant", method = RequestMethod.GET)
	public List<Seance>  listSeancesEtudiant(@RequestParam Etudiant etudiant){
		return SeanceMetier.listSeanceByEtudiant(etudiant);
	}
	
	@RequestMapping(value = "/seanceDTO", method = RequestMethod.POST)
	public void recupSeance(@RequestBody List<fr.univangers.isve.dto.Seance> seances) {
		SeanceMetier.recupSeanceDTO(seances);
	}
	
	
}
