package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Equipement;
import fr.univangers.isve.metier.IEquipementMetier;

@RestController
public class EquipementRestService {
	
	@Autowired
	IEquipementMetier equipementMetier;
	
	@RequestMapping(value="/equipement", method= RequestMethod.POST)
	public void saveEquipement(Equipement equipement) {
		equipementMetier.insererEquipement(equipement);
	}
	
	@RequestMapping(value="/equipements", method= RequestMethod.GET)
	public List<Equipement> listEquipement(){
		return equipementMetier.listEquipement();
	}
}
