package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.UniteEnseignement;
import fr.univangers.isve.metier.IUniteEnseignementMetier;

@RestController
public class UniteEnseignementRestService {
	
	@Autowired
	private IUniteEnseignementMetier uniteEnseignementMetier;
	
	@RequestMapping(value="/uniteEnseignement", method=RequestMethod.POST)
	public void saveUniteEnseignement(UniteEnseignement uniteEnseignement) {
		uniteEnseignementMetier.insererUniteEnseignement(uniteEnseignement);
	}
	
	@RequestMapping(value="/uniteEnseignements", method=RequestMethod.GET)
	public List<UniteEnseignement> listUniteEnseignement(){
		return uniteEnseignementMetier.listUniteEnseignement();
	}

}
