package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.metier.IGroupeMetier;

@RestController
public class GroupeRestService {
	
	@Autowired
	IGroupeMetier groupeMetier;
	
	@RequestMapping(value="/groupe", method=RequestMethod.POST)	
	public void saveGroupe(Groupe groupe) {
		groupeMetier.insererGroupe(groupe);
	}
	
	@RequestMapping(value="/groupes", method=RequestMethod.GET)
	public List<Groupe> listGroupe(){
		return groupeMetier.listGroupes();
	}
	
	/*
	@RequestMapping(value="/groupesFormation",method=RequestMethod.GET)
	public List<Groupe> listGroupesFormation(@RequestParam Diplome formation){
		return groupeMetier.listGroupesByFormation(formation);
	}
	*/
}
