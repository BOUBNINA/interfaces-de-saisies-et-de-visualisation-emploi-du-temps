package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.metier.IClasseMetier;

@RestController
public class ClasseRestService {
	
	@Autowired
	IClasseMetier classeMetier;
	
	@RequestMapping(value="/classe", method= RequestMethod.POST)
	public void saveClasse(Classe classe) {
		classeMetier.insererClasse(classe);
	}
	
	
	@RequestMapping(value="/classes", method= RequestMethod.GET)
	public List<Classe> listClasses(){
		return classeMetier.listClasse();
	}
	
	@RequestMapping(value="/classesByEtape", method= RequestMethod.GET)
	public List<Classe> listClassesByEtape(@RequestParam Etape etape){
		return classeMetier.listClasseByEtape(etape);
	}
}
