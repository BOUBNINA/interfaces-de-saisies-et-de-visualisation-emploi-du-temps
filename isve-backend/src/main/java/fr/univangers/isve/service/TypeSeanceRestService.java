package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.TypeSeance;
import fr.univangers.isve.metier.ITypeCoursMetier;

@RestController
public class TypeSeanceRestService {
	
	@Autowired
	private ITypeCoursMetier typeCoursMetier;
	
	@RequestMapping(value = "/typeCours", method = RequestMethod.POST)
	public void saveTypeCours(@RequestBody TypeSeance typeCours) {
		typeCoursMetier.insererTypeCours(typeCours);
	}
	
	@RequestMapping(value = "/typeCours", method = RequestMethod.GET)
	public List<TypeSeance> listTypeCours() {
		return typeCoursMetier.listTypeCours();
	}
	
}
