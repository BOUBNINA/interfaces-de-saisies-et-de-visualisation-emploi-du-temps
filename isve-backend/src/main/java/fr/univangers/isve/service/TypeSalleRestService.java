package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.TypeSalle;
import fr.univangers.isve.metier.ITypeSalleMetier;


@RestController
public class TypeSalleRestService {
	
	@Autowired
	private ITypeSalleMetier typeSalleMetier;
	
	
	
	@RequestMapping(value="/typeSalle", method = RequestMethod.POST)
	public void saveTypeSalle(TypeSalle typeSalle) {
		typeSalleMetier.insererTypeSalle(typeSalle);
	}
	
	
	@RequestMapping(value="/typeSalles", method = RequestMethod.GET)
	public List<TypeSalle> listTypeSalles(){
		return typeSalleMetier.listTypeSalles();
	}

}
