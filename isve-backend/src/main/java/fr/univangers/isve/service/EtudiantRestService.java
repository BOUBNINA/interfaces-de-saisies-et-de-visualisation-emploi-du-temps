package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.metier.IEtudiantMetier;

@RestController
public class EtudiantRestService {
	@Autowired
	IEtudiantMetier etudiantMetier;
	
	@RequestMapping(value="/etudiant" , method = RequestMethod.POST)
	public void saveEtudiant(Etudiant etudiant) {
		etudiantMetier.insertEtudiant(etudiant);
	}
	
	@RequestMapping(value="/etudiants")
	public List<Etudiant> listEtudiants(){
		return etudiantMetier.listEtudiant();
	}
	
	
}
