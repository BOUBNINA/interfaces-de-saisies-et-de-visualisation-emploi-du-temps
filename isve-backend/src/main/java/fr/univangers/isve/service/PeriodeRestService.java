package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Periode;
import fr.univangers.isve.metier.IPeriodeMetier;

@RestController
public class PeriodeRestService {
	@Autowired
	private IPeriodeMetier periodeMetier;
	
	@RequestMapping(value="/periode", method=RequestMethod.POST)
	public void savePeriode(Periode periode) {
		periodeMetier.insererPeriode(periode);
	}
	
	@RequestMapping(value="/periodes", method=RequestMethod.GET)
	public List<Periode> listPeriodes(){
		return periodeMetier.listPeriode();
	}

}
