package fr.univangers.isve.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.univangers.isve.entities.Agenda;
import fr.univangers.isve.metier.IAgendaMetier;

@RestController
public class AgendaRestService {
	
	@Autowired
	IAgendaMetier agendaMetier;
	
	@RequestMapping(value="/agenda", method=RequestMethod.POST)
	public void saveAgenda(Agenda agenda) {
		agendaMetier.insererAgenda(agenda);
	}
	
	@RequestMapping(value="/agendas", method=RequestMethod.GET)
	public List<Agenda> listAgendas() {
		return agendaMetier.listAgenda();
	}
	
}
