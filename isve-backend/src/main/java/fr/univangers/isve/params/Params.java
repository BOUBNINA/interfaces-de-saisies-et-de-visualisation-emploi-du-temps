package fr.univangers.isve.params;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Params {
    private static Params instance = null;
    private Properties properties;

    protected Params() throws IOException {
        properties = new Properties();
        properties.load( new FileInputStream("src/main/resources/config.properties"));
    }

    public static Params getInstance(){
        if(instance == null) {
            try {
                instance = new Params();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  instance;
    }

    public String getValue(String key){
        return properties.getProperty(key);
    }
}
