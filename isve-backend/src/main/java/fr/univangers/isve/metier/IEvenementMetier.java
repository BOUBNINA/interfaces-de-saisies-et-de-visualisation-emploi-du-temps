package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import fr.univangers.isve.dao.EvenementRepository;
import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.entities.Evenement;

public interface IEvenementMetier {

	public void insererEvenement(Evenement evenement);
	public List<Evenement> listEvenement();
	public List<Evenement> listEvenementByCalendrier(Calendrier calendrier);
	
}
