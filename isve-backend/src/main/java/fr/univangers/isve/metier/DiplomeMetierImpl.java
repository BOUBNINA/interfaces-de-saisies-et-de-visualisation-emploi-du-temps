package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.DiplomeRepository;
import fr.univangers.isve.entities.Diplome;
import fr.univangers.isve.entities.Etape;

@Service
public class DiplomeMetierImpl implements IDiplomeMetier{
	@Autowired
	private DiplomeRepository diplomeRepository;
	
	@Override
	public void insererDiplome(Diplome diplome) {
		diplomeRepository.save(diplome);
	}

	@Override
	public List<Diplome> listDiplome() {
		return diplomeRepository.findAll();
	}

	@Override
	public List<Diplome> listDiplomeByEtape(Etape etape) {
		return diplomeRepository.findAllByEtape(etape);
	}

}
