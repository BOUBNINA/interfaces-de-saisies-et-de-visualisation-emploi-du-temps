package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Agenda;
import fr.univangers.isve.entities.Enseignant;

public interface IAgendaMetier {
	
	public void insererAgenda(Agenda agenda);
	public List<Agenda> listAgenda();
	public List<Agenda> listAgendaByEnseignant(Enseignant enseignant);
	
}
