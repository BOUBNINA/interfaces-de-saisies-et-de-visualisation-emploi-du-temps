package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.TypeSeanceRepository;
import fr.univangers.isve.entities.TypeSeance;

@Service
public class TypeCoursMetierImpl implements ITypeCoursMetier {
	
	@Autowired
	private TypeSeanceRepository typeCoursRepository;
	@Override
	public void insererTypeCours(TypeSeance typeCours) {
		typeCoursRepository.save(typeCours);
		
	}

	@Override
	public List<TypeSeance> listTypeCours() {
		return typeCoursRepository.findAll();
	}

}
