package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.GroupeRepository;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;


@Service
public class GroupeMetierImpl implements IGroupeMetier{
	
	@Autowired
	private GroupeRepository groupeRepository;
	
	@Override
	public void insererGroupe(Groupe groupe) {
		
		groupeRepository.save(groupe);
	}

	@Override
	public List<Groupe> listGroupes(){
		return groupeRepository.findAll();
	}

	@Override
	public List<Groupe> listGroupesByEtape(Etape etape) {
		return groupeRepository.findAllByEtape(etape); 
	}
	
	
	
}
