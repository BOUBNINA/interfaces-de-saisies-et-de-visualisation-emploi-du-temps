package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.entities.Universite;

public interface ICalendrierMetier {
	
	public void insererCalendrier(Calendrier calendrier);
	public List<Calendrier> listCalendrier();
	public List<Calendrier> listCalendrierByUniversite(Universite universite);
}
