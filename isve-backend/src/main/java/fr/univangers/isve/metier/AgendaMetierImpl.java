package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.AgendaRepository;
import fr.univangers.isve.entities.Agenda;
import fr.univangers.isve.entities.Enseignant;

@Service
public class AgendaMetierImpl implements IAgendaMetier{
	@Autowired
	private AgendaRepository agendaRepository;
	
	@Override
	public void insererAgenda(Agenda agenda) {
		agendaRepository.save(agenda);
	}

	@Override
	public List<Agenda> listAgenda() {
		
		return agendaRepository.findAll();
	}

	@Override
	public List<Agenda> listAgendaByEnseignant(Enseignant enseignant) {
		return agendaRepository.findAllByEnseignant(enseignant);
		
	}

}
