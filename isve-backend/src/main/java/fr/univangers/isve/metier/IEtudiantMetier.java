package fr.univangers.isve.metier;

import java.util.Collection;
import java.util.List;

import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;

public interface IEtudiantMetier {
	
	public void insertEtudiant(Etudiant etudiant);
	public List<Etudiant> listEtudiant();
	public Collection<Etudiant> listEtudiantByGroupe(Groupe groupe);
}
