package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.UniteEnseignement;

public interface IUniteEnseignementMetier {
	
	public void insererUniteEnseignement(UniteEnseignement ue);
	public List<UniteEnseignement> listUniteEnseignement();	
}
