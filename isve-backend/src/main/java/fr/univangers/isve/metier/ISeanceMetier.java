package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Seance;
import fr.univangers.isve.entities.TypeSeance;
import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Enseignant;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.entities.Salle;

public interface ISeanceMetier {
	public void insererSeance(Seance seance);
	public List<Seance> listSeance();
	public List<Seance> listSeanceByTypeSeance(TypeSeance typeSeance);
	public List<Seance> listSeanceByClasse(Classe classe);
	public List<Seance> listSeanceBySalle(Salle salle);
	public List<Seance> listSeanceByEtape(Etape etape);
	public void deleteSeancesByEtape(Etape etape);
	public List<Seance> listSeanceByEtudiant(Etudiant etudiant);
	public void recupSeanceDTO(List<fr.univangers.isve.dto.Seance> seances);
}
