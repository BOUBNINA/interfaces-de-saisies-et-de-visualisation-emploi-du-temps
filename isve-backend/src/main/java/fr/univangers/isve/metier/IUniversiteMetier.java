package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Universite;

public interface IUniversiteMetier {
	
	public void insererUniversite(Universite universite);
	public List<Universite> listUniversite();
	
}
