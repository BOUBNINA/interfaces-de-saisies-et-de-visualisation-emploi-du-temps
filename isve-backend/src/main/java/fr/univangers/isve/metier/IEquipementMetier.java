package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Equipement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSalle;

public interface IEquipementMetier {
	public void insererEquipement(Equipement equipement);
	public List<Equipement> listEquipement();
	public List<Equipement> listEquipementBySalle(Salle salle);
}
