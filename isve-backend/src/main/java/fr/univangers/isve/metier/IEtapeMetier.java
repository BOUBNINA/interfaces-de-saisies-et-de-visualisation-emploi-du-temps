package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Etape;

public interface IEtapeMetier{
	
	public void insererEtape(Etape etape);
	public List<Etape> listFormations();
	public List<Etape> listEtapeByDepartement(Departement departement);
	public Etape getEtapeById(String codeEtape);
	public List<Etape> listEtapeByFaculte(Faculte faculte);

	public Etape getByLibelleEtape(String libelleEtape);
	public Faculte getFaculteByEtape(Etape etape);
}
