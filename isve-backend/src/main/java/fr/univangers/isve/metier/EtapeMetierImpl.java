package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import fr.univangers.isve.dao.EtapeRepository;
import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Etape;

@Service
public class EtapeMetierImpl implements IEtapeMetier {
	
	@Autowired
	private EtapeRepository etapeRepository;
	
	@Override
	public void insererEtape(Etape etape) {
		etapeRepository.save(etape);
	}

	@Override
	public List<Etape> listFormations() {
		
		return etapeRepository.findAll();
	}

	@Override
	public List<Etape> listEtapeByDepartement(Departement departement) {
		return etapeRepository.findAllByDepartement(departement);
	}

	@Override
	public List<Etape> listEtapeByFaculte(Faculte faculte) {
		return etapeRepository.findAllByFaculte(faculte);
	}

	@Override
	public Etape getByLibelleEtape(String libelleEtape) {
		return etapeRepository.findByLibelleEtape(libelleEtape);
	}




	@Override
	public Etape getEtapeById(String codeEtape) {
		return etapeRepository.findById(codeEtape).get();
	}

	@Override
	public Faculte getFaculteByEtape(Etape etape) {
		return etapeRepository.findFaculteByEtape(etape);
	}




}
