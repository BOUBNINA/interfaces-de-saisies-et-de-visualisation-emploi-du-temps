package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSalle;

public interface ISalleMetier {
	
	public void insererSalle(Salle salle);
	public List<Salle> listSalles();
	public List<Salle> listSalleByDepartement(Departement departement);
	public List<Salle> listSalleByType(TypeSalle typeSalle);
}
