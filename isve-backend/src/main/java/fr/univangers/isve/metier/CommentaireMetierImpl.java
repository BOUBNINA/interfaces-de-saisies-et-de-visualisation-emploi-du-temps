package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.CommentaireRepository;
import fr.univangers.isve.entities.Commentaire;
import fr.univangers.isve.entities.Seance;

@Service
public class CommentaireMetierImpl implements ICommentaireMetier {
	
	@Autowired
	private CommentaireRepository commentaireRepository;
	
	@Override
	public void insererCommentaire(Commentaire commentaire) {
		commentaireRepository.save(commentaire);
		
	}

	@Override
	public List<Commentaire> listCommentaires() {
		return commentaireRepository.findAll();
	}

	
}
