package fr.univangers.isve.metier;

import java.util.List;
import java.util.Optional;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Enseignant;

public interface IEnseignantMetier {
	public Optional<Enseignant> getEnseignantById(String matriculeEnseignant);
	public void insererEnseignant(Enseignant enseignant);
	public List<Enseignant> listEnseignants();
	public List<Enseignant> listEnseignantsByDepartement(Departement departement);
}
