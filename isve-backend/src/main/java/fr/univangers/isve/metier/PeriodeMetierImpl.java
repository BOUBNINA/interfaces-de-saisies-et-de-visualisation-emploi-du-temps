package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.PeriodeRepository;
import fr.univangers.isve.entities.Periode;

@Service
public class PeriodeMetierImpl implements IPeriodeMetier{
	
	@Autowired
	private PeriodeRepository periodeRepository ;
	
	@Override
	public void insererPeriode(Periode periode) {
		periodeRepository.save(periode);
	}

	@Override
	public List<Periode> listPeriode() {
		return periodeRepository.findAll();
	}
	

}
