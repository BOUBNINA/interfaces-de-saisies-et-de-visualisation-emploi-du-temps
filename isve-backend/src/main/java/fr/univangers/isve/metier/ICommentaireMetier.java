package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Commentaire;
import fr.univangers.isve.entities.Seance;

public interface ICommentaireMetier {
	public void insererCommentaire(Commentaire commentaire);
	public List<Commentaire> listCommentaires();
}
