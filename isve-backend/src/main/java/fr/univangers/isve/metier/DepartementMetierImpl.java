package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.DepartementRepository;
import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Universite;


@Service
public class DepartementMetierImpl implements IDepartementMetier{
	@Autowired
	private DepartementRepository departementRepository;
	
	@Override
	public void insererDepartement(Departement departement) {
		departementRepository.save(departement);
	}

	@Override
	public List<Departement> listDepartement() {
		return departementRepository.findAll();
	}

	@Override
	public List<Departement> listDepartementByFaculte(Faculte faculte) {
		return departementRepository.findAllByFaculte(faculte);
	}

}
