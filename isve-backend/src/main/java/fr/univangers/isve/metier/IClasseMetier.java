package fr.univangers.isve.metier;

import java.util.List;
import java.util.Optional;

import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.entities.Matiere;

public interface IClasseMetier {
	
	public void insererClasse(Classe classe);
	public Optional<Classe> getClasseById(Long codeClasse);
	public List<Classe> listClasse();
	public List<Classe> listClasseByMatiere(Matiere matiere);
	public List<Classe> listClasseByGroupe(Groupe groupe);
	public List<Classe> listClasseByEtape(Etape etape);
	
}
