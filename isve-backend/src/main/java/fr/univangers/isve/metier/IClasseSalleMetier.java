package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.dto.Correspondance;
import fr.univangers.isve.entities.ClasseSalle;
import fr.univangers.isve.entities.Etape;

public interface IClasseSalleMetier {
	public void insererClasseSalle(ClasseSalle classeSalle);
	public List<ClasseSalle> listClasseSalleByEtape(Etape etape);
	public void recupCorrespondance(List<Correspondance> correspondances);
	public List<ClasseSalle> listClasseSalles();
}
