package fr.univangers.isve.metier;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.EtudiantRepository;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;

@Service
public class EtudiantMetierImpl implements IEtudiantMetier {
	
	@Autowired
	EtudiantRepository etudiantRepository;
	
	@Override
	public void insertEtudiant(Etudiant etudiant) {
		etudiantRepository.save(etudiant);
	}

	@Override
	public List<Etudiant> listEtudiant() {
		return etudiantRepository.findAll();
	}

	@Override
	public Collection<Etudiant> listEtudiantByGroupe(Groupe groupe) {
		return etudiantRepository.findAllByGroupe(groupe);
	}

}
