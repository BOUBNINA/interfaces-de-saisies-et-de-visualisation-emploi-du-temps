package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Contrainte;

public interface IContrainteMetier {
	
	public void insererContrainte(Contrainte contrainte);
	public List<Contrainte> listContrainte();
	public List<Contrainte> listContrainteByClasse(Classe classe);
}
