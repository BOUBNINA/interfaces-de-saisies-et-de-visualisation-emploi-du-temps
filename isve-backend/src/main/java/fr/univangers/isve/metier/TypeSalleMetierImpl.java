package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.TypeSalleRepository;
import fr.univangers.isve.entities.TypeSalle;

@Service
public class TypeSalleMetierImpl implements ITypeSalleMetier{
	
	@Autowired
	TypeSalleRepository typeSalleRepository;
	
	@Override
	public void insererTypeSalle(TypeSalle typeSalle) {
		typeSalleRepository.save(typeSalle);	
	}

	@Override
	public List<TypeSalle> listTypeSalles() {
		return typeSalleRepository.findAll();
	}

}
