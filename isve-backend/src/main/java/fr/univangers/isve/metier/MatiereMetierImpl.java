package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.MatiereRepository;
import fr.univangers.isve.entities.Matiere;
import fr.univangers.isve.entities.UniteEnseignement;

@Service
public class MatiereMetierImpl implements IMatiereMetier {
	@Autowired
	private MatiereRepository matiereRepository;
	@Override
	public void insererMatiere(Matiere matiere) {
		matiereRepository.save(matiere);
		
	}

	@Override
	public List<Matiere> listMatieres() {
		return matiereRepository.findAll();
	}

	@Override
	public List<Matiere> listMatiereByUniteEnseignement(UniteEnseignement uniteEnseignement) {
		return matiereRepository.findAllByUniteEnseignement(uniteEnseignement);
	}
	
	

}