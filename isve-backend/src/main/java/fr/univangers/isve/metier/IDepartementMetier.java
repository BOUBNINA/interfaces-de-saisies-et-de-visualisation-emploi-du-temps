package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Universite;

public interface IDepartementMetier {
	
	public void insererDepartement(Departement departement);
	public List<Departement> listDepartement();
	public List<Departement> listDepartementByFaculte(Faculte faculte);
}
