package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.FaculteRepository;
import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Universite;

@Service
public class FaculteMetierImpl implements IFaculteMetier{
	
	@Autowired
	private FaculteRepository faculteRepository; 
	
	@Override
	public void insererFaculte(Faculte faculte) {
		faculteRepository.save(faculte);
	}

	@Override
	public List<Faculte> listFacultes() {
		
		return faculteRepository.findAll();
	}

	@Override
	public List<Faculte> listFaculteByUniversite(Universite universite) {
		return faculteRepository.findAllByUniversite(universite);
	}
	
	
}
