package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.UniteEnseignementRepository;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.UniteEnseignement;


@Service
public class UniteEnseignementMetierImpl implements IUniteEnseignementMetier{
	
	@Autowired
	private UniteEnseignementRepository uniteEnseignementRepository;
	
	
	@Override
	public void insererUniteEnseignement(UniteEnseignement ue) {
		
		uniteEnseignementRepository.save(ue);
	}

	@Override
	public List<UniteEnseignement> listUniteEnseignement() {
		return uniteEnseignementRepository.findAll();
	}

}
