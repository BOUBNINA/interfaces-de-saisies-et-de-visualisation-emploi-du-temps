package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Diplome;
import fr.univangers.isve.entities.Etape;

public interface IDiplomeMetier {
	
	public void insererDiplome(Diplome diplome);
	public List<Diplome> listDiplome();
	public List<Diplome> listDiplomeByEtape(Etape etape);
}
