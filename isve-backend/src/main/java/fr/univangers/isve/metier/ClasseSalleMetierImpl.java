package fr.univangers.isve.metier;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.ClasseRepository;
import fr.univangers.isve.dao.ClasseSalleRepository;
import fr.univangers.isve.dao.SalleRepository;
import fr.univangers.isve.dto.Correspondance;
import fr.univangers.isve.entities.ClasseSalle;
import fr.univangers.isve.entities.ClasseSalleId;
import fr.univangers.isve.entities.Etape;

@Service
public class ClasseSalleMetierImpl implements IClasseSalleMetier {
	
	@Autowired
	private ClasseSalleRepository classeSalleRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private SalleRepository salleRepository;
	
	@Override
	public void insererClasseSalle(ClasseSalle classeSalle) {
		classeSalleRepository.save(classeSalle);
		
	}

	@Override
	public List<ClasseSalle> listClasseSalleByEtape(Etape etape) {
		return classeSalleRepository.findAllByEtape(etape);
	}
	
	@Transactional
	@Override
	public void recupCorrespondance(List<Correspondance> correspondances) {
		for(Correspondance correspondance : correspondances) {
			ClasseSalle correspondanceAInserer = new ClasseSalle();
			correspondanceAInserer.setCode_classe_salle(new ClasseSalleId(correspondance.getCodeClasse(), correspondance.getCodeSalle()));
			correspondanceAInserer.setClasse(classeRepository.getOne(correspondance.getCodeClasse()));
			correspondanceAInserer.setSalle(salleRepository.getOne(correspondance.getCodeSalle()));
			insererClasseSalle(correspondanceAInserer);
			
		}
		
	}

	@Override
	public List<ClasseSalle> listClasseSalles() {
		return classeSalleRepository.findAll();
	}

}
