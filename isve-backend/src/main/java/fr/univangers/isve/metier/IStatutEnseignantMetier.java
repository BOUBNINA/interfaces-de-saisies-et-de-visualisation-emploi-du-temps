package fr.univangers.isve.metier;


import java.util.List;

import fr.univangers.isve.entities.StatutEnseignant;

public interface IStatutEnseignantMetier {
	
	public void insererStatutEnseignant(StatutEnseignant statutEnseignant);
	public List<StatutEnseignant> listStatutEnseignant();
}
