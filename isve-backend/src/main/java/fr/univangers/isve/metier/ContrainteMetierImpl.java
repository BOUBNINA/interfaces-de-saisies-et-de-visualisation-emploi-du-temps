package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.ContrainteRepository;
import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Contrainte;

@Service
public class ContrainteMetierImpl implements IContrainteMetier{
	@Autowired
	private ContrainteRepository contrainteRepository;
	
	@Override
	public void insererContrainte(Contrainte contrainte) {
		contrainteRepository.save(contrainte);
	}

	@Override
	public List<Contrainte> listContrainte() {
		return contrainteRepository.findAll();
	}

	@Override
	public List<Contrainte> listContrainteByClasse(Classe classe) {
		//return contrainteRepository.findAllByClasse(classe);
		return null;
	}

}
