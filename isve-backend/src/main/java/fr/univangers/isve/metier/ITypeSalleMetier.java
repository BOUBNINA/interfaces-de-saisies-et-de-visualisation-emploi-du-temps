package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.TypeSalle;

public interface ITypeSalleMetier {
	
	public void insererTypeSalle(TypeSalle typeSalle);
	public List<TypeSalle> listTypeSalles();
}
