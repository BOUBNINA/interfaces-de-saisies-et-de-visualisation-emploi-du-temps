package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;


public interface IGroupeMetier {
	
	public void insererGroupe(Groupe groupe);
	public List<Groupe> listGroupes();
	public List<Groupe> listGroupesByEtape(Etape etape);

}
