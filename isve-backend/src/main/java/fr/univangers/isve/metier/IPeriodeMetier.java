package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Periode;

public interface IPeriodeMetier {
	
	public void insererPeriode(Periode periode);
	public List<Periode> listPeriode();
	
}
