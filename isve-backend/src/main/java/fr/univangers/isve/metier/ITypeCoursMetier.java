package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.TypeSeance;

public interface ITypeCoursMetier {
	public void insererTypeCours(TypeSeance typeCours);
	public List<TypeSeance> listTypeCours();
}
