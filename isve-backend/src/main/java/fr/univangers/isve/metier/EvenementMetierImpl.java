package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.EvenementRepository;
import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.entities.Evenement;


@Service
public class EvenementMetierImpl implements IEvenementMetier {
	
	private EvenementRepository evenementRepository;
	
	@Override
	public void insererEvenement(Evenement evenement) {
		
		evenementRepository.save(evenement);
	}

	@Override
	public List<Evenement> listEvenement() {
		return evenementRepository.findAll();
	}

	@Override
	public List<Evenement> listEvenementByCalendrier(Calendrier calendrier) {
		return evenementRepository.findAllByCalendrier(calendrier);
	}

}
