package fr.univangers.isve.metier;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.ClasseRepository;
import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.entities.Matiere;


@Service
public class ClasseMetierImpl implements IClasseMetier{
	@Autowired
	private ClasseRepository classeRepository;
	
	@Override
	public void insererClasse(Classe classe) {
		classeRepository.save(classe);
	}

	@Override
	public List<Classe> listClasse() {
		return classeRepository.findAll();
	}

	@Override
	public List<Classe> listClasseByMatiere(Matiere matiere) {
		return classeRepository.findAllByMatiere(matiere);
	}

	@Override
	public List<Classe> listClasseByGroupe(Groupe groupe) {
		return classeRepository.findAllByGroupe(groupe);
	}

	@Override
	public Optional<Classe> getClasseById(Long codeClasse) {
		return classeRepository.findById(codeClasse);
	}

	@Override
	public List<Classe> listClasseByEtape(Etape etape) {
		return classeRepository.findAllByEtape(etape);
	}

}
