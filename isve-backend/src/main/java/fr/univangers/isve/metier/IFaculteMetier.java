package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Universite;

public interface IFaculteMetier {
	
	public void insererFaculte(Faculte faculte);
	public List<Faculte> listFacultes();
	public List<Faculte> listFaculteByUniversite(Universite universite);
	
}


