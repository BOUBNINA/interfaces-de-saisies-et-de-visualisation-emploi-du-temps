package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.SalleRepository;
import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSalle;

@Service
public class SalleMetierImpl implements ISalleMetier{
	
	@Autowired
	private SalleRepository salleRepository;
	
	@Override
	public void insererSalle(Salle salle) {
		salleRepository.save(salle);	
	}

	@Override
	public List<Salle> listSalles() {
		return salleRepository.findAll();
	}

	@Override
	public List<Salle> listSalleByDepartement(Departement departement) {
		return salleRepository.findAllByDepartement(departement);
	}

	@Override
	public List<Salle> listSalleByType(TypeSalle typeSalle) {
		return salleRepository.findAllByTypeSalle(typeSalle);
	}

}
