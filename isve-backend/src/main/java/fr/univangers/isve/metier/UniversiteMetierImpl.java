package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.UniversiteRepository;
import fr.univangers.isve.entities.Universite;

@Service
public class UniversiteMetierImpl implements IUniversiteMetier{
	
	@Autowired
	private UniversiteRepository universiteRepository;
	
	@Override
	public void insererUniversite(Universite universite) {
		universiteRepository.save(universite);
	}

	@Override
	public List<Universite> listUniversite() {
		return universiteRepository.findAll();
	}

}
