package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.StatutEnseignantRepository;
import fr.univangers.isve.entities.StatutEnseignant;

@Service
public class StatutEnseignantMetierImpl implements IStatutEnseignantMetier {
	
	@Autowired
	private StatutEnseignantRepository statutEnseignantRepository;
	
	@Override
	public void insererStatutEnseignant(StatutEnseignant statutEnseignant) {
		statutEnseignantRepository.save(statutEnseignant);
	}

	@Override
	public List<StatutEnseignant> listStatutEnseignant() {
		return statutEnseignantRepository.findAll();
	}
	
}
