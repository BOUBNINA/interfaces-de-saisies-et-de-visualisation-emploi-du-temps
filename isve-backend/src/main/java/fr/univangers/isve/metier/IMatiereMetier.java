package fr.univangers.isve.metier;

import java.util.List;

import fr.univangers.isve.entities.Matiere;
import fr.univangers.isve.entities.UniteEnseignement;

public interface IMatiereMetier{
	public void insererMatiere(Matiere matiere);
	public List<Matiere> listMatieres();
	public List<Matiere> listMatiereByUniteEnseignement(UniteEnseignement uniteEnseignement);
}

