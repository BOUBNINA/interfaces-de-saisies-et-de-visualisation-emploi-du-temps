package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.CalendrierRepository;
import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.entities.Universite;


@Service
public class CalendrierMetierImpl implements ICalendrierMetier{
	@Autowired
	private CalendrierRepository calendrierRepository;
	
	@Override
	public void insererCalendrier(Calendrier calendrier) {
		calendrierRepository.save(calendrier);
	}

	@Override
	public List<Calendrier> listCalendrier() {
		return calendrierRepository.findAll();
	}

	@Override
	public List<Calendrier> listCalendrierByUniversite(Universite universite) {
		return calendrierRepository.findAllByUniversite(universite);
	}

}
