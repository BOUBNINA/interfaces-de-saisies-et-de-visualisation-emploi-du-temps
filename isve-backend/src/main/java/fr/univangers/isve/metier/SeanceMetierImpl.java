package fr.univangers.isve.metier;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import fr.univangers.isve.dao.TypeSeanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.ClasseRepository;
import fr.univangers.isve.dao.SalleRepository;
import fr.univangers.isve.dao.SeanceRepository;
import fr.univangers.isve.entities.Seance;
import fr.univangers.isve.entities.TypeSeance;
import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Enseignant;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.entities.Salle;

@Service
public class SeanceMetierImpl implements ISeanceMetier {
	
	@Autowired
	private SeanceRepository seanceRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private SalleRepository salleRepository;
	@Autowired
	private TypeSeanceRepository typeSeanceRepository;

	@Override
	public void insererSeance(Seance seance) {
		seanceRepository.save(seance);
		
	}

	@Override
	public List<Seance> listSeance() {
		return seanceRepository.findAll();
	}

	@Override
	public List<Seance> listSeanceByClasse(Classe classe) {
		return seanceRepository.findAllByClasse(classe);
	}


	@Override
	public List<Seance> listSeanceByTypeSeance(TypeSeance typeSeance) {
		return seanceRepository.findAllByTypeSeance(typeSeance);
	}

	@Override
	public List<Seance> listSeanceBySalle(Salle salle) {
		return seanceRepository.findAllBySalle(salle);
	}

	@Override
	public List<Seance> listSeanceByEtape(Etape etape) {
		return seanceRepository.findAllByEtape(etape);
	}

	@Override
	public void deleteSeancesByEtape(Etape etape) {
		List<Seance> seances = seanceRepository.findAllByEtape(etape);
		for (Seance seance: seances){
			seanceRepository.delete(seance);
		}
	}

	@Override
	public List<Seance> listSeanceByEtudiant(Etudiant etudiant) {
		return seanceRepository.findAllByEtudiant(etudiant);
	}
	
	@Transactional
	@Override
	public void recupSeanceDTO(List<fr.univangers.isve.dto.Seance> seances) {
		for(fr.univangers.isve.dto.Seance seance : seances) {
			Seance seanceAInserer = new Seance();
			Collection<Enseignant> ens =  seanceAInserer.getEnseignants();
			ens = new ArrayList<Enseignant>();
			Classe classe = classeRepository.getOne(seance.getCodeClasse());
			ens.add(classe.getEnseignant());
			seanceAInserer.setEnseignants(ens);
			seanceAInserer.setClasse(classe);
			seanceAInserer.setSalle(salleRepository.getOne(seance.getCodeSalle()));
			seanceAInserer.setTypeCours(typeSeanceRepository.getOne(seance.getCodeTypeSeance()));
			seanceAInserer.setDateSeance(seance.getDateSeance());
			seanceAInserer.setDureeSeance(seance.getDureeSeance());
			seanceAInserer.setLibelleMatiere(classe.getMatiere().getLibelleMatiere());
			seanceRepository.save(seanceAInserer);
			
		}
		
	}
	
	
}
