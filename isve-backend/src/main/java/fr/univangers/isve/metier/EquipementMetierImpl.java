package fr.univangers.isve.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.EquipementRepository;
import fr.univangers.isve.entities.Equipement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSalle;

@Service
public class EquipementMetierImpl implements IEquipementMetier {
	@Autowired
	private EquipementRepository equipementRepository;
	
	@Override
	public void insererEquipement(Equipement equipement) {
		equipementRepository.save(equipement);
	}

	@Override
	public List<Equipement> listEquipement() {
		return equipementRepository.findAll();
	}

	@Override
	public List<Equipement> listEquipementBySalle(Salle salle) {
		return equipementRepository.findAllBySalle(salle);
	}

}
