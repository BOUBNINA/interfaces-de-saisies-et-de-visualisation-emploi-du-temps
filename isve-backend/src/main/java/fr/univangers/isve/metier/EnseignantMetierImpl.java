package fr.univangers.isve.metier;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.univangers.isve.dao.EnseignantRepository;
import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Enseignant;

@Service
public class EnseignantMetierImpl implements IEnseignantMetier {
	
	@Autowired
	private EnseignantRepository enseignantRepository;
	
	@Override
	public void insererEnseignant(Enseignant enseignant) {
		enseignantRepository.save(enseignant);
		
	}

	@Override
	public List<Enseignant> listEnseignants() {
		return enseignantRepository.findAll();
	}

	@Override
	public List<Enseignant> listEnseignantsByDepartement(Departement departement) {
		return enseignantRepository.findAllByDepartement(departement);
	}

	@Override
	public Optional<Enseignant> getEnseignantById(String matriculeEnseignant) {
		return enseignantRepository.findById(matriculeEnseignant);
	}

}
