package fr.univangers.isve;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsveApplication{

	public static void main(String[] args) throws IOException {
		SpringApplication.run(IsveApplication.class, args);

	}
}
