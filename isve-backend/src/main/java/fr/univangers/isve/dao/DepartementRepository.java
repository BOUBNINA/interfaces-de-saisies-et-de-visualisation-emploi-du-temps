package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Universite;

public interface DepartementRepository extends JpaRepository<Departement, Long>{

	public List<Departement> findAllByFaculte(Faculte faculte);

}
