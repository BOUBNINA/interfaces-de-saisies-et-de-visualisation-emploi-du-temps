package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Matiere;
import fr.univangers.isve.entities.UniteEnseignement;

public interface MatiereRepository extends JpaRepository<Matiere, String> {

	public List<Matiere> findAllByUniteEnseignement(UniteEnseignement uniteEnseignement);

}
