package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.UniteEnseignement;

public interface UniteEnseignementRepository extends JpaRepository<UniteEnseignement, Long> {

}
