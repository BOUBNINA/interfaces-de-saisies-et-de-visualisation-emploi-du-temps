package fr.univangers.isve.dao;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;

public interface EtudiantRepository extends JpaRepository<Etudiant, String>{
	
	public List<Etudiant> findAllByGroupe(Groupe groupe);
}
