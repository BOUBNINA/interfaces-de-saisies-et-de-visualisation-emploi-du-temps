package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.univangers.isve.entities.Seance;
import fr.univangers.isve.entities.TypeSeance;
import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Enseignant;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Etudiant;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.entities.Salle;

public interface SeanceRepository extends JpaRepository<Seance, Long> {

	public List<Seance> findAllByClasse(Classe classe);	
	public List<Seance> findAllBySalle(Salle salle);	
	public List<Seance> findAllByTypeSeance(TypeSeance seance);	
	
	@Query("SELECT DISTINCT s FROM Seance s JOIN s.classe.groupes g WHERE g.etape = ?1")
	public List<Seance> findAllByEtape(Etape etape);


	@Query("SELECT  s FROM Seance s JOIN s.classe.groupes g WHERE g = (SELECT e.groupe FROM Etudiant e WHERE e =  ?1) ")
	public List<Seance> findAllByEtudiant(Etudiant etudiant);

}
