package fr.univangers.isve.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.StatutEnseignant;

public interface StatutEnseignantRepository extends JpaRepository<StatutEnseignant, Long> {

}
