package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Diplome;
import fr.univangers.isve.entities.Etape;

public interface DiplomeRepository extends JpaRepository<Diplome,Long>{

	public List<Diplome> findAllByEtape(Etape etape);

}
