package fr.univangers.isve.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.TypeSeance;

public interface TypeSeanceRepository extends JpaRepository<TypeSeance,Long>{

}
