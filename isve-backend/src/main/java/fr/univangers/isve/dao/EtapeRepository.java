package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Etape;

public interface EtapeRepository extends JpaRepository<Etape, String> {

	public List<Etape> findAllByDepartement(Departement departement);
	
	@Query("SELECT e FROM Etape e WHERE e.departement.faculte = ?1")
	public List<Etape> findAllByFaculte(Faculte faculte);

	

	@Query("SELECT e FROM Etape e WHERE e.libelleEtape = ?1")
	public Etape findByLibelleEtape(String libelleEtape);
	
	@Query("SELECT e.departement.faculte FROM Etape e WHERE e = ?1")
	public Faculte findFaculteByEtape(Etape etape);

}
