package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Commentaire;
import fr.univangers.isve.entities.Seance;

public interface CommentaireRepository extends JpaRepository<Commentaire, Long> {

}


