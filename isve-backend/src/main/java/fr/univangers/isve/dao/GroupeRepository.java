package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;

public interface GroupeRepository extends JpaRepository<Groupe, Long> {

	public List<Groupe> findAllByEtape(Etape etape);

}
