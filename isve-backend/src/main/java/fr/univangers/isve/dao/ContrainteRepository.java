package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Contrainte;

public interface ContrainteRepository extends JpaRepository<Contrainte, Long>{

	//public List<Contrainte> findAllByClasse(Classe classe);

}
