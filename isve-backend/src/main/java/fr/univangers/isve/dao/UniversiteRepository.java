package fr.univangers.isve.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Universite;

public interface UniversiteRepository extends JpaRepository<Universite,Long> {

}
