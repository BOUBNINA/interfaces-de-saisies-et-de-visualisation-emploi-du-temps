package fr.univangers.isve.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.TypeSalle;

public interface TypeSalleRepository extends JpaRepository<TypeSalle, String> {

}
