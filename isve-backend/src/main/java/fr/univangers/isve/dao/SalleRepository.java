package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSalle;

public interface SalleRepository extends JpaRepository<Salle,String> {

	public List<Salle> findAllByDepartement(Departement departement);

	public List<Salle> findAllByTypeSalle(TypeSalle typeSalle);

}
