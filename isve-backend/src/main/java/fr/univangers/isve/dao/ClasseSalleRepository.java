package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.univangers.isve.entities.ClasseSalle;
import fr.univangers.isve.entities.ClasseSalleId;
import fr.univangers.isve.entities.Etape;

public interface ClasseSalleRepository extends JpaRepository<ClasseSalle, ClasseSalleId> {
	
	@Query("SELECT cs FROM ClasseSalle cs JOIN cs.classe.groupes g WHERE g.etape = ?1")
	public List<ClasseSalle> findAllByEtape(Etape etape);

}
