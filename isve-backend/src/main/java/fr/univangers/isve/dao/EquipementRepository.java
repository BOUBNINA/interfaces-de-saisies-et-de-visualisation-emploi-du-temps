package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Equipement;
import fr.univangers.isve.entities.Salle;
import fr.univangers.isve.entities.TypeSalle;

public interface EquipementRepository extends JpaRepository<Equipement, Long> {

	public List<Equipement> findAllBySalle(Salle salle);

}
