package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Departement;
import fr.univangers.isve.entities.Enseignant;

public interface EnseignantRepository extends JpaRepository<Enseignant, String>{
		public List<Enseignant> findAllByDepartement(Departement departement);
}
