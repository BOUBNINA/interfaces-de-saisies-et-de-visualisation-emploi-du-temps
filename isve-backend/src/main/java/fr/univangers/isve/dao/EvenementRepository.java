package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.entities.Evenement;

public interface EvenementRepository 
extends JpaRepository<Evenement, Long> {
	public List<Evenement> findAllByCalendrier(Calendrier calendrier);
}
