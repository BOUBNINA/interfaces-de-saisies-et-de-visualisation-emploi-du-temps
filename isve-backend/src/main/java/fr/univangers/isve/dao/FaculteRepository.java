package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Faculte;
import fr.univangers.isve.entities.Universite;

public interface FaculteRepository extends JpaRepository<Faculte, String> {

	public List<Faculte> findAllByUniversite(Universite universite);
	
}
