package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Calendrier;
import fr.univangers.isve.entities.Universite;

public interface CalendrierRepository extends JpaRepository<Calendrier, Long>{

	public List<Calendrier> findAllByUniversite(Universite universite);

}
