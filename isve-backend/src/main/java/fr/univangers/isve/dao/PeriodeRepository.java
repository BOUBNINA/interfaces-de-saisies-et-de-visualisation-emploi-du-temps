package fr.univangers.isve.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Periode;

public interface PeriodeRepository extends JpaRepository<Periode, Long> {

}
