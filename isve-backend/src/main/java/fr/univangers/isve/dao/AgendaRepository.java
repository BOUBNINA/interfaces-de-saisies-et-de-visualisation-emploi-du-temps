package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univangers.isve.entities.Agenda;
import fr.univangers.isve.entities.Enseignant;

public interface AgendaRepository extends JpaRepository<Agenda, Long>{
	public List<Agenda>findAllByEnseignant(Enseignant enseignant);
}
