package fr.univangers.isve.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.univangers.isve.entities.Classe;
import fr.univangers.isve.entities.Etape;
import fr.univangers.isve.entities.Groupe;
import fr.univangers.isve.entities.Matiere;
import org.springframework.data.jpa.repository.Query;

public interface ClasseRepository extends JpaRepository<Classe, Long>{

	public List<Classe> findAllByMatiere(Matiere matiere);

	@Query("SELECT c FROM Classe c JOIN c.groupes g WHERE g.libelleGroupe = ?1")
	public List<Classe> findAllByGroupe(Groupe groupe);
	
	@Query("SELECT c FROM Classe c JOIN c.groupes g WHERE g.etape = ?1")
	public List<Classe> findAllByEtape(Etape etape);
	
	

}
