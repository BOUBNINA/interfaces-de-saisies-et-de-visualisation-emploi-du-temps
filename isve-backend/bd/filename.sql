-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: emploi_temps
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agenda` (
  `code_agenda` bigint NOT NULL,
  `date_debut_agenda` datetime DEFAULT NULL,
  `date_fin_agenda` datetime DEFAULT NULL,
  `motif_agenda` varchar(255) DEFAULT NULL,
  `matricule_enseignant` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`code_agenda`),
  KEY `FKi9xm9330cl1l6j2bfy38sb4g5` (`matricule_enseignant`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendrier`
--

DROP TABLE IF EXISTS `calendrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `calendrier` (
  `code_calendrier` bigint NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `code_universite` bigint DEFAULT NULL,
  PRIMARY KEY (`code_calendrier`),
  KEY `FKmw3y3qpt9496f0o9tsfb3i4jm` (`code_universite`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendrier`
--

LOCK TABLES `calendrier` WRITE;
/*!40000 ALTER TABLE `calendrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe`
--

DROP TABLE IF EXISTS `classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classe` (
  `code_classe` bigint NOT NULL,
  `code_groupe` bigint DEFAULT NULL,
  `code_matiere` varchar(15) DEFAULT NULL,
  `matricule_enseignant` varchar(15) DEFAULT NULL,
  `code_type_seance` bigint DEFAULT NULL,
  PRIMARY KEY (`code_classe`),
  KEY `FKpw0c1th9d8d3veyiau5eguxw7` (`code_groupe`),
  KEY `FKoivrvu8bj7nqvmr96hperapnu` (`matricule_enseignant`),
  KEY `FK56c4637ya3ati6pxhh1c2eyre` (`code_matiere`),
  KEY `FKa3n6i8vb2u4tphwy7ul765uhd` (`code_type_seance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe`
--

LOCK TABLES `classe` WRITE;
/*!40000 ALTER TABLE `classe` DISABLE KEYS */;
INSERT INTO `classe` VALUES (35,NULL,'9','8',3),(34,NULL,'9','8',2),(33,NULL,'9','8',1),(32,NULL,'8','7',3),(31,NULL,'8','7',1),(30,NULL,'7','6',3),(29,NULL,'7','6',3),(28,NULL,'7','6',3),(27,NULL,'7','6',2),(26,NULL,'7','6',2),(25,NULL,'7','6',1),(24,NULL,'6','5',3),(23,NULL,'6','5',3),(22,NULL,'6','5',3),(21,NULL,'6','5',2),(20,NULL,'6','5',2),(19,NULL,'6','5',1),(18,NULL,'5','9',3),(17,NULL,'5','9',2),(16,NULL,'5','9',1),(15,NULL,'4','3',3),(14,NULL,'4','3',1),(13,NULL,'3','2',3),(12,NULL,'3','2',3),(11,NULL,'3','2',3),(10,NULL,'3','2',1),(9,NULL,'2','1',3),(8,NULL,'2','1',3),(7,NULL,'2','1',3),(6,NULL,'2','1',2),(5,NULL,'2','1',2),(4,NULL,'2','1',1),(3,NULL,'1','4',3),(2,NULL,'1','4',3),(1,NULL,'1','4',3);
/*!40000 ALTER TABLE `classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe_contrainte`
--

DROP TABLE IF EXISTS `classe_contrainte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classe_contrainte` (
  `code_clase` bigint NOT NULL,
  `code_contrainte` bigint NOT NULL,
  KEY `FK3eh11ijn4l0dirr6pbkqv7o3g` (`code_contrainte`),
  KEY `FKt81oodwj2g2gb5m66359rhs8m` (`code_clase`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe_contrainte`
--

LOCK TABLES `classe_contrainte` WRITE;
/*!40000 ALTER TABLE `classe_contrainte` DISABLE KEYS */;
/*!40000 ALTER TABLE `classe_contrainte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe_groupe`
--

DROP TABLE IF EXISTS `classe_groupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classe_groupe` (
  `code_classe` bigint NOT NULL,
  `numero_groupe` bigint NOT NULL,
  KEY `FK59726gfr8a1m2nlsibihrlfd7` (`numero_groupe`),
  KEY `FKgb1q45pccpucqcfop84u12fw3` (`code_classe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe_groupe`
--

LOCK TABLES `classe_groupe` WRITE;
/*!40000 ALTER TABLE `classe_groupe` DISABLE KEYS */;
INSERT INTO `classe_groupe` VALUES (35,4),(34,4),(33,4),(32,6),(31,6),(30,7),(29,6),(29,5),(28,4),(27,3),(26,2),(25,1),(24,7),(23,6),(23,5),(22,4),(21,3),(20,2),(19,1),(18,7),(17,7),(16,7),(15,5),(14,5),(13,7),(12,6),(12,5),(11,4),(10,1),(9,7),(8,6),(8,5),(7,4),(6,3),(5,2),(4,1),(3,7),(2,6),(2,5),(1,4);
/*!40000 ALTER TABLE `classe_groupe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe_salle`
--

DROP TABLE IF EXISTS `classe_salle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `classe_salle` (
  `code_classe` bigint NOT NULL,
  `code_salle` varchar(255) NOT NULL,
  `penalty` int DEFAULT NULL,
  PRIMARY KEY (`code_classe`,`code_salle`),
  KEY `FKs3ml6utnm9y78xqwubpb6gt6h` (`code_salle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe_salle`
--

LOCK TABLES `classe_salle` WRITE;
/*!40000 ALTER TABLE `classe_salle` DISABLE KEYS */;
INSERT INTO `classe_salle` VALUES (35,'3',1),(35,'2',0),(35,'1',2),(34,'5',1),(34,'4',0),(33,'5',1),(33,'4',0),(32,'3',1),(32,'2',0),(32,'1',2),(31,'5',1),(31,'4',0),(30,'3',1),(30,'2',0),(30,'1',2),(29,'3',1),(29,'2',0),(29,'1',2),(28,'3',1),(28,'2',0),(28,'1',2),(27,'5',1),(27,'4',0),(26,'5',1),(26,'4',0),(25,'5',1),(25,'4',0),(24,'3',1),(24,'2',0),(24,'1',2),(23,'3',1),(23,'2',0),(23,'1',2),(22,'3',1),(22,'2',0),(22,'1',2),(21,'5',1),(21,'4',0),(20,'5',1),(20,'4',0),(19,'5',1),(19,'4',0),(18,'3',1),(18,'2',0),(18,'1',2),(17,'5',1),(17,'4',0),(16,'5',1),(16,'4',0),(15,'3',1),(15,'2',0),(15,'1',2),(14,'5',1),(14,'4',0),(13,'3',1),(13,'2',0),(13,'1',2),(12,'3',1),(12,'2',0),(12,'1',2),(11,'3',1),(11,'2',0),(11,'1',2),(10,'5',1),(10,'4',0),(9,'3',1),(9,'2',0),(9,'1',2),(8,'3',1),(8,'2',0),(8,'1',2),(7,'3',1),(7,'2',0),(7,'1',2),(6,'5',1),(6,'4',0),(5,'5',1),(5,'4',0),(4,'5',1),(4,'4',0),(3,'3',1),(3,'2',0),(3,'1',2),(2,'3',1),(2,'2',0),(2,'1',2),(1,'3',1),(1,'2',0),(1,'1',2);
/*!40000 ALTER TABLE `classe_salle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commentaire` (
  `numero_commentaire` bigint NOT NULL,
  `libelle_commentaire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`numero_commentaire`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaire`
--

LOCK TABLES `commentaire` WRITE;
/*!40000 ALTER TABLE `commentaire` DISABLE KEYS */;
INSERT INTO `commentaire` VALUES (1,'Ceci est un commentaire');
/*!40000 ALTER TABLE `commentaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrainte`
--

DROP TABLE IF EXISTS `contrainte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contrainte` (
  `code_contrainte` bigint NOT NULL,
  `type_contrainte` varchar(255) DEFAULT NULL,
  `code_classe` bigint DEFAULT NULL,
  PRIMARY KEY (`code_contrainte`),
  KEY `FKkrvm7h3acf3wipe5sqgndbkjf` (`code_classe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrainte`
--

LOCK TABLES `contrainte` WRITE;
/*!40000 ALTER TABLE `contrainte` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrainte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departement`
--

DROP TABLE IF EXISTS `departement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departement` (
  `code_departement` bigint NOT NULL,
  `nom_departement` varchar(255) DEFAULT NULL,
  `code_faculte` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_departement`),
  KEY `FKk4cv1gydvl4j96cqs8a2gf88s` (`code_faculte`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departement`
--

LOCK TABLES `departement` WRITE;
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
INSERT INTO `departement` VALUES (200,'Maths','1'),(100,'Informatique','1');
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diplome`
--

DROP TABLE IF EXISTS `diplome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `diplome` (
  `code_diplome` bigint NOT NULL,
  `libelle_diplome` varchar(255) DEFAULT NULL,
  `code_etape` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`code_diplome`),
  KEY `FKrsa150876enwxknlfmnrwmfpj` (`code_etape`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diplome`
--

LOCK TABLES `diplome` WRITE;
/*!40000 ALTER TABLE `diplome` DISABLE KEYS */;
/*!40000 ALTER TABLE `diplome` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enseignant`
--

DROP TABLE IF EXISTS `enseignant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enseignant` (
  `matricule_enseignant` varchar(15) NOT NULL,
  `obligation_theorique_enseignent` int DEFAULT NULL,
  `email_enseignant` varchar(100) DEFAULT NULL,
  `nom_enseignant` varchar(45) DEFAULT NULL,
  `prenom_enseignant` varchar(60) DEFAULT NULL,
  `code_departement` bigint DEFAULT NULL,
  `code_statut` bigint DEFAULT NULL,
  PRIMARY KEY (`matricule_enseignant`),
  KEY `FKpowvieb1no4nsit1o1cdhrkrm` (`code_departement`),
  KEY `FKr4s61bp5yux97flyk91da3vdx` (`code_statut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enseignant`
--

LOCK TABLES `enseignant` WRITE;
/*!40000 ALTER TABLE `enseignant` DISABLE KEYS */;
INSERT INTO `enseignant` VALUES ('8',1,'C.L@univ-angers.fr','Lefevre','Claire',100,NULL),('7',1,'D.L@univ-angers.fr','Lesaint','David',100,NULL),('6',1,'B.D@univ-angers.fr','Damota','Benoit',100,NULL),('5',1,'E.M@univ-angers.fr','Monfroy','Eric',100,NULL),('4',1,'P.T@univ-angers.fr','Torres','Philippe',100,NULL),('3',1,'V.B@univ-angers.fr','Barichard','Vincent',100,NULL),('2',1,'F.L@univ-angers.fr','Lardeux','Frédéric',100,NULL),('1',1,'O.G@univ-angers.fr','Goudet','Olivier',100,NULL),('9',1,'P.T@univ-angers.fr','Tea','Pot',100,NULL);
/*!40000 ALTER TABLE `enseignant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipement`
--

DROP TABLE IF EXISTS `equipement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipement` (
  `type_equipement` varchar(2) NOT NULL,
  `code_equipement` bigint NOT NULL,
  `nom_equipement` varchar(255) DEFAULT NULL,
  `code_salle` varchar(255) DEFAULT NULL,
  `code_ue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_equipement`),
  KEY `FK68v9eo3shu6lurwio3na5hxbo` (`code_salle`),
  KEY `FK3px70fx6f1mxvkbew2qg13g8h` (`code_ue`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipement`
--

LOCK TABLES `equipement` WRITE;
/*!40000 ALTER TABLE `equipement` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etape`
--

DROP TABLE IF EXISTS `etape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etape` (
  `code_etape` varchar(15) NOT NULL,
  `libelle_etape` varchar(100) DEFAULT NULL,
  `code_departement` bigint DEFAULT NULL,
  PRIMARY KEY (`code_etape`),
  KEY `FK1l3oqpo80effxjfns9a5n8d1o` (`code_departement`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etape`
--

LOCK TABLES `etape` WRITE;
/*!40000 ALTER TABLE `etape` DISABLE KEYS */;
INSERT INTO `etape` VALUES ('10','M2 Maths',200),('9','M1 Maths',200),('8','L3 Maths',200),('7','L2 Maths',200),('6','L1 Maths',200),('5','M2 Informatique',100),('4','M1 Informatique',100),('3','L3 Informatique',100),('2','L2 Informatique',100),('1','L1 Informatique',100);
/*!40000 ALTER TABLE `etape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etudiant`
--

DROP TABLE IF EXISTS `etudiant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etudiant` (
  `code_etudiant` varchar(255) NOT NULL,
  `email_etudiant` varchar(255) DEFAULT NULL,
  `nom_etudiant` varchar(255) DEFAULT NULL,
  `prenom_etudiant` varchar(255) DEFAULT NULL,
  `code_groupe` bigint DEFAULT NULL,
  PRIMARY KEY (`code_etudiant`),
  KEY `FKrjjq13ic58vacwi09blk6c00c` (`code_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etudiant`
--

LOCK TABLES `etudiant` WRITE;
/*!40000 ALTER TABLE `etudiant` DISABLE KEYS */;
/*!40000 ALTER TABLE `etudiant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `evenement` (
  `code_evenement` bigint NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `type_evenement` varchar(255) DEFAULT NULL,
  `code_calendrier` bigint DEFAULT NULL,
  PRIMARY KEY (`code_evenement`),
  KEY `FK8sufww86ifysc0xx38elwjusg` (`code_calendrier`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evenement`
--

LOCK TABLES `evenement` WRITE;
/*!40000 ALTER TABLE `evenement` DISABLE KEYS */;
/*!40000 ALTER TABLE `evenement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculte`
--

DROP TABLE IF EXISTS `faculte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `faculte` (
  `code_faculte` varchar(255) NOT NULL,
  `lieu_faculte` varchar(200) DEFAULT NULL,
  `nom_faculte` varchar(200) DEFAULT NULL,
  `code_universite` bigint DEFAULT NULL,
  PRIMARY KEY (`code_faculte`),
  KEY `FKfti3usn1jt02b5afq9hd1yn9i` (`code_universite`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculte`
--

LOCK TABLES `faculte` WRITE;
/*!40000 ALTER TABLE `faculte` DISABLE KEYS */;
INSERT INTO `faculte` VALUES ('2','Saint serger','Economie',10),('1','Belle-beille','Sciences',10);
/*!40000 ALTER TABLE `faculte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groupe` (
  `numero_groupe` bigint NOT NULL,
  `libelle_groupe` varchar(30) DEFAULT NULL,
  `code_etape` varchar(15) DEFAULT NULL,
  `code_groupe_parent` bigint DEFAULT NULL,
  PRIMARY KEY (`numero_groupe`),
  KEY `FK2ya1r8xhar59bklcxs2nlvyhl` (`code_etape`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupe`
--

LOCK TABLES `groupe` WRITE;
/*!40000 ALTER TABLE `groupe` DISABLE KEYS */;
INSERT INTO `groupe` VALUES (7,'CM-TDB-TPCPROD','3',3),(6,'CM-TDB-TPBQT','3',3),(5,'CM-TDA-TPBSYNT','3',2),(4,'CM-TDA-TPASYS','3',2),(3,'CM-TDB','3',1),(2,'CM-TDA','3',1),(1,'CM','3',0);
/*!40000 ALTER TABLE `groupe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801),(2801);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `matiere` (
  `code_matiere` varchar(15) NOT NULL,
  `charge_cm_matiere` int DEFAULT NULL,
  `charge_tp_matiere` int DEFAULT NULL,
  `ects_matiere` int DEFAULT NULL,
  `libelle_matiere` varchar(100) DEFAULT NULL,
  `volume_horaire_matiere` varchar(255) DEFAULT NULL,
  `code_ue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_matiere`),
  KEY `FK6wh1bwxhwaee9chhsxb3vpwyg` (`code_ue`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matiere`
--

LOCK TABLES `matiere` WRITE;
/*!40000 ALTER TABLE `matiere` DISABLE KEYS */;
INSERT INTO `matiere` VALUES ('9',1,20,10,'Sys intelligents',NULL,'4'),('8',1,20,10,'QT avancé',NULL,'4'),('6',1,20,10,'Prog Fonctionnelle',NULL,'3'),('7',1,20,10,'Prog Logique',NULL,'3'),('5',1,20,10,'Production automatisée de doc',NULL,'4'),('4',1,20,10,'Image de Synthèse',NULL,'4'),('3',1,20,10,'Dev Web',NULL,'2'),('2',1,20,10,'BDD',NULL,'1'),('1',1,20,10,'Anglais',NULL,'5');
/*!40000 ALTER TABLE `matiere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periode`
--

DROP TABLE IF EXISTS `periode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `periode` (
  `code_periode` bigint NOT NULL,
  `annee_periode` varchar(255) DEFAULT NULL,
  `libelle_periode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periode`
--

LOCK TABLES `periode` WRITE;
/*!40000 ALTER TABLE `periode` DISABLE KEYS */;
INSERT INTO `periode` VALUES (1,'2020-2021','2e semestre');
/*!40000 ALTER TABLE `periode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programme`
--

DROP TABLE IF EXISTS `programme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `programme` (
  `code_programme` bigint NOT NULL,
  `duree_programme` varchar(255) DEFAULT NULL,
  `jour_programme` varchar(255) DEFAULT NULL,
  `penalty_programme` int DEFAULT NULL,
  `semaine_programme` varchar(255) DEFAULT NULL,
  `code_classe` bigint DEFAULT NULL,
  `debut_programme` int DEFAULT NULL,
  PRIMARY KEY (`code_programme`),
  KEY `FKas7hhp33w2rftxcildm5inb5r` (`code_classe`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programme`
--

LOCK TABLES `programme` WRITE;
/*!40000 ALTER TABLE `programme` DISABLE KEYS */;
INSERT INTO `programme` VALUES (874,'1','00001',44,'1111111111',35,4),(873,'1','00001',10,'1111111111',35,3),(872,'1','00001',32,'1111111111',35,2),(871,'1','00001',4,'1111111111',35,1),(870,'1','00010',11,'1111111111',35,5),(869,'1','00010',2,'1111111111',35,4),(868,'1','00010',7,'1111111111',35,3),(867,'1','00010',9,'1111111111',35,2),(866,'1','00010',36,'1111111111',35,1),(865,'1','00100',21,'1111111111',35,5),(864,'1','00100',32,'1111111111',35,4),(863,'1','00100',13,'1111111111',35,3),(862,'1','00100',13,'1111111111',35,2),(861,'1','00100',20,'1111111111',35,1),(860,'1','01000',35,'1111111111',35,5),(859,'1','01000',1,'1111111111',35,4),(858,'1','01000',4,'1111111111',35,3),(857,'1','01000',25,'1111111111',35,2),(856,'1','01000',40,'1111111111',35,1),(855,'1','10000',23,'1111111111',35,5),(854,'1','10000',14,'1111111111',35,4),(853,'1','10000',0,'1111111111',35,3),(852,'1','10000',20,'1111111111',35,2),(851,'1','10000',8,'1111111111',35,1),(850,'1','00001',44,'1111111111',34,5),(849,'1','00001',32,'1111111111',34,4),(848,'1','00001',25,'1111111111',34,3),(847,'1','00001',18,'1111111111',34,2),(846,'1','00001',22,'1111111111',34,1),(845,'1','00010',18,'1111111111',34,5),(844,'1','00010',2,'1111111111',34,4),(843,'1','00010',4,'1111111111',34,3),(842,'1','00010',5,'1111111111',34,2),(841,'1','00010',41,'1111111111',34,1),(840,'1','00100',6,'1111111111',34,5),(839,'1','00100',13,'1111111111',34,4),(838,'1','00100',3,'1111111111',34,3),(837,'1','00100',29,'1111111111',34,2),(836,'1','00100',8,'1111111111',34,1),(835,'1','01000',29,'1111111111',34,5),(834,'1','01000',28,'1111111111',34,4),(833,'1','01000',28,'1111111111',34,3),(832,'1','01000',22,'1111111111',34,2),(831,'1','01000',42,'1111111111',34,1),(830,'1','10000',29,'1111111111',34,5),(829,'1','10000',39,'1111111111',34,4),(828,'1','10000',28,'1111111111',34,3),(827,'1','10000',41,'1111111111',34,2),(826,'1','10000',34,'1111111111',34,1),(825,'1','00001',40,'1111111111',33,5),(824,'1','00001',43,'1111111111',33,4),(823,'1','00001',25,'1111111111',33,3),(822,'1','00001',11,'1111111111',33,2),(821,'1','00001',1,'1111111111',33,1),(820,'1','00010',25,'1111111111',33,5),(819,'1','00010',26,'1111111111',33,4),(818,'1','00010',38,'1111111111',33,3),(817,'1','00010',9,'1111111111',33,2),(816,'1','00010',14,'1111111111',33,1),(815,'1','00100',5,'1111111111',33,5),(814,'1','00100',39,'1111111111',33,4),(813,'1','00100',31,'1111111111',33,3),(812,'1','00100',0,'1111111111',33,2),(811,'1','00100',43,'1111111111',33,1),(810,'1','01000',28,'1111111111',33,5),(809,'1','01000',34,'1111111111',33,4),(808,'1','01000',7,'1111111111',33,3),(807,'1','01000',5,'1111111111',33,2),(806,'1','01000',38,'1111111111',33,1),(805,'1','10000',18,'1111111111',33,5),(804,'1','10000',22,'1111111111',33,4),(803,'1','10000',35,'1111111111',33,3),(802,'1','10000',26,'1111111111',33,2),(801,'1','10000',40,'1111111111',33,1),(800,'1','00001',40,'1111111111',32,5),(799,'1','00001',20,'1111111111',32,4),(798,'1','00001',24,'1111111111',32,3),(797,'1','00001',13,'1111111111',32,2),(796,'1','00001',26,'1111111111',32,1),(795,'1','00010',41,'1111111111',32,5),(794,'1','00010',19,'1111111111',32,4),(793,'1','00010',29,'1111111111',32,3),(792,'1','00010',8,'1111111111',32,2),(791,'1','00010',32,'1111111111',32,1),(790,'1','00100',5,'1111111111',32,5),(789,'1','00100',30,'1111111111',32,4),(788,'1','00100',38,'1111111111',32,3),(787,'1','00100',43,'1111111111',32,2),(786,'1','00100',21,'1111111111',32,1),(785,'1','01000',4,'1111111111',32,5),(784,'1','01000',5,'1111111111',32,4),(783,'1','01000',38,'1111111111',32,3),(782,'1','01000',11,'1111111111',32,2),(781,'1','01000',9,'1111111111',32,1),(780,'1','10000',14,'1111111111',32,5),(779,'1','10000',44,'1111111111',32,4),(778,'1','10000',20,'1111111111',32,3),(777,'1','10000',42,'1111111111',32,2),(776,'1','10000',7,'1111111111',32,1),(775,'1','00001',21,'1111111111',31,5),(774,'1','00001',20,'1111111111',31,4),(773,'1','00001',36,'1111111111',31,3),(772,'1','00001',38,'1111111111',31,2),(771,'1','00001',7,'1111111111',31,1),(770,'1','00010',10,'1111111111',31,5),(769,'1','00010',8,'1111111111',31,4),(768,'1','00010',37,'1111111111',31,3),(767,'1','00010',12,'1111111111',31,2),(766,'1','00010',41,'1111111111',31,1),(765,'1','00100',23,'1111111111',31,5),(764,'1','00100',35,'1111111111',31,4),(763,'1','00100',24,'1111111111',31,3),(762,'1','00100',30,'1111111111',31,2),(761,'1','00100',11,'1111111111',31,1),(760,'1','01000',34,'1111111111',31,5),(759,'1','01000',27,'1111111111',31,4),(758,'1','01000',43,'1111111111',31,3),(757,'1','01000',23,'1111111111',31,2),(756,'1','01000',32,'1111111111',31,1),(755,'1','10000',33,'1111111111',31,5),(754,'1','10000',36,'1111111111',31,4),(753,'1','10000',4,'1111111111',31,3),(752,'1','10000',11,'1111111111',31,2),(751,'1','10000',14,'1111111111',31,1),(750,'1','00001',27,'1111111111',30,5),(749,'1','00001',20,'1111111111',30,4),(748,'1','00001',3,'1111111111',30,3),(747,'1','00001',36,'1111111111',30,2),(746,'1','00001',44,'1111111111',30,1),(745,'1','00010',21,'1111111111',30,5),(744,'1','00010',28,'1111111111',30,4),(743,'1','00010',9,'1111111111',30,3),(742,'1','00010',3,'1111111111',30,2),(741,'1','00010',40,'1111111111',30,1),(740,'1','00100',38,'1111111111',30,5),(739,'1','00100',8,'1111111111',30,4),(738,'1','00100',14,'1111111111',30,3),(737,'1','00100',26,'1111111111',30,2),(736,'1','00100',37,'1111111111',30,1),(735,'1','01000',30,'1111111111',30,5),(734,'1','01000',29,'1111111111',30,4),(733,'1','01000',25,'1111111111',30,3),(732,'1','01000',16,'1111111111',30,2),(731,'1','01000',14,'1111111111',30,1),(730,'1','10000',2,'1111111111',30,5),(729,'1','10000',23,'1111111111',30,4),(728,'1','10000',9,'1111111111',30,3),(727,'1','10000',23,'1111111111',30,2),(726,'1','10000',1,'1111111111',30,1),(725,'1','00001',27,'1111111111',29,5),(724,'1','00001',39,'1111111111',29,4),(723,'1','00001',43,'1111111111',29,3),(722,'1','00001',8,'1111111111',29,2),(721,'1','00001',36,'1111111111',29,1),(720,'1','00010',2,'1111111111',29,5),(719,'1','00010',34,'1111111111',29,4),(718,'1','00010',8,'1111111111',29,3),(717,'1','00010',29,'1111111111',29,2),(716,'1','00010',10,'1111111111',29,1),(715,'1','00100',19,'1111111111',29,5),(714,'1','00100',24,'1111111111',29,4),(713,'1','00100',35,'1111111111',29,3),(712,'1','00100',21,'1111111111',29,2),(711,'1','00100',21,'1111111111',29,1),(710,'1','01000',30,'1111111111',29,5),(709,'1','01000',28,'1111111111',29,4),(708,'1','01000',21,'1111111111',29,3),(707,'1','01000',30,'1111111111',29,2),(706,'1','01000',37,'1111111111',29,1),(705,'1','10000',34,'1111111111',29,5),(704,'1','10000',29,'1111111111',29,4),(703,'1','10000',28,'1111111111',29,3),(702,'1','10000',38,'1111111111',29,2),(701,'1','10000',0,'1111111111',29,1),(700,'1','00001',36,'1111111111',28,5),(699,'1','00001',42,'1111111111',28,4),(698,'1','00001',31,'1111111111',28,3),(697,'1','00001',23,'1111111111',28,2),(696,'1','00001',17,'1111111111',28,1),(695,'1','00010',16,'1111111111',28,5),(694,'1','00010',34,'1111111111',28,4),(693,'1','00010',1,'1111111111',28,3),(692,'1','00010',1,'1111111111',28,2),(691,'1','00010',5,'1111111111',28,1),(690,'1','00100',8,'1111111111',28,5),(689,'1','00100',39,'1111111111',28,4),(688,'1','00100',33,'1111111111',28,3),(687,'1','00100',41,'1111111111',28,2),(686,'1','00100',19,'1111111111',28,1),(685,'1','01000',25,'1111111111',28,5),(684,'1','01000',14,'1111111111',28,4),(683,'1','01000',11,'1111111111',28,3),(682,'1','01000',2,'1111111111',28,2),(681,'1','01000',31,'1111111111',28,1),(680,'1','10000',37,'1111111111',28,5),(679,'1','10000',0,'1111111111',28,4),(678,'1','10000',26,'1111111111',28,3),(677,'1','10000',44,'1111111111',28,2),(676,'1','10000',22,'1111111111',28,1),(675,'1','00001',0,'1111111111',27,5),(674,'1','00001',6,'1111111111',27,4),(673,'1','00001',39,'1111111111',27,3),(672,'1','00001',43,'1111111111',27,2),(671,'1','00001',3,'1111111111',27,1),(670,'1','00010',2,'1111111111',27,5),(669,'1','00010',0,'1111111111',27,4),(668,'1','00010',13,'1111111111',27,3),(667,'1','00010',31,'1111111111',27,2),(666,'1','00010',11,'1111111111',27,1),(665,'1','00100',3,'1111111111',27,5),(664,'1','00100',30,'1111111111',27,4),(663,'1','00100',13,'1111111111',27,3),(662,'1','00100',44,'1111111111',27,2),(661,'1','00100',23,'1111111111',27,1),(660,'1','01000',36,'1111111111',27,5),(659,'1','01000',34,'1111111111',27,4),(658,'1','01000',21,'1111111111',27,3),(657,'1','01000',9,'1111111111',27,2),(656,'1','01000',43,'1111111111',27,1),(655,'1','10000',24,'1111111111',27,5),(654,'1','10000',40,'1111111111',27,4),(653,'1','10000',18,'1111111111',27,3),(652,'1','10000',10,'1111111111',27,2),(651,'1','10000',18,'1111111111',27,1),(650,'1','00001',9,'1111111111',26,5),(649,'1','00001',25,'1111111111',26,4),(648,'1','00001',37,'1111111111',26,3),(647,'1','00001',3,'1111111111',26,2),(646,'1','00001',31,'1111111111',26,1),(645,'1','00010',32,'1111111111',26,5),(644,'1','00010',19,'1111111111',26,4),(643,'1','00010',31,'1111111111',26,3),(642,'1','00010',32,'1111111111',26,2),(641,'1','00010',10,'1111111111',26,1),(640,'1','00100',19,'1111111111',26,5),(639,'1','00100',24,'1111111111',26,4),(638,'1','00100',9,'1111111111',26,3),(637,'1','00100',18,'1111111111',26,2),(636,'1','00100',31,'1111111111',26,1),(635,'1','01000',22,'1111111111',26,5),(634,'1','01000',44,'1111111111',26,4),(633,'1','01000',20,'1111111111',26,3),(632,'1','01000',0,'1111111111',26,2),(631,'1','01000',0,'1111111111',26,1),(630,'1','10000',37,'1111111111',26,5),(629,'1','10000',10,'1111111111',26,4),(628,'1','10000',32,'1111111111',26,3),(627,'1','10000',4,'1111111111',26,2),(626,'1','10000',4,'1111111111',26,1),(625,'1','00001',18,'1111111111',25,5),(624,'1','00001',5,'1111111111',25,4),(623,'1','00001',8,'1111111111',25,3),(622,'1','00001',42,'1111111111',25,2),(621,'1','00001',9,'1111111111',25,1),(620,'1','00010',2,'1111111111',25,5),(619,'1','00010',2,'1111111111',25,4),(618,'1','00010',40,'1111111111',25,3),(617,'1','00010',14,'1111111111',25,2),(616,'1','00010',8,'1111111111',25,1),(615,'1','00100',44,'1111111111',25,5),(614,'1','00100',16,'1111111111',25,4),(613,'1','00100',7,'1111111111',25,3),(612,'1','00100',26,'1111111111',25,2),(611,'1','00100',24,'1111111111',25,1),(610,'1','01000',35,'1111111111',25,5),(609,'1','01000',16,'1111111111',25,4),(608,'1','01000',2,'1111111111',25,3),(607,'1','01000',44,'1111111111',25,2),(606,'1','01000',38,'1111111111',25,1),(605,'1','10000',41,'1111111111',25,5),(604,'1','10000',21,'1111111111',25,4),(603,'1','10000',1,'1111111111',25,3),(602,'1','10000',41,'1111111111',25,2),(601,'1','10000',4,'1111111111',25,1),(600,'1','00001',23,'1111111111',24,5),(599,'1','00001',16,'1111111111',24,4),(598,'1','00001',44,'1111111111',24,3),(597,'1','00001',9,'1111111111',24,2),(596,'1','00001',24,'1111111111',24,1),(595,'1','00010',40,'1111111111',24,5),(594,'1','00010',44,'1111111111',24,4),(593,'1','00010',16,'1111111111',24,3),(592,'1','00010',2,'1111111111',24,2),(591,'1','00010',12,'1111111111',24,1),(590,'1','00100',2,'1111111111',24,5),(589,'1','00100',39,'1111111111',24,4),(588,'1','00100',22,'1111111111',24,3),(587,'1','00100',2,'1111111111',24,2),(586,'1','00100',35,'1111111111',24,1),(585,'1','01000',15,'1111111111',24,5),(584,'1','01000',25,'1111111111',24,4),(583,'1','01000',33,'1111111111',24,3),(582,'1','01000',37,'1111111111',24,2),(581,'1','01000',4,'1111111111',24,1),(580,'1','10000',30,'1111111111',24,5),(579,'1','10000',24,'1111111111',24,4),(578,'1','10000',3,'1111111111',24,3),(577,'1','10000',42,'1111111111',24,2),(576,'1','10000',25,'1111111111',24,1),(575,'1','00001',38,'1111111111',23,5),(574,'1','00001',41,'1111111111',23,4),(573,'1','00001',41,'1111111111',23,3),(572,'1','00001',35,'1111111111',23,2),(571,'1','00001',27,'1111111111',23,1),(570,'1','00010',3,'1111111111',23,5),(569,'1','00010',41,'1111111111',23,4),(568,'1','00010',41,'1111111111',23,3),(567,'1','00010',36,'1111111111',23,2),(566,'1','00010',20,'1111111111',23,1),(565,'1','00100',37,'1111111111',23,5),(564,'1','00100',0,'1111111111',23,4),(563,'1','00100',37,'1111111111',23,3),(562,'1','00100',40,'1111111111',23,2),(561,'1','00100',36,'1111111111',23,1),(560,'1','01000',38,'1111111111',23,5),(559,'1','01000',8,'1111111111',23,4),(558,'1','01000',37,'1111111111',23,3),(557,'1','01000',20,'1111111111',23,2),(556,'1','01000',19,'1111111111',23,1),(555,'1','10000',41,'1111111111',23,5),(554,'1','10000',41,'1111111111',23,4),(553,'1','10000',41,'1111111111',23,3),(552,'1','10000',19,'1111111111',23,2),(551,'1','10000',21,'1111111111',23,1),(550,'1','00001',8,'1111111111',22,5),(549,'1','00001',39,'1111111111',22,4),(548,'1','00001',15,'1111111111',22,3),(547,'1','00001',37,'1111111111',22,2),(546,'1','00001',13,'1111111111',22,1),(545,'1','00010',33,'1111111111',22,5),(544,'1','00010',26,'1111111111',22,4),(543,'1','00010',43,'1111111111',22,3),(542,'1','00010',9,'1111111111',22,2),(541,'1','00010',18,'1111111111',22,1),(540,'1','00100',44,'1111111111',22,5),(539,'1','00100',13,'1111111111',22,4),(538,'1','00100',39,'1111111111',22,3),(537,'1','00100',20,'1111111111',22,2),(536,'1','00100',29,'1111111111',22,1),(535,'1','01000',24,'1111111111',22,5),(534,'1','01000',41,'1111111111',22,4),(533,'1','01000',11,'1111111111',22,3),(532,'1','01000',33,'1111111111',22,2),(531,'1','01000',29,'1111111111',22,1),(530,'1','10000',35,'1111111111',22,5),(529,'1','10000',27,'1111111111',22,4),(528,'1','10000',16,'1111111111',22,3),(527,'1','10000',39,'1111111111',22,2),(526,'1','10000',41,'1111111111',22,1),(525,'1','00001',20,'1111111111',21,5),(524,'1','00001',39,'1111111111',21,4),(523,'1','00001',9,'1111111111',21,3),(522,'1','00001',18,'1111111111',21,2),(521,'1','00001',29,'1111111111',21,1),(520,'1','00010',24,'1111111111',21,5),(519,'1','00010',8,'1111111111',21,4),(518,'1','00010',13,'1111111111',21,3),(517,'1','00010',40,'1111111111',21,2),(516,'1','00010',35,'1111111111',21,1),(515,'1','00100',37,'1111111111',21,5),(514,'1','00100',2,'1111111111',21,4),(513,'1','00100',21,'1111111111',21,3),(512,'1','00100',15,'1111111111',21,2),(511,'1','00100',3,'1111111111',21,1),(510,'1','01000',27,'1111111111',21,5),(509,'1','01000',15,'1111111111',21,4),(508,'1','01000',30,'1111111111',21,3),(507,'1','01000',30,'1111111111',21,2),(506,'1','01000',25,'1111111111',21,1),(505,'1','10000',12,'1111111111',21,5),(504,'1','10000',5,'1111111111',21,4),(503,'1','10000',17,'1111111111',21,3),(502,'1','10000',33,'1111111111',21,2),(501,'1','10000',30,'1111111111',21,1),(500,'1','00001',30,'1111111111',20,5),(499,'1','00001',18,'1111111111',20,4),(498,'1','00001',0,'1111111111',20,3),(497,'1','00001',44,'1111111111',20,2),(496,'1','00001',25,'1111111111',20,1),(495,'1','00010',23,'1111111111',20,5),(494,'1','00010',2,'1111111111',20,4),(493,'1','00010',40,'1111111111',20,3),(492,'1','00010',29,'1111111111',20,2),(491,'1','00010',42,'1111111111',20,1),(490,'1','00100',26,'1111111111',20,5),(489,'1','00100',29,'1111111111',20,4),(488,'1','00100',7,'1111111111',20,3),(487,'1','00100',30,'1111111111',20,2),(486,'1','00100',23,'1111111111',20,1),(485,'1','01000',21,'1111111111',20,5),(484,'1','01000',19,'1111111111',20,4),(483,'1','01000',30,'1111111111',20,3),(482,'1','01000',13,'1111111111',20,2),(481,'1','01000',33,'1111111111',20,1),(480,'1','10000',27,'1111111111',20,5),(479,'1','10000',27,'1111111111',20,4),(478,'1','10000',20,'1111111111',20,3),(477,'1','10000',3,'1111111111',20,2),(476,'1','10000',28,'1111111111',20,1),(475,'1','00001',5,'1111111111',19,5),(474,'1','00001',6,'1111111111',19,4),(473,'1','00001',1,'1111111111',19,3),(472,'1','00001',23,'1111111111',19,2),(471,'1','00001',2,'1111111111',19,1),(470,'1','00010',14,'1111111111',19,5),(469,'1','00010',4,'1111111111',19,4),(468,'1','00010',40,'1111111111',19,3),(467,'1','00010',8,'1111111111',19,2),(466,'1','00010',30,'1111111111',19,1),(465,'1','00100',31,'1111111111',19,5),(464,'1','00100',36,'1111111111',19,4),(463,'1','00100',26,'1111111111',19,3),(462,'1','00100',38,'1111111111',19,2),(461,'1','00100',7,'1111111111',19,1),(460,'1','01000',17,'1111111111',19,5),(459,'1','01000',27,'1111111111',19,4),(458,'1','01000',12,'1111111111',19,3),(457,'1','01000',32,'1111111111',19,2),(456,'1','01000',28,'1111111111',19,1),(455,'1','10000',43,'1111111111',19,5),(454,'1','10000',37,'1111111111',19,4),(453,'1','10000',28,'1111111111',19,3),(452,'1','10000',13,'1111111111',19,2),(451,'1','10000',13,'1111111111',19,1),(450,'1','00001',40,'1111111111',18,5),(449,'1','00001',35,'1111111111',18,4),(448,'1','00001',5,'1111111111',18,3),(447,'1','00001',18,'1111111111',18,2),(446,'1','00001',34,'1111111111',18,1),(445,'1','00010',22,'1111111111',18,5),(444,'1','00010',20,'1111111111',18,4),(443,'1','00010',28,'1111111111',18,3),(442,'1','00010',39,'1111111111',18,2),(441,'1','00010',0,'1111111111',18,1),(440,'1','00100',29,'1111111111',18,5),(439,'1','00100',37,'1111111111',18,4),(438,'1','00100',22,'1111111111',18,3),(437,'1','00100',3,'1111111111',18,2),(436,'1','00100',43,'1111111111',18,1),(435,'1','01000',3,'1111111111',18,5),(434,'1','01000',30,'1111111111',18,4),(433,'1','01000',12,'1111111111',18,3),(432,'1','01000',6,'1111111111',18,2),(431,'1','01000',6,'1111111111',18,1),(430,'1','10000',23,'1111111111',18,5),(429,'1','10000',18,'1111111111',18,4),(428,'1','10000',22,'1111111111',18,3),(427,'1','10000',3,'1111111111',18,2),(426,'1','10000',43,'1111111111',18,1),(425,'1','00001',14,'1111111111',17,5),(424,'1','00001',41,'1111111111',17,4),(423,'1','00001',13,'1111111111',17,3),(422,'1','00001',26,'1111111111',17,2),(421,'1','00001',9,'1111111111',17,1),(420,'1','00010',15,'1111111111',17,5),(419,'1','00010',4,'1111111111',17,4),(418,'1','00010',16,'1111111111',17,3),(417,'1','00010',16,'1111111111',17,2),(416,'1','00010',11,'1111111111',17,1),(415,'1','00100',39,'1111111111',17,5),(414,'1','00100',37,'1111111111',17,4),(413,'1','00100',23,'1111111111',17,3),(412,'1','00100',42,'1111111111',17,2),(411,'1','00100',4,'1111111111',17,1),(410,'1','01000',12,'1111111111',17,5),(409,'1','01000',15,'1111111111',17,4),(408,'1','01000',0,'1111111111',17,3),(407,'1','01000',34,'1111111111',17,2),(406,'1','01000',21,'1111111111',17,1),(405,'1','10000',13,'1111111111',17,5),(404,'1','10000',7,'1111111111',17,4),(403,'1','10000',43,'1111111111',17,3),(402,'1','10000',22,'1111111111',17,2),(401,'1','10000',32,'1111111111',17,1),(400,'1','00001',18,'1111111111',16,5),(399,'1','00001',40,'1111111111',16,4),(398,'1','00001',33,'1111111111',16,3),(397,'1','00001',39,'1111111111',16,2),(396,'1','00001',36,'1111111111',16,1),(395,'1','00010',28,'1111111111',16,5),(394,'1','00010',21,'1111111111',16,4),(393,'1','00010',27,'1111111111',16,3),(392,'1','00010',11,'1111111111',16,2),(391,'1','00010',29,'1111111111',16,1),(390,'1','00100',28,'1111111111',16,5),(389,'1','00100',15,'1111111111',16,4),(388,'1','00100',39,'1111111111',16,3),(387,'1','00100',1,'1111111111',16,2),(386,'1','00100',7,'1111111111',16,1),(385,'1','01000',6,'1111111111',16,5),(384,'1','01000',10,'1111111111',16,4),(383,'1','01000',16,'1111111111',16,3),(382,'1','01000',32,'1111111111',16,2),(381,'1','01000',43,'1111111111',16,1),(380,'1','10000',44,'1111111111',16,5),(379,'1','10000',12,'1111111111',16,4),(378,'1','10000',29,'1111111111',16,3),(377,'1','10000',36,'1111111111',16,2),(376,'1','10000',26,'1111111111',16,1),(375,'1','00001',37,'1111111111',15,5),(374,'1','00001',28,'1111111111',15,4),(373,'1','00001',12,'1111111111',15,3),(372,'1','00001',31,'1111111111',15,2),(371,'1','00001',38,'1111111111',15,1),(370,'1','00010',42,'1111111111',15,5),(369,'1','00010',37,'1111111111',15,4),(368,'1','00010',30,'1111111111',15,3),(367,'1','00010',42,'1111111111',15,2),(366,'1','00010',19,'1111111111',15,1),(365,'1','00100',4,'1111111111',15,5),(364,'1','00100',5,'1111111111',15,4),(363,'1','00100',5,'1111111111',15,3),(362,'1','00100',31,'1111111111',15,2),(361,'1','00100',44,'1111111111',15,1),(360,'1','01000',38,'1111111111',15,5),(359,'1','01000',12,'1111111111',15,4),(358,'1','01000',8,'1111111111',15,3),(357,'1','01000',25,'1111111111',15,2),(356,'1','01000',14,'1111111111',15,1),(355,'1','10000',15,'1111111111',15,5),(354,'1','10000',39,'1111111111',15,4),(353,'1','10000',16,'1111111111',15,3),(352,'1','10000',41,'1111111111',15,2),(351,'1','10000',40,'1111111111',15,1),(350,'1','00001',37,'1111111111',14,5),(349,'1','00001',36,'1111111111',14,4),(348,'1','00001',32,'1111111111',14,3),(347,'1','00001',31,'1111111111',14,2),(346,'1','00001',24,'1111111111',14,1),(345,'1','00010',19,'1111111111',14,5),(344,'1','00010',25,'1111111111',14,4),(343,'1','00010',26,'1111111111',14,3),(342,'1','00010',21,'1111111111',14,2),(341,'1','00010',18,'1111111111',14,1),(340,'1','00100',13,'1111111111',14,5),(339,'1','00100',20,'1111111111',14,4),(338,'1','00100',8,'1111111111',14,3),(337,'1','00100',4,'1111111111',14,2),(336,'1','00100',18,'1111111111',14,1),(335,'1','01000',7,'1111111111',14,5),(334,'1','01000',32,'1111111111',14,4),(333,'1','01000',10,'1111111111',14,3),(332,'1','01000',18,'1111111111',14,2),(331,'1','01000',9,'1111111111',14,1),(330,'1','10000',1,'1111111111',14,5),(329,'1','10000',40,'1111111111',14,4),(328,'1','10000',37,'1111111111',14,3),(327,'1','10000',38,'1111111111',14,2),(326,'1','10000',9,'1111111111',14,1),(325,'1','00001',31,'1111111111',13,5),(324,'1','00001',14,'1111111111',13,4),(323,'1','00001',17,'1111111111',13,3),(322,'1','00001',41,'1111111111',13,2),(321,'1','00001',43,'1111111111',13,1),(320,'1','00010',21,'1111111111',13,5),(319,'1','00010',18,'1111111111',13,4),(318,'1','00010',19,'1111111111',13,3),(317,'1','00010',18,'1111111111',13,2),(316,'1','00010',13,'1111111111',13,1),(315,'1','00100',18,'1111111111',13,5),(314,'1','00100',13,'1111111111',13,4),(313,'1','00100',6,'1111111111',13,3),(312,'1','00100',26,'1111111111',13,2),(311,'1','00100',20,'1111111111',13,1),(310,'1','01000',37,'1111111111',13,5),(309,'1','01000',40,'1111111111',13,4),(308,'1','01000',2,'1111111111',13,3),(307,'1','01000',24,'1111111111',13,2),(306,'1','01000',26,'1111111111',13,1),(305,'1','10000',22,'1111111111',13,5),(304,'1','10000',15,'1111111111',13,4),(303,'1','10000',39,'1111111111',13,3),(302,'1','10000',28,'1111111111',13,2),(301,'1','10000',27,'1111111111',13,1),(300,'1','00001',39,'1111111111',12,5),(299,'1','00001',31,'1111111111',12,4),(298,'1','00001',5,'1111111111',12,3),(297,'1','00001',5,'1111111111',12,2),(296,'1','00001',7,'1111111111',12,1),(295,'1','00010',18,'1111111111',12,5),(294,'1','00010',0,'1111111111',12,4),(293,'1','00010',33,'1111111111',12,3),(292,'1','00010',28,'1111111111',12,2),(291,'1','00010',4,'1111111111',12,1),(290,'1','00100',4,'1111111111',12,5),(289,'1','00100',1,'1111111111',12,4),(288,'1','00100',14,'1111111111',12,3),(287,'1','00100',10,'1111111111',12,2),(286,'1','00100',2,'1111111111',12,1),(285,'1','01000',22,'1111111111',12,5),(284,'1','01000',34,'1111111111',12,4),(283,'1','01000',37,'1111111111',12,3),(282,'1','01000',4,'1111111111',12,2),(281,'1','01000',36,'1111111111',12,1),(280,'1','10000',5,'1111111111',12,5),(279,'1','10000',2,'1111111111',12,4),(278,'1','10000',17,'1111111111',12,3),(277,'1','10000',35,'1111111111',12,2),(276,'1','10000',11,'1111111111',12,1),(275,'1','00001',21,'1111111111',11,5),(274,'1','00001',35,'1111111111',11,4),(273,'1','00001',4,'1111111111',11,3),(272,'1','00001',0,'1111111111',11,2),(271,'1','00001',40,'1111111111',11,1),(270,'1','00010',16,'1111111111',11,5),(269,'1','00010',22,'1111111111',11,4),(268,'1','00010',39,'1111111111',11,3),(267,'1','00010',44,'1111111111',11,2),(266,'1','00010',31,'1111111111',11,1),(265,'1','00100',22,'1111111111',11,5),(264,'1','00100',12,'1111111111',11,4),(263,'1','00100',2,'1111111111',11,3),(262,'1','00100',39,'1111111111',11,2),(261,'1','00100',13,'1111111111',11,1),(260,'1','01000',42,'1111111111',11,5),(259,'1','01000',28,'1111111111',11,4),(258,'1','01000',27,'1111111111',11,3),(257,'1','01000',6,'1111111111',11,2),(256,'1','01000',25,'1111111111',11,1),(255,'1','10000',25,'1111111111',11,5),(254,'1','10000',6,'1111111111',11,4),(253,'1','10000',22,'1111111111',11,3),(252,'1','10000',16,'1111111111',11,2),(251,'1','10000',20,'1111111111',11,1),(250,'1','00001',6,'1111111111',10,5),(249,'1','00001',33,'1111111111',10,4),(248,'1','00001',38,'1111111111',10,3),(247,'1','00001',21,'1111111111',10,2),(246,'1','00001',24,'1111111111',10,1),(245,'1','00010',15,'1111111111',10,5),(244,'1','00010',19,'1111111111',10,4),(243,'1','00010',42,'1111111111',10,3),(242,'1','00010',33,'1111111111',10,2),(241,'1','00010',34,'1111111111',10,1),(240,'1','00100',36,'1111111111',10,5),(239,'1','00100',25,'1111111111',10,4),(238,'1','00100',24,'1111111111',10,3),(237,'1','00100',8,'1111111111',10,2),(236,'1','00100',43,'1111111111',10,1),(235,'1','01000',28,'1111111111',10,5),(234,'1','01000',44,'1111111111',10,4),(233,'1','01000',31,'1111111111',10,3),(232,'1','01000',42,'1111111111',10,2),(231,'1','01000',39,'1111111111',10,1),(230,'1','10000',34,'1111111111',10,5),(229,'1','10000',7,'1111111111',10,4),(228,'1','10000',41,'1111111111',10,3),(227,'1','10000',16,'1111111111',10,2),(226,'1','10000',17,'1111111111',10,1),(225,'1','00001',7,'1111111111',9,5),(224,'1','00001',44,'1111111111',9,4),(223,'1','00001',10,'1111111111',9,3),(222,'1','00001',31,'1111111111',9,2),(221,'1','00001',27,'1111111111',9,1),(220,'1','00010',36,'1111111111',9,5),(219,'1','00010',4,'1111111111',9,4),(218,'1','00010',24,'1111111111',9,3),(217,'1','00010',41,'1111111111',9,2),(216,'1','00010',8,'1111111111',9,1),(215,'1','00100',30,'1111111111',9,5),(214,'1','00100',27,'1111111111',9,4),(213,'1','00100',39,'1111111111',9,3),(212,'1','00100',2,'1111111111',9,2),(211,'1','00100',28,'1111111111',9,1),(210,'1','01000',8,'1111111111',9,5),(209,'1','01000',39,'1111111111',9,4),(208,'1','01000',31,'1111111111',9,3),(207,'1','01000',36,'1111111111',9,2),(206,'1','01000',41,'1111111111',9,1),(205,'1','10000',18,'1111111111',9,5),(204,'1','10000',4,'1111111111',9,4),(203,'1','10000',6,'1111111111',9,3),(202,'1','10000',17,'1111111111',9,2),(201,'1','10000',33,'1111111111',9,1),(200,'1','00001',36,'1111111111',8,5),(199,'1','00001',18,'1111111111',8,4),(198,'1','00001',2,'1111111111',8,3),(197,'1','00001',4,'1111111111',8,2),(196,'1','00001',39,'1111111111',8,1),(195,'1','00010',13,'1111111111',8,5),(194,'1','00010',22,'1111111111',8,4),(193,'1','00010',7,'1111111111',8,3),(192,'1','00010',25,'1111111111',8,2),(191,'1','00010',24,'1111111111',8,1),(190,'1','00100',20,'1111111111',8,5),(189,'1','00100',37,'1111111111',8,4),(188,'1','00100',39,'1111111111',8,3),(187,'1','00100',18,'1111111111',8,2),(186,'1','00100',15,'1111111111',8,1),(185,'1','01000',27,'1111111111',8,5),(184,'1','01000',29,'1111111111',8,4),(183,'1','01000',30,'1111111111',8,3),(182,'1','01000',1,'1111111111',8,2),(181,'1','01000',7,'1111111111',8,1),(180,'1','10000',28,'1111111111',8,5),(179,'1','10000',34,'1111111111',8,4),(178,'1','10000',11,'1111111111',8,3),(177,'1','10000',36,'1111111111',8,2),(176,'1','10000',7,'1111111111',8,1),(175,'1','00001',24,'1111111111',7,5),(174,'1','00001',29,'1111111111',7,4),(173,'1','00001',38,'1111111111',7,3),(172,'1','00001',22,'1111111111',7,2),(171,'1','00001',22,'1111111111',7,1),(170,'1','00010',18,'1111111111',7,5),(169,'1','00010',9,'1111111111',7,4),(168,'1','00010',9,'1111111111',7,3),(167,'1','00010',9,'1111111111',7,2),(166,'1','00010',5,'1111111111',7,1),(165,'1','00100',26,'1111111111',7,5),(164,'1','00100',4,'1111111111',7,4),(163,'1','00100',14,'1111111111',7,3),(162,'1','00100',27,'1111111111',7,2),(161,'1','00100',3,'1111111111',7,1),(160,'1','01000',36,'1111111111',7,5),(159,'1','01000',12,'1111111111',7,4),(158,'1','01000',18,'1111111111',7,3),(157,'1','01000',40,'1111111111',7,2),(156,'1','01000',26,'1111111111',7,1),(155,'1','10000',15,'1111111111',7,5),(154,'1','10000',5,'1111111111',7,4),(153,'1','10000',24,'1111111111',7,3),(152,'1','10000',30,'1111111111',7,2),(151,'1','10000',44,'1111111111',7,1),(150,'1','00001',8,'1111111111',6,5),(149,'1','00001',11,'1111111111',6,4),(148,'1','00001',9,'1111111111',6,3),(147,'1','00001',28,'1111111111',6,2),(146,'1','00001',9,'1111111111',6,1),(145,'1','00010',35,'1111111111',6,5),(144,'1','00010',23,'1111111111',6,4),(143,'1','00010',31,'1111111111',6,3),(142,'1','00010',38,'1111111111',6,2),(141,'1','00010',40,'1111111111',6,1),(140,'1','00100',38,'1111111111',6,5),(139,'1','00100',18,'1111111111',6,4),(138,'1','00100',33,'1111111111',6,3),(137,'1','00100',21,'1111111111',6,2),(136,'1','00100',1,'1111111111',6,1),(135,'1','01000',43,'1111111111',6,5),(134,'1','01000',20,'1111111111',6,4),(133,'1','01000',25,'1111111111',6,3),(132,'1','01000',25,'1111111111',6,2),(131,'1','01000',38,'1111111111',6,1),(130,'1','10000',13,'1111111111',6,5),(129,'1','10000',15,'1111111111',6,4),(128,'1','10000',41,'1111111111',6,3),(127,'1','10000',15,'1111111111',6,2),(126,'1','10000',2,'1111111111',6,1),(125,'1','00001',37,'1111111111',5,5),(124,'1','00001',2,'1111111111',5,4),(123,'1','00001',7,'1111111111',5,3),(122,'1','00001',39,'1111111111',5,2),(121,'1','00001',6,'1111111111',5,1),(120,'1','00010',37,'1111111111',5,5),(119,'1','00010',31,'1111111111',5,4),(118,'1','00010',4,'1111111111',5,3),(117,'1','00010',33,'1111111111',5,2),(116,'1','00010',34,'1111111111',5,1),(115,'1','00100',33,'1111111111',5,5),(114,'1','00100',4,'1111111111',5,4),(113,'1','00100',5,'1111111111',5,3),(112,'1','00100',35,'1111111111',5,2),(111,'1','00100',42,'1111111111',5,1),(110,'1','01000',17,'1111111111',5,5),(109,'1','01000',14,'1111111111',5,4),(108,'1','01000',21,'1111111111',5,3),(107,'1','01000',31,'1111111111',5,2),(106,'1','01000',35,'1111111111',5,1),(105,'1','10000',20,'1111111111',5,5),(104,'1','10000',37,'1111111111',5,4),(103,'1','10000',42,'1111111111',5,3),(102,'1','10000',13,'1111111111',5,2),(101,'1','10000',20,'1111111111',5,1),(100,'1','00001',40,'1111111111',4,5),(99,'1','00001',27,'1111111111',4,4),(98,'1','00001',33,'1111111111',4,3),(97,'1','00001',14,'1111111111',4,2),(96,'1','00001',29,'1111111111',4,1),(95,'1','00010',24,'1111111111',4,5),(94,'1','00010',12,'1111111111',4,4),(93,'1','00010',2,'1111111111',4,3),(92,'1','00010',38,'1111111111',4,2),(91,'1','00010',2,'1111111111',4,1),(90,'1','00100',42,'1111111111',4,5),(89,'1','00100',34,'1111111111',4,4),(88,'1','00100',41,'1111111111',4,3),(87,'1','00100',17,'1111111111',4,2),(86,'1','00100',8,'1111111111',4,1),(85,'1','01000',25,'1111111111',4,5),(84,'1','01000',19,'1111111111',4,4),(83,'1','01000',43,'1111111111',4,3),(82,'1','01000',32,'1111111111',4,2),(81,'1','01000',12,'1111111111',4,1),(80,'1','10000',40,'1111111111',4,5),(79,'1','10000',8,'1111111111',4,4),(78,'1','10000',31,'1111111111',4,3),(77,'1','10000',2,'1111111111',4,2),(76,'1','10000',39,'1111111111',4,1),(75,'1','00001',3,'1111111111',3,5),(74,'1','00001',1,'1111111111',3,4),(73,'1','00001',35,'1111111111',3,3),(72,'1','00001',34,'1111111111',3,2),(71,'1','00001',16,'1111111111',3,1),(70,'1','00010',7,'1111111111',3,5),(69,'1','00010',5,'1111111111',3,4),(68,'1','00010',22,'1111111111',3,3),(67,'1','00010',38,'1111111111',3,2),(66,'1','00010',36,'1111111111',3,1),(65,'1','00100',26,'1111111111',3,5),(64,'1','00100',30,'1111111111',3,4),(63,'1','00100',21,'1111111111',3,3),(62,'1','00100',35,'1111111111',3,2),(61,'1','00100',37,'1111111111',3,1),(60,'1','01000',16,'1111111111',3,5),(59,'1','01000',34,'1111111111',3,4),(58,'1','01000',14,'1111111111',3,3),(57,'1','01000',22,'1111111111',3,2),(56,'1','01000',23,'1111111111',3,1),(55,'1','10000',25,'1111111111',3,5),(54,'1','10000',18,'1111111111',3,4),(53,'1','10000',29,'1111111111',3,3),(52,'1','10000',37,'1111111111',3,2),(51,'1','10000',34,'1111111111',3,1),(50,'1','00001',6,'1111111111',2,5),(49,'1','00001',24,'1111111111',2,4),(48,'1','00001',11,'1111111111',2,3),(47,'1','00001',43,'1111111111',2,2),(46,'1','00001',23,'1111111111',2,1),(45,'1','00010',20,'1111111111',2,5),(44,'1','00010',17,'1111111111',2,4),(43,'1','00010',9,'1111111111',2,3),(42,'1','00010',43,'1111111111',2,2),(41,'1','00010',44,'1111111111',2,1),(40,'1','00100',14,'1111111111',2,5),(39,'1','00100',20,'1111111111',2,4),(38,'1','00100',32,'1111111111',2,3),(37,'1','00100',32,'1111111111',2,2),(36,'1','00100',34,'1111111111',2,1),(35,'1','01000',14,'1111111111',2,5),(34,'1','01000',31,'1111111111',2,4),(33,'1','01000',16,'1111111111',2,3),(32,'1','01000',36,'1111111111',2,2),(31,'1','01000',28,'1111111111',2,1),(30,'1','10000',0,'1111111111',2,5),(29,'1','10000',6,'1111111111',2,4),(28,'1','10000',15,'1111111111',2,3),(27,'1','10000',13,'1111111111',2,2),(26,'1','10000',31,'1111111111',2,1),(25,'1','00001',6,'1111111111',1,5),(24,'1','00001',4,'1111111111',1,4),(23,'1','00001',18,'1111111111',1,3),(22,'1','00001',20,'1111111111',1,2),(21,'1','00001',13,'1111111111',1,1),(20,'1','00010',21,'1111111111',1,5),(19,'1','00010',13,'1111111111',1,4),(18,'1','00010',22,'1111111111',1,3),(17,'1','00010',2,'1111111111',1,2),(16,'1','00010',38,'1111111111',1,1),(15,'1','00100',35,'1111111111',1,5),(14,'1','00100',42,'1111111111',1,4),(13,'1','00100',22,'1111111111',1,3),(12,'1','00100',4,'1111111111',1,2),(11,'1','00100',4,'1111111111',1,1),(10,'1','01000',5,'1111111111',1,5),(9,'1','01000',33,'1111111111',1,4),(8,'1','01000',29,'1111111111',1,3),(7,'1','01000',38,'1111111111',1,2),(6,'1','01000',34,'1111111111',1,1),(5,'1','10000',39,'1111111111',1,5),(4,'1','10000',0,'1111111111',1,4),(3,'1','10000',39,'1111111111',1,3),(2,'1','10000',30,'1111111111',1,2),(1,'1','10000',18,'1111111111',1,1),(875,'1','00001',2,'1111111111',35,5);
/*!40000 ALTER TABLE `programme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salle`
--

DROP TABLE IF EXISTS `salle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salle` (
  `code_salle` varchar(255) NOT NULL,
  `code_departement` bigint DEFAULT NULL,
  `code_type_salle` bigint DEFAULT NULL,
  `capacite_salle` int DEFAULT NULL,
  `libelle_salle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_salle`),
  KEY `FKfmch7mnncnd7rfuu54h633aba` (`code_departement`),
  KEY `FKgnf0h4lgps07vgvhr7a3eh9q1` (`code_type_salle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salle`
--

LOCK TABLES `salle` WRITE;
/*!40000 ALTER TABLE `salle` DISABLE KEYS */;
INSERT INTO `salle` VALUES ('3',100,3,20,'H003'),('2',100,2,20,'H002'),('1',100,1,20,'H001'),('4',100,4,20,'AMPHI-A'),('5',100,5,20,'AMPHI-B');
/*!40000 ALTER TABLE `salle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seance`
--

DROP TABLE IF EXISTS `seance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seance` (
  `code_seance` bigint NOT NULL,
  `date_seance` varchar(255) DEFAULT NULL,
  `duree_seance` varchar(255) DEFAULT NULL,
  `code_classe` bigint DEFAULT NULL,
  `numero_commentaire` bigint DEFAULT NULL,
  `code_salle` varchar(255) DEFAULT NULL,
  `code_type_seance` bigint DEFAULT NULL,
  `libelle_matiere` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_seance`),
  KEY `FK8if8w2gaqd3htskn3s0w24f0m` (`code_classe`),
  KEY `FKet7u8dpkg1yym1fnlwut4tdwm` (`numero_commentaire`),
  KEY `FKs8qekijtd3segx8sumgllpi46` (`code_salle`),
  KEY `FKoh8f6edpbjfgct2ujhowsla32` (`code_type_seance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seance`
--

LOCK TABLES `seance` WRITE;
/*!40000 ALTER TABLE `seance` DISABLE KEYS */;
INSERT INTO `seance` VALUES (2451,'2020-09-03T14:40','01:20',3,NULL,'1',3,'Anglais'),(2452,'2020-09-10T14:40','01:20',3,NULL,'1',3,'Anglais'),(2453,'2020-09-17T14:40','01:20',3,NULL,'1',3,'Anglais'),(2454,'2020-09-24T14:40','01:20',3,NULL,'1',3,'Anglais'),(2455,'2020-10-01T14:40','01:20',3,NULL,'1',3,'Anglais'),(2456,'2020-10-08T14:40','01:20',3,NULL,'1',3,'Anglais'),(2457,'2020-10-15T14:40','01:20',3,NULL,'1',3,'Anglais'),(2458,'2020-10-22T14:40','01:20',3,NULL,'1',3,'Anglais'),(2459,'2020-10-29T14:40','01:20',3,NULL,'1',3,'Anglais'),(2460,'2020-11-05T14:40','01:20',3,NULL,'1',3,'Anglais'),(2461,'2020-09-03T13:20','01:20',2,NULL,'1',3,'Anglais'),(2462,'2020-09-10T13:20','01:20',2,NULL,'1',3,'Anglais'),(2463,'2020-09-17T13:20','01:20',2,NULL,'1',3,'Anglais'),(2464,'2020-09-24T13:20','01:20',2,NULL,'1',3,'Anglais'),(2465,'2020-10-01T13:20','01:20',2,NULL,'1',3,'Anglais'),(2466,'2020-10-08T13:20','01:20',2,NULL,'1',3,'Anglais'),(2467,'2020-10-15T13:20','01:20',2,NULL,'1',3,'Anglais'),(2468,'2020-10-22T13:20','01:20',2,NULL,'1',3,'Anglais'),(2469,'2020-10-29T13:20','01:20',2,NULL,'1',3,'Anglais'),(2470,'2020-11-05T13:20','01:20',2,NULL,'1',3,'Anglais'),(2471,'2020-09-03T12:00','01:20',1,NULL,'4',3,'Anglais'),(2472,'2020-09-10T12:00','01:20',1,NULL,'4',3,'Anglais'),(2473,'2020-09-17T12:00','01:20',1,NULL,'4',3,'Anglais'),(2474,'2020-09-24T12:00','01:20',1,NULL,'4',3,'Anglais'),(2475,'2020-10-01T12:00','01:20',1,NULL,'4',3,'Anglais'),(2476,'2020-10-08T12:00','01:20',1,NULL,'4',3,'Anglais'),(2477,'2020-10-15T12:00','01:20',1,NULL,'4',3,'Anglais'),(2478,'2020-10-22T12:00','01:20',1,NULL,'4',3,'Anglais'),(2479,'2020-10-29T12:00','01:20',1,NULL,'4',3,'Anglais'),(2480,'2020-11-05T12:00','01:20',1,NULL,'4',3,'Anglais'),(2481,'2020-09-05T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2482,'2020-09-12T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2483,'2020-09-19T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2484,'2020-09-26T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2485,'2020-10-03T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2486,'2020-10-10T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2487,'2020-10-17T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2488,'2020-10-24T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2489,'2020-10-31T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2490,'2020-11-07T09:20','01:20',33,NULL,'3',1,'Sys intelligents'),(2491,'2020-09-01T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2492,'2020-09-08T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2493,'2020-09-15T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2494,'2020-09-22T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2495,'2020-09-29T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2496,'2020-10-06T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2497,'2020-10-13T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2498,'2020-10-20T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2499,'2020-10-27T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2500,'2020-11-03T10:40','01:20',34,NULL,'3',2,'Sys intelligents'),(2501,'2020-09-05T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2502,'2020-09-12T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2503,'2020-09-19T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2504,'2020-09-26T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2505,'2020-10-03T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2506,'2020-10-10T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2507,'2020-10-17T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2508,'2020-10-24T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2509,'2020-10-31T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2510,'2020-11-07T12:00','01:20',35,NULL,'4',3,'Sys intelligents'),(2511,'2020-09-04T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2512,'2020-09-11T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2513,'2020-09-18T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2514,'2020-09-25T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2515,'2020-10-02T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2516,'2020-10-09T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2517,'2020-10-16T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2518,'2020-10-23T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2519,'2020-10-30T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2520,'2020-11-06T12:00','01:20',31,NULL,'2',1,'QT avancé'),(2521,'2020-09-01T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2522,'2020-09-08T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2523,'2020-09-15T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2524,'2020-09-22T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2525,'2020-09-29T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2526,'2020-10-06T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2527,'2020-10-13T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2528,'2020-10-20T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2529,'2020-10-27T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2530,'2020-11-03T13:20','01:20',32,NULL,'5',3,'QT avancé'),(2531,'2020-09-01T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2532,'2020-09-08T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2533,'2020-09-15T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2534,'2020-09-22T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2535,'2020-09-29T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2536,'2020-10-06T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2537,'2020-10-13T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2538,'2020-10-20T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2539,'2020-10-27T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2540,'2020-11-03T10:40','01:20',16,NULL,'2',1,'Production automatisée de doc'),(2541,'2020-09-03T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2542,'2020-09-10T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2543,'2020-09-17T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2544,'2020-09-24T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2545,'2020-10-01T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2546,'2020-10-08T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2547,'2020-10-15T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2548,'2020-10-22T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2549,'2020-10-29T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2550,'2020-11-05T09:20','01:20',17,NULL,'3',2,'Production automatisée de doc'),(2551,'2020-09-04T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2552,'2020-09-11T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2553,'2020-09-18T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2554,'2020-09-25T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2555,'2020-10-02T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2556,'2020-10-09T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2557,'2020-10-16T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2558,'2020-10-23T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2559,'2020-10-30T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2560,'2020-11-06T14:40','01:20',18,NULL,'1',3,'Production automatisée de doc'),(2561,'2020-09-05T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2562,'2020-09-12T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2563,'2020-09-19T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2564,'2020-09-26T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2565,'2020-10-03T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2566,'2020-10-10T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2567,'2020-10-17T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2568,'2020-10-24T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2569,'2020-10-31T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2570,'2020-11-07T13:20','01:20',14,NULL,'2',1,'Image de Synthèse'),(2571,'2020-09-01T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2572,'2020-09-08T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2573,'2020-09-15T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2574,'2020-09-22T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2575,'2020-09-29T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2576,'2020-10-06T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2577,'2020-10-13T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2578,'2020-10-20T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2579,'2020-10-27T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2580,'2020-11-03T13:20','01:20',15,NULL,'4',3,'Image de Synthèse'),(2581,'2020-09-03T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2582,'2020-09-10T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2583,'2020-09-17T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2584,'2020-09-24T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2585,'2020-10-01T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2586,'2020-10-08T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2587,'2020-10-15T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2588,'2020-10-22T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2589,'2020-10-29T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2590,'2020-11-05T09:20','01:20',19,NULL,'2',1,'Prog Fonctionnelle'),(2591,'2020-09-04T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2592,'2020-09-11T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2593,'2020-09-18T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2594,'2020-09-25T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2595,'2020-10-02T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2596,'2020-10-09T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2597,'2020-10-16T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2598,'2020-10-23T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2599,'2020-10-30T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2600,'2020-11-06T10:40','01:20',21,NULL,'2',2,'Prog Fonctionnelle'),(2601,'2020-09-04T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2602,'2020-09-11T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2603,'2020-09-18T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2604,'2020-09-25T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2605,'2020-10-02T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2606,'2020-10-09T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2607,'2020-10-16T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2608,'2020-10-23T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2609,'2020-10-30T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2610,'2020-11-06T13:20','01:20',20,NULL,'2',2,'Prog Fonctionnelle'),(2611,'2020-09-01T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2612,'2020-09-08T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2613,'2020-09-15T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2614,'2020-09-22T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2615,'2020-09-29T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2616,'2020-10-06T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2617,'2020-10-13T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2618,'2020-10-20T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2619,'2020-10-27T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2620,'2020-11-03T13:20','01:20',24,NULL,'5',3,'Prog Fonctionnelle'),(2621,'2020-09-01T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2622,'2020-09-08T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2623,'2020-09-15T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2624,'2020-09-22T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2625,'2020-09-29T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2626,'2020-10-06T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2627,'2020-10-13T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2628,'2020-10-20T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2629,'2020-10-27T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2630,'2020-11-03T14:40','01:20',23,NULL,'5',3,'Prog Fonctionnelle'),(2631,'2020-09-01T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2632,'2020-09-08T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2633,'2020-09-15T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2634,'2020-09-22T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2635,'2020-09-29T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2636,'2020-10-06T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2637,'2020-10-13T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2638,'2020-10-20T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2639,'2020-10-27T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2640,'2020-11-03T10:40','01:20',22,NULL,'5',3,'Prog Fonctionnelle'),(2641,'2020-09-05T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2642,'2020-09-12T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2643,'2020-09-19T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2644,'2020-09-26T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2645,'2020-10-03T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2646,'2020-10-10T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2647,'2020-10-17T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2648,'2020-10-24T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2649,'2020-10-31T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2650,'2020-11-07T09:20','01:20',25,NULL,'2',1,'Prog Logique'),(2651,'2020-09-02T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2652,'2020-09-09T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2653,'2020-09-16T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2654,'2020-09-23T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2655,'2020-09-30T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2656,'2020-10-07T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2657,'2020-10-14T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2658,'2020-10-21T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2659,'2020-10-28T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2660,'2020-11-04T13:20','01:20',27,NULL,'3',2,'Prog Logique'),(2661,'2020-09-02T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2662,'2020-09-09T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2663,'2020-09-16T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2664,'2020-09-23T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2665,'2020-09-30T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2666,'2020-10-07T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2667,'2020-10-14T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2668,'2020-10-21T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2669,'2020-10-28T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2670,'2020-11-04T12:00','01:20',26,NULL,'3',2,'Prog Logique'),(2671,'2020-09-02T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2672,'2020-09-09T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2673,'2020-09-16T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2674,'2020-09-23T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2675,'2020-09-30T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2676,'2020-10-07T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2677,'2020-10-14T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2678,'2020-10-21T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2679,'2020-10-28T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2680,'2020-11-04T12:00','01:20',30,NULL,'1',3,'Prog Logique'),(2681,'2020-09-02T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2682,'2020-09-09T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2683,'2020-09-16T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2684,'2020-09-23T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2685,'2020-09-30T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2686,'2020-10-07T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2687,'2020-10-14T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2688,'2020-10-21T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2689,'2020-10-28T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2690,'2020-11-04T12:00','01:20',29,NULL,'4',3,'Prog Logique'),(2691,'2020-09-02T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2692,'2020-09-09T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2693,'2020-09-16T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2694,'2020-09-23T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2695,'2020-09-30T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2696,'2020-10-07T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2697,'2020-10-14T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2698,'2020-10-21T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2699,'2020-10-28T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2700,'2020-11-04T10:40','01:20',28,NULL,'5',3,'Prog Logique'),(2701,'2020-09-03T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2702,'2020-09-10T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2703,'2020-09-17T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2704,'2020-09-24T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2705,'2020-10-01T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2706,'2020-10-08T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2707,'2020-10-15T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2708,'2020-10-22T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2709,'2020-10-29T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2710,'2020-11-05T09:20','01:20',10,NULL,'3',1,'Dev Web'),(2711,'2020-09-02T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2712,'2020-09-09T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2713,'2020-09-16T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2714,'2020-09-23T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2715,'2020-09-30T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2716,'2020-10-07T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2717,'2020-10-14T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2718,'2020-10-21T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2719,'2020-10-28T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2720,'2020-11-04T13:20','01:20',13,NULL,'4',3,'Dev Web'),(2721,'2020-09-02T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2722,'2020-09-09T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2723,'2020-09-16T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2724,'2020-09-23T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2725,'2020-09-30T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2726,'2020-10-07T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2727,'2020-10-14T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2728,'2020-10-21T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2729,'2020-10-28T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2730,'2020-11-04T09:20','01:20',12,NULL,'4',3,'Dev Web'),(2731,'2020-09-02T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2732,'2020-09-09T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2733,'2020-09-16T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2734,'2020-09-23T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2735,'2020-09-30T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2736,'2020-10-07T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2737,'2020-10-14T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2738,'2020-10-21T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2739,'2020-10-28T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2740,'2020-11-04T10:40','01:20',11,NULL,'1',3,'Dev Web'),(2741,'2020-09-05T10:40','01:20',4,NULL,'2',1,'BDD'),(2742,'2020-09-12T10:40','01:20',4,NULL,'2',1,'BDD'),(2743,'2020-09-19T10:40','01:20',4,NULL,'2',1,'BDD'),(2744,'2020-09-26T10:40','01:20',4,NULL,'2',1,'BDD'),(2745,'2020-10-03T10:40','01:20',4,NULL,'2',1,'BDD'),(2746,'2020-10-10T10:40','01:20',4,NULL,'2',1,'BDD'),(2747,'2020-10-17T10:40','01:20',4,NULL,'2',1,'BDD'),(2748,'2020-10-24T10:40','01:20',4,NULL,'2',1,'BDD'),(2749,'2020-10-31T10:40','01:20',4,NULL,'2',1,'BDD'),(2750,'2020-11-07T10:40','01:20',4,NULL,'2',1,'BDD'),(2751,'2020-09-01T13:20','01:20',6,NULL,'3',2,'BDD'),(2752,'2020-09-08T13:20','01:20',6,NULL,'3',2,'BDD'),(2753,'2020-09-15T13:20','01:20',6,NULL,'3',2,'BDD'),(2754,'2020-09-22T13:20','01:20',6,NULL,'3',2,'BDD'),(2755,'2020-09-29T13:20','01:20',6,NULL,'3',2,'BDD'),(2756,'2020-10-06T13:20','01:20',6,NULL,'3',2,'BDD'),(2757,'2020-10-13T13:20','01:20',6,NULL,'3',2,'BDD'),(2758,'2020-10-20T13:20','01:20',6,NULL,'3',2,'BDD'),(2759,'2020-10-27T13:20','01:20',6,NULL,'3',2,'BDD'),(2760,'2020-11-03T13:20','01:20',6,NULL,'3',2,'BDD'),(2761,'2020-09-01T13:20','01:20',5,NULL,'3',2,'BDD'),(2762,'2020-09-08T13:20','01:20',5,NULL,'3',2,'BDD'),(2763,'2020-09-15T13:20','01:20',5,NULL,'3',2,'BDD'),(2764,'2020-09-22T13:20','01:20',5,NULL,'3',2,'BDD'),(2765,'2020-09-29T13:20','01:20',5,NULL,'3',2,'BDD'),(2766,'2020-10-06T13:20','01:20',5,NULL,'3',2,'BDD'),(2767,'2020-10-13T13:20','01:20',5,NULL,'3',2,'BDD'),(2768,'2020-10-20T13:20','01:20',5,NULL,'3',2,'BDD'),(2769,'2020-10-27T13:20','01:20',5,NULL,'3',2,'BDD'),(2770,'2020-11-03T13:20','01:20',5,NULL,'3',2,'BDD'),(2771,'2020-09-02T14:40','01:20',9,NULL,'5',3,'BDD'),(2772,'2020-09-09T14:40','01:20',9,NULL,'5',3,'BDD'),(2773,'2020-09-16T14:40','01:20',9,NULL,'5',3,'BDD'),(2774,'2020-09-23T14:40','01:20',9,NULL,'5',3,'BDD'),(2775,'2020-09-30T14:40','01:20',9,NULL,'5',3,'BDD'),(2776,'2020-10-07T14:40','01:20',9,NULL,'5',3,'BDD'),(2777,'2020-10-14T14:40','01:20',9,NULL,'5',3,'BDD'),(2778,'2020-10-21T14:40','01:20',9,NULL,'5',3,'BDD'),(2779,'2020-10-28T14:40','01:20',9,NULL,'5',3,'BDD'),(2780,'2020-11-04T14:40','01:20',9,NULL,'5',3,'BDD'),(2781,'2020-09-02T09:20','01:20',8,NULL,'1',3,'BDD'),(2782,'2020-09-09T09:20','01:20',8,NULL,'1',3,'BDD'),(2783,'2020-09-16T09:20','01:20',8,NULL,'1',3,'BDD'),(2784,'2020-09-23T09:20','01:20',8,NULL,'1',3,'BDD'),(2785,'2020-09-30T09:20','01:20',8,NULL,'1',3,'BDD'),(2786,'2020-10-07T09:20','01:20',8,NULL,'1',3,'BDD'),(2787,'2020-10-14T09:20','01:20',8,NULL,'1',3,'BDD'),(2788,'2020-10-21T09:20','01:20',8,NULL,'1',3,'BDD'),(2789,'2020-10-28T09:20','01:20',8,NULL,'1',3,'BDD'),(2790,'2020-11-04T09:20','01:20',8,NULL,'1',3,'BDD'),(2791,'2020-09-02T13:20','01:20',7,NULL,'1',3,'BDD'),(2792,'2020-09-09T13:20','01:20',7,NULL,'1',3,'BDD'),(2793,'2020-09-16T13:20','01:20',7,NULL,'1',3,'BDD'),(2794,'2020-09-23T13:20','01:20',7,NULL,'1',3,'BDD'),(2795,'2020-09-30T13:20','01:20',7,NULL,'1',3,'BDD'),(2796,'2020-10-07T13:20','01:20',7,NULL,'1',3,'BDD'),(2797,'2020-10-14T13:20','01:20',7,NULL,'1',3,'BDD'),(2798,'2020-10-21T13:20','01:20',7,NULL,'1',3,'BDD'),(2799,'2020-10-28T13:20','01:20',7,NULL,'1',3,'BDD'),(2800,'2020-11-04T13:20','01:20',7,NULL,'1',3,'BDD');
/*!40000 ALTER TABLE `seance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seance_enseignant`
--

DROP TABLE IF EXISTS `seance_enseignant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seance_enseignant` (
  `code_seance` bigint NOT NULL,
  `matricule_enseignant` varchar(15) NOT NULL,
  KEY `FKse12ewhafppd6sc2gqspa4wps` (`matricule_enseignant`),
  KEY `FK2kch6c110x887q5pqxuxlcwgx` (`code_seance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seance_enseignant`
--

LOCK TABLES `seance_enseignant` WRITE;
/*!40000 ALTER TABLE `seance_enseignant` DISABLE KEYS */;
INSERT INTO `seance_enseignant` VALUES (3,'1'),(2,'1'),(2,'2'),(1,'1'),(2451,'4'),(2452,'4'),(2453,'4'),(2454,'4'),(2455,'4'),(2456,'4'),(2457,'4'),(2458,'4'),(2459,'4'),(2460,'4'),(2461,'4'),(2462,'4'),(2463,'4'),(2464,'4'),(2465,'4'),(2466,'4'),(2467,'4'),(2468,'4'),(2469,'4'),(2470,'4'),(2471,'4'),(2472,'4'),(2473,'4'),(2474,'4'),(2475,'4'),(2476,'4'),(2477,'4'),(2478,'4'),(2479,'4'),(2480,'4'),(2481,'8'),(2482,'8'),(2483,'8'),(2484,'8'),(2485,'8'),(2486,'8'),(2487,'8'),(2488,'8'),(2489,'8'),(2490,'8'),(2491,'8'),(2492,'8'),(2493,'8'),(2494,'8'),(2495,'8'),(2496,'8'),(2497,'8'),(2498,'8'),(2499,'8'),(2500,'8'),(2501,'8'),(2502,'8'),(2503,'8'),(2504,'8'),(2505,'8'),(2506,'8'),(2507,'8'),(2508,'8'),(2509,'8'),(2510,'8'),(2511,'7'),(2512,'7'),(2513,'7'),(2514,'7'),(2515,'7'),(2516,'7'),(2517,'7'),(2518,'7'),(2519,'7'),(2520,'7'),(2521,'7'),(2522,'7'),(2523,'7'),(2524,'7'),(2525,'7'),(2526,'7'),(2527,'7'),(2528,'7'),(2529,'7'),(2530,'7'),(2531,'9'),(2532,'9'),(2533,'9'),(2534,'9'),(2535,'9'),(2536,'9'),(2537,'9'),(2538,'9'),(2539,'9'),(2540,'9'),(2541,'9'),(2542,'9'),(2543,'9'),(2544,'9'),(2545,'9'),(2546,'9'),(2547,'9'),(2548,'9'),(2549,'9'),(2550,'9'),(2551,'9'),(2552,'9'),(2553,'9'),(2554,'9'),(2555,'9'),(2556,'9'),(2557,'9'),(2558,'9'),(2559,'9'),(2560,'9'),(2561,'3'),(2562,'3'),(2563,'3'),(2564,'3'),(2565,'3'),(2566,'3'),(2567,'3'),(2568,'3'),(2569,'3'),(2570,'3'),(2571,'3'),(2572,'3'),(2573,'3'),(2574,'3'),(2575,'3'),(2576,'3'),(2577,'3'),(2578,'3'),(2579,'3'),(2580,'3'),(2581,'5'),(2582,'5'),(2583,'5'),(2584,'5'),(2585,'5'),(2586,'5'),(2587,'5'),(2588,'5'),(2589,'5'),(2590,'5'),(2591,'5'),(2592,'5'),(2593,'5'),(2594,'5'),(2595,'5'),(2596,'5'),(2597,'5'),(2598,'5'),(2599,'5'),(2600,'5'),(2601,'5'),(2602,'5'),(2603,'5'),(2604,'5'),(2605,'5'),(2606,'5'),(2607,'5'),(2608,'5'),(2609,'5'),(2610,'5'),(2611,'5'),(2612,'5'),(2613,'5'),(2614,'5'),(2615,'5'),(2616,'5'),(2617,'5'),(2618,'5'),(2619,'5'),(2620,'5'),(2621,'5'),(2622,'5'),(2623,'5'),(2624,'5'),(2625,'5'),(2626,'5'),(2627,'5'),(2628,'5'),(2629,'5'),(2630,'5'),(2631,'5'),(2632,'5'),(2633,'5'),(2634,'5'),(2635,'5'),(2636,'5'),(2637,'5'),(2638,'5'),(2639,'5'),(2640,'5'),(2641,'6'),(2642,'6'),(2643,'6'),(2644,'6'),(2645,'6'),(2646,'6'),(2647,'6'),(2648,'6'),(2649,'6'),(2650,'6'),(2651,'6'),(2652,'6'),(2653,'6'),(2654,'6'),(2655,'6'),(2656,'6'),(2657,'6'),(2658,'6'),(2659,'6'),(2660,'6'),(2661,'6'),(2662,'6'),(2663,'6'),(2664,'6'),(2665,'6'),(2666,'6'),(2667,'6'),(2668,'6'),(2669,'6'),(2670,'6'),(2671,'6'),(2672,'6'),(2673,'6'),(2674,'6'),(2675,'6'),(2676,'6'),(2677,'6'),(2678,'6'),(2679,'6'),(2680,'6'),(2681,'6'),(2682,'6'),(2683,'6'),(2684,'6'),(2685,'6'),(2686,'6'),(2687,'6'),(2688,'6'),(2689,'6'),(2690,'6'),(2691,'6'),(2692,'6'),(2693,'6'),(2694,'6'),(2695,'6'),(2696,'6'),(2697,'6'),(2698,'6'),(2699,'6'),(2700,'6'),(2701,'2'),(2702,'2'),(2703,'2'),(2704,'2'),(2705,'2'),(2706,'2'),(2707,'2'),(2708,'2'),(2709,'2'),(2710,'2'),(2711,'2'),(2712,'2'),(2713,'2'),(2714,'2'),(2715,'2'),(2716,'2'),(2717,'2'),(2718,'2'),(2719,'2'),(2720,'2'),(2721,'2'),(2722,'2'),(2723,'2'),(2724,'2'),(2725,'2'),(2726,'2'),(2727,'2'),(2728,'2'),(2729,'2'),(2730,'2'),(2731,'2'),(2732,'2'),(2733,'2'),(2734,'2'),(2735,'2'),(2736,'2'),(2737,'2'),(2738,'2'),(2739,'2'),(2740,'2'),(2741,'1'),(2742,'1'),(2743,'1'),(2744,'1'),(2745,'1'),(2746,'1'),(2747,'1'),(2748,'1'),(2749,'1'),(2750,'1'),(2751,'1'),(2752,'1'),(2753,'1'),(2754,'1'),(2755,'1'),(2756,'1'),(2757,'1'),(2758,'1'),(2759,'1'),(2760,'1'),(2761,'1'),(2762,'1'),(2763,'1'),(2764,'1'),(2765,'1'),(2766,'1'),(2767,'1'),(2768,'1'),(2769,'1'),(2770,'1'),(2771,'1'),(2772,'1'),(2773,'1'),(2774,'1'),(2775,'1'),(2776,'1'),(2777,'1'),(2778,'1'),(2779,'1'),(2780,'1'),(2781,'1'),(2782,'1'),(2783,'1'),(2784,'1'),(2785,'1'),(2786,'1'),(2787,'1'),(2788,'1'),(2789,'1'),(2790,'1'),(2791,'1'),(2792,'1'),(2793,'1'),(2794,'1'),(2795,'1'),(2796,'1'),(2797,'1'),(2798,'1'),(2799,'1'),(2800,'1');
/*!40000 ALTER TABLE `seance_enseignant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statut_enseignant`
--

DROP TABLE IF EXISTS `statut_enseignant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statut_enseignant` (
  `code_statut` bigint NOT NULL,
  `libelle_statut` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_statut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statut_enseignant`
--

LOCK TABLES `statut_enseignant` WRITE;
/*!40000 ALTER TABLE `statut_enseignant` DISABLE KEYS */;
/*!40000 ALTER TABLE `statut_enseignant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_salle`
--

DROP TABLE IF EXISTS `type_salle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_salle` (
  `code_type_salle` bigint NOT NULL,
  `libelle_type_salle` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`code_type_salle`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_salle`
--

LOCK TABLES `type_salle` WRITE;
/*!40000 ALTER TABLE `type_salle` DISABLE KEYS */;
INSERT INTO `type_salle` VALUES (5,'AMPHI-B'),(4,'AMPHI-A'),(3,'H003'),(2,'H002'),(1,'H001');
/*!40000 ALTER TABLE `type_salle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_seance`
--

DROP TABLE IF EXISTS `type_seance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_seance` (
  `code_type_seance` bigint NOT NULL,
  `couleur_type_cours` varchar(255) DEFAULT NULL,
  `libelle_type_cours` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_type_seance`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_seance`
--

LOCK TABLES `type_seance` WRITE;
/*!40000 ALTER TABLE `type_seance` DISABLE KEYS */;
INSERT INTO `type_seance` VALUES (4,'Rouge','Examen'),(3,'Orange','Travaux Pratiques'),(2,'Vert','Travaux Dirigés'),(1,'Jaune','Cours Magistral');
/*!40000 ALTER TABLE `type_seance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ue_etape`
--

DROP TABLE IF EXISTS `ue_etape`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ue_etape` (
  `code_ue` varchar(255) NOT NULL,
  `code_etape` varchar(15) NOT NULL,
  KEY `FK51rrqqdljqvo4lxtrjxhyhufx` (`code_etape`),
  KEY `FKj3gih08xd2e97oynstvx7aos6` (`code_ue`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ue_etape`
--

LOCK TABLES `ue_etape` WRITE;
/*!40000 ALTER TABLE `ue_etape` DISABLE KEYS */;
INSERT INTO `ue_etape` VALUES ('9','3'),('8','3'),('7','3'),('6','3'),('5','3'),('4','3'),('3','3'),('2','3'),('1','3');
/*!40000 ALTER TABLE `ue_etape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unite_enseignement`
--

DROP TABLE IF EXISTS `unite_enseignement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unite_enseignement` (
  `code_ue` varchar(255) NOT NULL,
  `ects_ue` int DEFAULT NULL,
  `charge_prevu_cm_ue` int DEFAULT NULL,
  `charge_prevu_tp_ue` int DEFAULT NULL,
  `effectif_prevu_ue` int DEFAULT NULL,
  `nom_ue` varchar(255) DEFAULT NULL,
  `volume_horaire_ue` varchar(255) DEFAULT NULL,
  `code_periode` bigint DEFAULT NULL,
  PRIMARY KEY (`code_ue`),
  KEY `FKboxwqvl513hb4xio40bpi8rq7` (`code_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unite_enseignement`
--

LOCK TABLES `unite_enseignement` WRITE;
/*!40000 ALTER TABLE `unite_enseignement` DISABLE KEYS */;
INSERT INTO `unite_enseignement` VALUES ('5',5,0,18,50,'UE7','18',1),('4',3,8,20,50,'UE4','30',1),('3',5,20,16,50,'UE3','48',1),('2',5,16,24,50,'UE2','40',1),('1',5,12,16,50,'UE1','40',1);
/*!40000 ALTER TABLE `unite_enseignement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `universite`
--

DROP TABLE IF EXISTS `universite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `universite` (
  `code_universite` bigint NOT NULL,
  `lieu_universite` varchar(255) DEFAULT NULL,
  `nom_universite` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code_universite`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `universite`
--

LOCK TABLES `universite` WRITE;
/*!40000 ALTER TABLE `universite` DISABLE KEYS */;
INSERT INTO `universite` VALUES (10,'Université Angers','Angers');
/*!40000 ALTER TABLE `universite` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-02 18:21:45
