-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 30 Septembre 2020 à 14:43
-- Version du serveur :  5.7.31-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `emploi_temps`
--

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(6),
(6),
(6);

-- --------------------------------------------------------

--
-- Structure de la table `tc_faculte`
--

CREATE TABLE `tc_faculte` (
  `nom_faculte` varchar(200) NOT NULL,
  `lieu_faculte` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tc_faculte`
--

INSERT INTO `tc_faculte` (`nom_faculte`, `lieu_faculte`) VALUES
('Sciences', 'Belle-beille'),
('Economie', 'Saint serger');

-- --------------------------------------------------------

--
-- Structure de la table `tc_type_cours`
--

CREATE TABLE `tc_type_cours` (
  `code_type_cours` varchar(255) NOT NULL,
  `couleur_type_cours` varchar(255) DEFAULT NULL,
  `libelle_type_cours` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tc_type_cours`
--

INSERT INTO `tc_type_cours` (`code_type_cours`, `couleur_type_cours`, `libelle_type_cours`) VALUES
('CM', 'Jaune', 'Cours Magistral'),
('TD', 'Vert', 'Travaux Dirigés'),
('TP', 'Orange', 'Travaux Pratiques'),
('EX', 'Rouge', 'Examen');

-- --------------------------------------------------------

--
-- Structure de la table `tc_type_salle`
--

CREATE TABLE `tc_type_salle` (
  `code_type_salle` varchar(15) NOT NULL,
  `libelle_type_salle` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tc_type_salle`
--

INSERT INTO `tc_type_salle` (`code_type_salle`, `libelle_type_salle`) VALUES
('Amphi', 'Amphithéâtre'),
('TD', 'Salle de TD');

-- --------------------------------------------------------

--
-- Structure de la table `td_commentaire`
--

CREATE TABLE `td_commentaire` (
  `numero_commentaire` bigint(20) NOT NULL,
  `libelle_commentaire` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_commentaire`
--

INSERT INTO `td_commentaire` (`numero_commentaire`, `libelle_commentaire`) VALUES
(1, 'groupe 1, 2, 3'),
(2, 'test insertion'),
(3, 'test insertion'),
(4, 'test2 insertion'),
(5, 'test3 insertion');

-- --------------------------------------------------------

--
-- Structure de la table `td_cours`
--

CREATE TABLE `td_cours` (
  `numero_cours` bigint(20) NOT NULL,
  `duree_cours` int(11) NOT NULL,
  `numero_commentaire` bigint(20) DEFAULT NULL,
  `numero_groupe` bigint(20) DEFAULT NULL,
  `code_matiere` varchar(15) DEFAULT NULL,
  `code_salle` varchar(255) DEFAULT NULL,
  `code_type_cours` varchar(255) DEFAULT NULL,
  `date_cours` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_cours`
--

INSERT INTO `td_cours` (`numero_cours`, `duree_cours`, `numero_commentaire`, `numero_groupe`, `code_matiere`, `code_salle`, `code_type_cours`, `date_cours`) VALUES
(1, '02:00:00', NULL, 1, 'I001', 'H007', 'CM', '2020-09-16T16:00:00'),
(2, '04:00:00', NULL, 2, 'I003', 'H007', 'TD', '2020-09-17T08:00:00'),
(3, '04:00:00', 1, NULL, 'I004', 'H007', 'TP', '2020-09-15T14:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `td_cours_enseignant`
--

CREATE TABLE `td_cours_enseignant` (
  `numero_cours` bigint(20) NOT NULL,
  `matricule_enseignant` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_cours_enseignant`
--

INSERT INTO `td_cours_enseignant` (`numero_cours`, `matricule_enseignant`) VALUES
(1, 'P00001'),
(2, 'P00002'),
(3, 'P00003'),
(3, 'P00004'),
(3, 'P00005'),
(3, 'P00006');

-- --------------------------------------------------------

--
-- Structure de la table `td_enseignant`
--

CREATE TABLE `td_enseignant` (
  `matricule_enseignant` varchar(15) NOT NULL,
  `email_enseignant` varchar(100) DEFAULT NULL,
  `nom_enseignant` varchar(45) DEFAULT NULL,
  `prenom_enseignant` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_enseignant`
--

INSERT INTO `td_enseignant` (`matricule_enseignant`, `email_enseignant`, `nom_enseignant`, `prenom_enseignant`) VALUES
('P00001', 'david.lesaint@univ-angers.fr', 'LeSaint', 'David'),
('P00002', 'olivier.gaudet@univ-angers.fr', 'Gaudet', 'Olivier'),
('P00003', 'vincent.barichard@univ-angers.fr', 'Barichard', 'Vincent'),
('P00004', 'benoit.damota@univ-angers.fr', 'Da Mota', 'Benoit'),
('P00005', 'laurent.garcia@univ-angers.fr', 'Garcia', 'Laurent'),
('P00006', 'jean-michel.richer@univ-angers.fr', 'Richer', 'Jean Michel');

-- --------------------------------------------------------

--
-- Structure de la table `td_formation`
--

CREATE TABLE `td_formation` (
  `code_formation` varchar(15) NOT NULL,
  `libelle_formation` varchar(100) DEFAULT NULL,
  `nom_faculte` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_formation`
--

INSERT INTO `td_formation` (`code_formation`, `libelle_formation`, `nom_faculte`) VALUES
('S001', 'Licence 1 Informatique', 'Sciences'),
('S002', 'Licence 2 Informatique', 'Sciences'),
('S003', 'Licence 3 Informatique', 'Sciences'),
('S004', 'Master 1 Informatique', 'Sciences'),
('E001', 'Licence 1 Economie', 'Economie'),
('E002', 'Licence 2 Economie', 'Economie');

-- --------------------------------------------------------

--
-- Structure de la table `td_groupe`
--

CREATE TABLE `td_groupe` (
  `numero_groupe` bigint(20) NOT NULL,
  `libelle_groupe` varchar(30) DEFAULT NULL,
  `code_formation` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_groupe`
--

INSERT INTO `td_groupe` (`numero_groupe`, `libelle_groupe`, `code_formation`) VALUES
(1, 'Groupe 1', 'S004'),
(2, 'Groupe 2', 'S004');

-- --------------------------------------------------------

--
-- Structure de la table `td_matiere`
--

CREATE TABLE `td_matiere` (
  `code_matiere` varchar(15) NOT NULL,
  `libelle_matiere` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_matiere`
--

INSERT INTO `td_matiere` (`code_matiere`, `libelle_matiere`) VALUES
('I001', 'JAVA'),
('I002', 'Développement Mobile'),
('I003', 'Design Pattern'),
('I004', 'Concrétisation disciplinaire'),
('I005', 'Communication'),
('I006', 'Optimisation Linéaire');

-- --------------------------------------------------------

--
-- Structure de la table `td_salle`
--

CREATE TABLE `td_salle` (
  `code_salle` varchar(255) NOT NULL,
  `code_type_salle` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `td_salle`
--

INSERT INTO `td_salle` (`code_salle`, `code_type_salle`) VALUES
('H007', 'TD');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `tc_faculte`
--
ALTER TABLE `tc_faculte`
  ADD PRIMARY KEY (`nom_faculte`);

--
-- Index pour la table `tc_type_cours`
--
ALTER TABLE `tc_type_cours`
  ADD PRIMARY KEY (`code_type_cours`);

--
-- Index pour la table `tc_type_salle`
--
ALTER TABLE `tc_type_salle`
  ADD PRIMARY KEY (`code_type_salle`);

--
-- Index pour la table `td_commentaire`
--
ALTER TABLE `td_commentaire`
  ADD PRIMARY KEY (`numero_commentaire`);

--
-- Index pour la table `td_cours`
--
ALTER TABLE `td_cours`
  ADD PRIMARY KEY (`numero_cours`),
  ADD KEY `FKdqdjinys28g3nmoq0xp6tvpmt` (`numero_commentaire`),
  ADD KEY `FKt5e1nutnt2q9x916noa1tiqnm` (`numero_groupe`),
  ADD KEY `FKas6rb9mq404ye298mig58fit` (`code_matiere`),
  ADD KEY `FKqedxwnv1k4vm0nx3mqv1arj2x` (`code_salle`),
  ADD KEY `FK6gf44j0xed8iuuwlc3dwh4ppi` (`code_type_cours`);

--
-- Index pour la table `td_cours_enseignant`
--
ALTER TABLE `td_cours_enseignant`
  ADD PRIMARY KEY (`numero_cours`,`matricule_enseignant`),
  ADD KEY `FK6usrhejqsk7yohrdas8xglug6` (`matricule_enseignant`);

--
-- Index pour la table `td_enseignant`
--
ALTER TABLE `td_enseignant`
  ADD PRIMARY KEY (`matricule_enseignant`);

--
-- Index pour la table `td_formation`
--
ALTER TABLE `td_formation`
  ADD PRIMARY KEY (`code_formation`),
  ADD KEY `FKnevriyofimhtichluwqhv6ur0` (`nom_faculte`);

--
-- Index pour la table `td_groupe`
--
ALTER TABLE `td_groupe`
  ADD PRIMARY KEY (`numero_groupe`),
  ADD KEY `FK59qf7vv12w9dm48q19o3vpm98` (`code_formation`);

--
-- Index pour la table `td_matiere`
--
ALTER TABLE `td_matiere`
  ADD PRIMARY KEY (`code_matiere`);

--
-- Index pour la table `td_salle`
--
ALTER TABLE `td_salle`
  ADD PRIMARY KEY (`code_salle`),
  ADD KEY `FKilcctec2drw9s6twunn61299u` (`code_type_salle`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `td_cours`
--
ALTER TABLE `td_cours`
  MODIFY `numero_cours` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `td_groupe`
--
ALTER TABLE `td_groupe`
  MODIFY `numero_groupe` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
