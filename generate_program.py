import sys
import getopt
import argparse
from math import ceil, floor
import subprocess
import random


def getArgs():
    parser = argparse.ArgumentParser(description="""Write a data-sql for time table.""")
    parser.add_argument(
        "classCount",
        metavar="C",
        type=int,
        help="The total count of classes.",
        default=35,
    )
    parser.add_argument(
        "slotCount",
        metavar="S",
        type=int,
        help="The total count of slot in a day.",
        default=5,
    )
    parser.add_argument(
        "dayCount",
        metavar="D",
        type=int,
        help="The total count of days in a week.",
        default=5,
    )
    parser.add_argument(
        "weekCount", metavar="W", type=int, help="The total count of weeks.", default=10
    )

    parser.add_argument(
        "--output", "-o", default="time.sql", help="Name of output file."
    )
    args = parser.parse_args()
    return args


def main():
    args = getArgs()
    nb = 1
    weeks = ""
    for i in range(0, args.weekCount):
        weeks += "1"
    f = open(args.output, "w")
    f.write(
        "INSERT INTO programme (code_programme,duree_programme,jour_programme,semaine_programme,penalty_programme,code_classe,debut_programme) VALUES\n"
    )
    for j in range(1, (args.classCount + 1)):
        for i in range(1, args.dayCount * args.slotCount + 1):
            days = ""
            for l in range(1, args.dayCount + 1):
                if (i - 1) // args.slotCount == l - 1:
                    days += "1"
                else:
                    days += "0"

            if args.dayCount * args.slotCount * args.classCount == nb:
                f.write(
                    "("
                    + str(nb)
                    + ',"1","'
                    + days
                    + '","'
                    + weeks
                    + '",'
                    + str(random.randint(0, 44))
                    + ","
                    + str(j)
                    + ","
                    + str((i - 1) % args.slotCount + 1)
                    + ");"
                )
            else:
                f.write(
                    "("
                    + str(nb)
                    + ',"1","'
                    + days
                    + '","'
                    + weeks
                    + '",'
                    + str(random.randint(0, 44))
                    + ","
                    + str(j)
                    + ","
                    + str((i - 1) % args.slotCount + 1)
                    + "),\n"
                )
            nb += 1
        f.write("\n")

    f.close()


if __name__ == "__main__":
    main()
